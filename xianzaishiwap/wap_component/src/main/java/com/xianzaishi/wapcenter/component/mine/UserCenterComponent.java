package com.xianzaishi.wapcenter.component.mine;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.couponcenter.client.usercoupon.UserCouponService;
import com.xianzaishi.couponcenter.client.usercoupon.dto.UserCouponDTO;
import com.xianzaishi.couponcenter.client.usercoupon.query.QueryParaDTO;
import com.xianzaishi.couponcenter.client.usercoupon.query.QueryParaDTO.QueryTypeConstants;
import com.xianzaishi.couponcenter.client.usercoupon.query.SendParaDTO;
import com.xianzaishi.couponcenter.client.usercoupon.query.SendParaDTO.SendTypeConstants;
import com.xianzaishi.itemcenter.client.sysproperty.SystemPropertyService;
import com.xianzaishi.itemcenter.common.CodeFactoryUtils;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.msg.domain.RpcResult;
import com.xianzaishi.service.MessageService;
import com.xianzaishi.usercenter.client.user.UserService;
import com.xianzaishi.usercenter.client.user.dto.BaseUserDTO;
import com.xianzaishi.usercenter.client.user.dto.BaseUserDTO.UserStatusConstants;
import com.xianzaishi.usercenter.client.user.dto.UserDTO;
import com.xianzaishi.usercenter.client.userrelation.UserRelationService;
import com.xianzaishi.usercenter.client.userrelation.dto.UserRelationDTO;
import com.xianzaishi.usercenter.client.userrelation.query.UserRelationQuery;
import com.xianzaishi.wapcenter.client.mine.vo.BaseUserInfoVO;
import com.xianzaishi.wapcenter.client.mine.vo.MineVO;
import com.xianzaishi.wapcenter.client.mine.vo.UserInfoVO;
import com.xianzaishi.wapcenter.client.mine.vo.UserRegistVO;
import com.xianzaishi.wapcenter.client.mine.vo.UserRegistVO.RegistTypeContent;
import com.xianzaishi.wapcenter.component.UserSource;

/**
 * 用户中心接口实现
 * 
 * @author dongpo
 * 
 */
@Component("userCenterComponent")
public class UserCenterComponent {

  private static final Integer SEND_REGIST_CODE = 1;// 注册发送验证码
  private static final Integer SEND_FORGET_PWD_CODE = 2;// 忘记密码发送验证码
  private static final Logger LOGGER = Logger.getLogger(UserCenterComponent.class);
  private static final long DEADTIME = 600;
  private static final String TOKEN_KEY = "token";
  private static final String NEW_USER_KEY = "newUser";
  private static final String ANGLE_USER_KEY = "angleUser";
  private static final Long IOS_VERIFICATION_PHONE = 13058702740L;
  // private static final Short COUPON_TYPE = 11;
  // private static final Long COUPON_DEAD_TIME = 2592000000L;// 30天时间
  private static final String CHECK_CODE_TEMPLATE = "SMS_26080186";
  private static final String templatefile = "/usr/works/systemconfig/checkcodetemplate.properties";// 模板名文件


  @Autowired
  private UserService userService;

  @Autowired
  private MessageService messageService;

  @Autowired
  private UserCouponService userCouponService;

  @Autowired
  private UserRelationService userRelationService;

  @Autowired
  private SystemPropertyService systemPropertyService;

  /**
   * 登陆接口实现
   * 
   * @param
   * @return
   * @throws MalformedURLException
   */
  public String login(Long phone, String verificationCode, String hardwareCode) {
    LOGGER.info("登陆用户:" + phone);
    if (null == phone ) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "phone is null"));
    }
    if (null == verificationCode) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "verificationCode is null"));
    }
    Result<? extends BaseUserDTO> userResult = userService.queryUserByPhone(phone, true);
    if (null == userResult || !userResult.getSuccess() || null == userResult.getModule()) {
      return toJsonData(userResult);
    }
    UserDTO userDTO = (UserDTO) userResult.getModule();

    Boolean isAngleUser = checkUserTag(userDTO.getUserTag(), 0x2);// 是否是天使会员
    Boolean isNewUser = false;// 用户是否是第一次登陆
    if (null == userDTO.getPhone() || userDTO.getUserId() == 0) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.VERIFICATION_CODE_UNEXIST,
          "请先获取验证码"));
    } else if (null != userDTO.getPhone()
        && userDTO.getStatus().shortValue() != UserStatusConstants.USER_STATUS_REGISTRATION_COMPLETE
            .shortValue()) {
      UserRegistVO registVO = new UserRegistVO();
      registVO.setPhone(phone);
      registVO.setVerificationCode(verificationCode);
      Result<Long> registResult = regist(registVO);
      if (null == registResult || !registResult.getSuccess() || null == registResult.getModule()) {
        LOGGER.error("注册失败原因：" + JackSonUtil.getJson(registResult));
        return toJsonData(registResult);
      }
    } else {
      if (!checkCodeCorrect(verificationCode, userDTO.getActiveCode())) {// 验证码是否正确
        return toJsonData(Result.getErrDataResult(ServerResultCode.VERIFICATION_CODE_UNEXIST,
            "验证码错误"));
      } else if (userDTO.getPhone().longValue() != IOS_VERIFICATION_PHONE.longValue()
          && !checkCodeInTime(userDTO.getActiveCode())) {// 验证码是否过期
        return toJsonData(Result.getErrDataResult(ServerResultCode.VERIFICATION_CODE_PAST, "验证码过期"));
      }
    }
    Result<String> result = userService.userLogin(phone, hardwareCode);// 查询用户手机号密码是否正确，并返回用户登录token
    if (null == result || !result.getSuccess() || StringUtils.isEmpty(result.getModule())) {
      return toJsonData(result);
    }
    if (!checkUserTag(userDTO.getUserTag(), 0x8) && !isAngleUser) {// 判断用户userTag是否右起第四位为1,不是的话就表示用户第一次登陆app
      int couponPackageType = 5;// 后台固定的新人礼包id
      QueryParaDTO query = new QueryParaDTO();
      query.setPackageId(couponPackageType);
      query.setQueryType(QueryTypeConstants.BY_COUPON_PACKAGE);
      query.setUserId(userDTO.getUserId());
      query.setIncludeAllCoupon(true);
      Result<List<UserCouponDTO>> userCouponVOResult = userCouponService.getUserCoupon(query);

      SendParaDTO send = new SendParaDTO();
      send.setPackageId(couponPackageType);
      send.setSendType(SendTypeConstants.BY_COUPON_PACKAGE);
      send.setUserId(userDTO.getUserId());
      Result<Boolean> receivedCouponPackage = userCouponService.sendCoupon(send);// 给第一次登陆app的用户发放app
      if (null == receivedCouponPackage || !receivedCouponPackage.getSuccess()
          || !receivedCouponPackage.getModule()) {
        LOGGER.error(JackSonUtil.getJson(receivedCouponPackage));// 打印日志
      }

      if (null != receivedCouponPackage && null != receivedCouponPackage.getModule()
          && receivedCouponPackage.getModule()
          && CollectionUtils.isEmpty(userCouponVOResult.getModule())) {
        isNewUser = true;// 如果用户不是天使会员并且第一次登陆app标识该用户为新用户
      }
      LOGGER.info("发放新人大礼包结果：" + receivedCouponPackage.getModule());
      Integer userTag = userDTO.getUserTag();
      if (null == userTag) {
        userTag = 0;
      }
      userTag = userTag | 0x8;// 将用户标识为老用户
      userDTO.setUserTag(userTag);
//      userDTO.setAppversion(appversion);//app版本
//      userDTO.setEquipmentNumber(hardwareCode);//设备号
      Result<Boolean> updateResult = userService.updateUserProfileInfo(userDTO);// 更新数据库
      if (null == updateResult || !updateResult.getSuccess() || null == updateResult.getModule()) {
        LOGGER.error(JackSonUtil.getJson(updateResult));// 打印错误日志
      }
      LOGGER.info("更新用户标志位结果：" + updateResult.getModule());
    }
    Map<String, Object> map = Maps.newHashMap();
    map.put(TOKEN_KEY, result.getModule());
    map.put(NEW_USER_KEY, isNewUser);
    map.put(ANGLE_USER_KEY, isAngleUser);
    return toJsonData(Result.getSuccDataResult(map));
  }

  /**
   * 检查用户类型
   * 
   * @param userTag
   * @return
   */
  private boolean checkUserTag(Integer userTag, int tag) {
    if (null == userTag) {
      return false;
    }
    Boolean result = false;
    if ((userTag & tag) != 0) {// &运算后如果不是0就说明user为预期用户类型
      result = true;
    }
    return result;
  }

  /**
   * 用户中心根据token获取用户信息
   * 
   * @param token
   * @return
   */
  public String queryUserByToken(String token) {
    if (null == token) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "token is null"));
    }
    Result<BaseUserDTO> baseResult = userService.queryBaseUserByToken(token);// 根据token查询用户基本信息
    if (null == baseResult || !baseResult.getSuccess() || null == baseResult.getModule()) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.TOKEN_UNEXIST, "该用户不存在，请先登录"));
    }
    BaseUserDTO baseUserDTO = baseResult.getModule();// 根据token获取用户基本信息
    if (null == baseUserDTO.getUserId() || baseUserDTO.getUserId() == 0) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.TOKEN_UNEXIST, "该用户不存在，请先登录"));
    }
    Result<? extends BaseUserDTO> userResult =
        userService.queryUserByUserId(baseUserDTO.getUserId(), true);// 根据用户id查询用户全部信息
    LOGGER.info("获取用户信息结果：" + JackSonUtil.getJson(userResult));
    UserDTO userDTO = (UserDTO) userResult.getModule();// 获取全部的用户数据

    return toJsonData(Result.getSuccDataResult(getMineVO(userDTO)));// 返回用户中心数据
  }

  /**
   * 发送验证码逻辑
   * 
   * @param phone
   * @return
   */
  public String sendCheckCode(Long phone) {
    LOGGER.info("请求验证码用户：" + phone);
    if (null == phone || IOS_VERIFICATION_PHONE.longValue() == phone.longValue()) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "phone is null"));
    }
    Result<String> sendCheckCodeResult = getRegistCheckCode(phone);// 获取验证码
    if (sendCheckCodeResult != null && sendCheckCodeResult.getSuccess()) {
      String checkCode = sendCheckCodeResult.getModule();
      String templateName = getCheckCodeTemplate();
      if (null == templateName) {
        templateName = CHECK_CODE_TEMPLATE;
      }
      Map<String, String> checkCodeMap = Maps.newHashMap();
      checkCodeMap.put("code", checkCode);// 验证码
      Long timeStart = new Date().getTime();
      LOGGER.info("短信服务开始调用：");
      RpcResult<Boolean> result =
          messageService.sendSmsMessage(String.valueOf(phone), templateName, checkCodeMap);// TODO:设置一个超时
      if (null == result || !result.isSuccess()) {
        LOGGER.info(JackSonUtil.getJson(result));
        return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.SERVER_ERROR,
            "短信发送失败，请稍后再试"));
      }
      Long timeEnd = new Date().getTime();
      if (timeEnd - timeStart > 300) {
        LOGGER.info("短信服务调用结束，耗时：" + (timeEnd - timeStart));
      } else {
        LOGGER.info("短信服务调用结束。");
      }
      return toJsonData(Result.getSuccDataResult(true));
    }
    return toJsonData(sendCheckCodeResult);
  }

  /**
   * 获取模板名
   * 
   * @return
   */
  private String getCheckCodeTemplate() {
    FileReader reader = null;
    BufferedReader br = null;
    try {
      reader = new FileReader(templatefile);
      br = new BufferedReader(reader);
      String templateName = br.readLine();
      LOGGER.info(templateName);
      if (StringUtils.isNotBlank(templateName)) {
        return templateName.trim().split(",")[0];
      }
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (null != br) {
        try {
          br.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
      if (null != reader) {
        try {
          reader.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
    return null;
  }

  /**
   * 初始注册发送验证码
   * 
   * @param phone
   * @return
   */
  private Result<String> getRegistCheckCode(Long phone) {
    Result<? extends BaseUserDTO> userDTOResult = userService.queryUserByPhone(phone, true);// 查询数据库，判断要插入的数据是否在数据库中已经存在了
    Result<String> chekCodeResult = userService.sendUserPhoneCheckCode(phone, SEND_REGIST_CODE);// 获取随机验证码
    UserDTO userDTO = (UserDTO) userDTOResult.getModule();// 获取用户基本信息
    if (userDTO != null && userDTO.getUserId() != 0L) {
      String checkCode = chekCodeResult.getModule();// 获取验证码
      userDTO.setActiveCode(getActiveCode(checkCode));// 将验证码加工成激活码，设置激活码
      Result<Boolean> updateResult = userService.updateUserProfileInfo(userDTO);// 更新数据
      if (!updateResult.getSuccess() || !updateResult.getModule()) {
        return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "服务器异常，获取验证码失败");// 如果没有更新成功，则提示注册失败
      }
    } else if (userDTO.getUserId() == null || userDTO.getUserId() == 0L) {
      userDTO = getUserDTO(phone, chekCodeResult.getModule());// 发送验证码的同时插入数据，到提交的时候只需要更新数据库
      Result<Long> userIdResult = userService.registe(userDTO);// 插入数据并返回userId
      if ((userIdResult.getModule()).equals(0L) || !userIdResult.getSuccess()) {
        return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "服务器异常，获取验证码失败");
      }
    }

    return chekCodeResult;
  }

  /**
   * 注册接口实现
   * 
   * @param registVO
   * @return
   */
  public Result<Long> regist(UserRegistVO registVO) {
    if (null == registVO || null == registVO.getPhone()) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "注册手机号 为空 ");
    }
    LOGGER.info("注册用户：" + registVO.getPhone());
    Result<? extends BaseUserDTO> userDTOResult =
        userService.queryUserByPhone(registVO.getPhone(), true);// 获取用户全部数据
    if (null == userDTOResult || !userDTOResult.getSuccess() || null == userDTOResult.getModule()) {
      LOGGER.error("获取用户数据失败：" + JackSonUtil.getJson(userDTOResult));
      return Result.getErrDataResult(userDTOResult.getResultCode(), "获取用户数据失败，请先获取验证码");
    }
    UserDTO userDTO = (UserDTO) userDTOResult.getModule();
    LOGGER
        .info("是否通过888888验证的用户："
            + (registVO.getRegistType().shortValue() == RegistTypeContent.OFFLINE_REGIST
                .shortValue() && !registVO.getVerificationCode().equals("888888")));
    if (registVO.getRegistType().shortValue() != RegistTypeContent.OFFLINE_REGIST.shortValue()
        || (registVO.getRegistType().shortValue() == RegistTypeContent.OFFLINE_REGIST.shortValue() && !registVO
            .getVerificationCode().equals("888888"))) {
      if (!checkCodeCorrect(registVO.getVerificationCode(), userDTO.getActiveCode())) {// 验证码是否正确
        return Result.getErrDataResult(ServerResultCode.VERIFICATION_CODE_UNEXIST, "验证码错误");
      } else if (userDTO.getPhone().longValue() != IOS_VERIFICATION_PHONE.longValue()
          && !checkCodeInTime(userDTO.getActiveCode())) {// 验证码是否过期
        return Result.getErrDataResult(ServerResultCode.VERIFICATION_CODE_PAST, "验证码过期");
      }
    }
    if (registVO.getRegistType().shortValue() == RegistTypeContent.OFFLINE_REGIST.shortValue()) {
      Integer userTag = userDTO.getUserTag();
      if (null == userTag) {
        userTag = 0;
      }
      userDTO.setUserTag(userTag | 0x4);// 如果用户是线下注册，那么右起第三位标识为1
    } else if (registVO.getRegistType().shortValue() == RegistTypeContent.INVITE_REGIST
        .shortValue()) {
      Integer userTag = userDTO.getUserTag();
      if (null == userTag) {
        userTag = 0;
      }
      userDTO.setUserTag(userTag | 0x10);// 将用户标识为邀请用户
    }
    userDTO.setStatus(UserStatusConstants.USER_STATUS_REGISTRATION_COMPLETE);// 更新用户状态
    BaseUserDTO baseUserDTO = userDTO;
    Result<Boolean> updateUserBaseResult = userService.updateUserBaseInfo(baseUserDTO);// 更新用户基本信息
    Result<Boolean> updateUserProfileResult = userService.updateUserProfileInfo(userDTO);// 更新用户描述信息（里面需要更新用户状态）
    if (!updateUserBaseResult.getSuccess() || !updateUserBaseResult.getModule()
        || !updateUserProfileResult.getSuccess() || !updateUserProfileResult.getModule()) {
      LOGGER.error(JackSonUtil.getJson("更新数据库结果：" + updateUserBaseResult));
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "服务器错误");
    }
    return Result.getSuccDataResult(baseUserDTO.getUserId());
  }


  /**
   * 忘记密码时发送验证码
   * 
   * @param phone
   * @return
   */
  private Result<String> sendForgetPwdCheckCode(Long phone) {
    Result<? extends BaseUserDTO> userDTOResult = userService.queryUserByPhone(phone, true);// 查询数据库，判断要插入的数据是否在数据库中已经存在了
    Result<String> checkCodeResult =
        userService.sendUserPhoneCheckCode(phone, SEND_FORGET_PWD_CODE);// 获取验证码
    UserDTO userDTO = (UserDTO) userDTOResult.getModule();
    if (userDTO != null && userDTO.getUserId() != 0L
        && userDTO.getStatus().equals(UserStatusConstants.USER_STATUS_REGISTRATION_COMPLETE)) {// 用户存在
      userDTO.setActiveCode(getActiveCode(checkCodeResult.getModule()));// 更新激活码
      userService.updateUserProfileInfo(userDTO);
    } else {
      return Result.getErrDataResult(ServerResultCode.USER_UNEXIST, "用户不存在，请注册");
    }
    return checkCodeResult;
  }

  /**
   * 忘记密码修改密码
   * 
   * @param userRegistVO
   * @return
   */
  public String updateUserPwd(UserRegistVO userRegistVO) {
    if (null == userRegistVO) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "userRegistVO is null"));
    }
    String pwd = userRegistVO.getPwd();
    if (pwd != null && pwd.length() < 6) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.LOGIN_ERROR_PWD, "密码长度少于6位字符"));
    }
    Result<? extends BaseUserDTO> userDTOResult =
        userService.queryUserByPhone(userRegistVO.getPhone(), true);// 获取用户全部信息
    UserDTO userDTO = (UserDTO) userDTOResult.getModule();
    if (null == userDTO) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.USER_UNEXIST, "用户不存在，请注册"));
    }
    if (!checkCodeCorrect(userRegistVO.getVerificationCode(), userDTO.getActiveCode())) {
      return toJsonData(Result
          .getErrDataResult(ServerResultCode.VERIFICATION_CODE_UNEXIST, "验证码错误"));
    } else if (!checkCodeInTime(userDTO.getActiveCode())) {// 验证码过期判断
      return toJsonData(Result.getErrDataResult(ServerResultCode.VERIFICATION_CODE_PAST, "验证码过期"));
    }
    BaseUserDTO baseUserDTO = userDTO;// 向上转型
    baseUserDTO.setPwd(pwd);// 更新用户密码
    Result<Boolean> updateResult = userService.updateUserBaseInfo(baseUserDTO);
    return toJsonData(updateResult);// 返回是否更新成功

  }

  /**
   * 用户退出
   * 
   * @param token
   * @return
   */
  public String logout(String token) {
    if (null == token) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "token is null"));
    }
    Result<Boolean> deleteResult = userService.deleteUserToken(token);// 删除user_token表数据
    if (null == deleteResult || !deleteResult.getSuccess() || null == deleteResult.getModule()
        || !deleteResult.getModule()) {
      LOGGER.error("用户退出失败原因：" + JackSonUtil.getJson(deleteResult));
      return toJsonData(Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "服务器token未删除"));
    }

    return toJsonData(deleteResult);
  }

  /**
   * 验证码是否正确
   * 
   * @param registCode:注册表单验证码
   * @param activeCode:发送给客户端的验证码
   * @return
   */
  private boolean checkCodeCorrect(String registCode, String activeCode) {
    if (null == registCode || null == activeCode) {
      return false;
    }
    String code = (activeCode.split(";"))[0];// 激活码第一位为发送验证码
    if (registCode.equals(code)) {
      return true;
    }
    return false;
  }

  /**
   * 判断验证码是否过期
   * 
   * @param activeCode
   * @return
   */
  private boolean checkCodeInTime(String activeCode) {
    if (activeCode == null) {
      return false;
    }
    String sendTimeString = (activeCode.split(";"))[1];// 激活码第二位为发送验证码时间
    Long sendTime = Long.parseLong(sendTimeString);// 转换为long类型
    Long commitTime = new Date().getTime();// 获取当前时间
    long deadLine = (commitTime - sendTime) / 1000;// 获取验证码到提交验证码的时间差
    if (deadLine <= DEADTIME) {// 不超过10分钟
      return true;
    }
    return false;
  }

  /**
   * 获取用户信息数据userDTO
   * 
   * @param
   */
  private UserDTO getUserDTO(Long phone, String checkCode) {
    UserDTO userDTO = new UserDTO();
    userDTO.setPhone(phone);
    userDTO.setPwd("123");
    userDTO.setUserType(UserSource.userType);
    userDTO.setStatus(UserStatusConstants.USER_STATUS_REGISTERING);
    userDTO.setName(String.valueOf(phone));
    userDTO.setSex(UserSource.sex);
    userDTO.setUserTag(UserSource.userTag);
    userDTO.setActiveCode(getActiveCode(checkCode));// 设置用户激活码
    Random random = new Random();
    userDTO.setPic(UserSource.pic[random.nextInt(UserSource.pic.length)]);
    return userDTO;
  }

  /**
   * userDT到mineVO的拷贝
   * 
   * @param userDTO
   * @return
   */
  private MineVO getMineVO(UserDTO userDTO) {
    MineVO mineVO = new MineVO();
    BeanCopierUtils.copyProperties(userDTO, mineVO);
    if (null != mineVO.getBirthday()) {
      Long birthday = mineVO.getBirthday().getTime() / 1000L;
      mineVO.setBirthday(new Date(birthday));
    }
    mineVO.setCouponCount(0);// 接口暂时还没有，设为默认值
    mineVO.setMessageCount(0);// 同上
    return mineVO;
  }

  /**
   * 获取用户激活码
   * 
   * @param checkCode
   * @return
   */
  private String getActiveCode(String checkCode) {
    if (null == checkCode) {
      return null;
    }
    StringBuffer activeCode = new StringBuffer();
    activeCode.append(checkCode);
    activeCode.append(";");// 以;为分隔符
    long date = new Date().getTime();// 当前发送验证码时间
    activeCode.append(date);
    return activeCode.toString();
  }


  /**
   * 根据用户登陆token查询用户id
   */
  public String queryUserIdByToken(String token) {
    if (null == token) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "token为空"));
    }
    Result<BaseUserDTO> result = userService.queryBaseUserByToken(token);
    if (null == result || !result.getSuccess() || null == result.getModule()) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.TOKEN_UNEXIST, "该用户不存在"));
    }
    BaseUserDTO baseUserDTO = result.getModule();
    if (null == baseUserDTO.getUserId() || baseUserDTO.getUserId() == 0) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.TOKEN_UNEXIST, "用户不存在，请先登录"));
    }
    Long userId = baseUserDTO.getUserId();
    return toJsonData(Result.getSuccDataResult(userId));
  }

  /**
   * 更新用户信息
   * 
   * @param userInfoVO
   * @return
   */
  public String updateUserInfo(UserInfoVO userInfoVO) {
    if (null == userInfoVO || null == userInfoVO.getUserId()) {
      return toJsonData(Result
          .getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    LOGGER.info("更新用户id：" + userInfoVO.getUserId());

    BaseUserDTO baseUserDTO = new BaseUserDTO();
    UserDTO userDTO = new UserDTO();

    setUserInfo(userInfoVO, baseUserDTO, userDTO);
    Result<Boolean> updateResult = null;
    baseUserDTO.setStatus(UserStatusConstants.USER_STATUS_REGISTRATION_COMPLETE);
    if (null != baseUserDTO.getName()) {
      updateResult = userService.updateUserBaseInfo(baseUserDTO);
    }
    userDTO.setStatus(UserStatusConstants.USER_STATUS_REGISTRATION_COMPLETE);
    if (null != userInfoVO.getSex() || null != userInfoVO.getPic()
        || null != userInfoVO.getBirthday()) {
      updateResult = userService.updateUserProfileInfo(userDTO);
    }
    LOGGER.info("更新用户信息结果：" + JackSonUtil.getJson(updateResult));
    return toJsonData(updateResult);
  }

  /**
   * 设置用户信息
   * 
   * @param userInfoVO
   * @param baseUserDTO
   * @param userDTO
   */
  private void setUserInfo(UserInfoVO userInfoVO, BaseUserDTO baseUserDTO, UserDTO userDTO) {
    Long userId = userInfoVO.getUserId();
    baseUserDTO.setUserId(userId);
    userDTO.setUserId(userId);
    if (null != userInfoVO.getPic()) {
      userDTO.setPic(userInfoVO.getPic());
    }
    if (null != userInfoVO.getName()) {
      baseUserDTO.setName(userInfoVO.getName());
    }

    if (null != userInfoVO.getSex()) {
      userDTO.setSex(userInfoVO.getSex());
    }

    if (null != userInfoVO.getBirthday()) {
      Long timeStamp = Long.valueOf(userInfoVO.getBirthday()) * 1000L;
      userDTO.setBirthday(new Date(timeStamp));
    }

  }

  /**
   * 检验是否是鲜在时会员
   * 
   * @param phone
   * @return
   */
  public String checkMember(Long phone) {
    LOGGER.info("验证用户手机号：" + phone);
    Result<? extends BaseUserDTO> userResult = userService.queryUserByPhone(phone, false);
    BaseUserDTO baseUserDTO = userResult.getModule();
    if (null != baseUserDTO.getUserId()
        && baseUserDTO.getStatus().shortValue() == UserStatusConstants.USER_STATUS_REGISTRATION_COMPLETE
            .shortValue()) {
      return toJsonData(Result.getSuccDataResult(baseUserDTO.getUserId()));
    } else {
      return toJsonData(Result.getSuccDataResult(false));
    }
  }

  /**
   * 将传进来的数据转换为json数据
   * 
   * @param result
   * @return
   */
  public String toJsonData(Object result) {
    String jsonResult = JackSonUtil.getJson(result);
    return jsonResult;
  }

  /**
   * 发送活动页验证码
   * 
   * @param phone
   * @return
   */
  public String sendActivityCheckCode(Long phone, String userTag) {
    if (null == phone) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "phone is null"));
    }

    Result<String> sendCheckCodeResult = getActivityCheckCode(phone, userTag);
    if (sendCheckCodeResult != null && sendCheckCodeResult.getSuccess()) {
      String checkCode = sendCheckCodeResult.getModule();
      String templateName = getCheckCodeTemplate();
      if (null == templateName) {
        templateName = CHECK_CODE_TEMPLATE;
      }
      Map<String, String> checkCodeMap = Maps.newHashMap();
      checkCodeMap.put("code", checkCode);// 验证码
      Long timeStart = new Date().getTime();
      LOGGER.info("短信服务开始调用：");
      RpcResult<Boolean> result =
          messageService.sendSmsMessage(String.valueOf(phone), templateName, checkCodeMap);// TODO:设置一个超时
      if (null == result || !result.isSuccess() || !result.getData()) {
        LOGGER.info(JackSonUtil.getJson(result));
        return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.SERVER_ERROR,
            "短信发送失败，请稍后再试"));
      }
      Long timeEnd = new Date().getTime();
      if (timeEnd - timeStart > 300) {
        LOGGER.info("调用短信返回：" + JackSonUtil.getJson(result) + "短信服务调用结束，耗时："
            + (timeEnd - timeStart));
      } else {
        LOGGER.info("调用短信返回：" + JackSonUtil.getJson(result) + "短信服务调用结束。");
      }
      return toJsonData(Result.getSuccDataResult(true));
    }
    return toJsonData(sendCheckCodeResult);
  }

  /**
   * 获取活动页面注册验证码
   * 
   * @param phone
   * @param userTag
   * @return
   */
  private Result<String> getActivityCheckCode(Long phone, String userTag) {
    Result<? extends BaseUserDTO> userDTOResult = userService.queryUserByPhone(phone, true);// 查询数据库，判断要插入的数据是否在数据库中已经存在了

    Result<String> chekCodeResult = userService.sendUserPhoneCheckCode(phone, SEND_REGIST_CODE);// 获取随机验证码
    if (!chekCodeResult.getSuccess()) {
      return chekCodeResult;
    }
    UserDTO userDTO = null;
    if (null != userDTOResult && userDTOResult.getSuccess()) {
      userDTO = (UserDTO) userDTOResult.getModule();
    }
    Integer tag = getUserTag(userTag, userDTO.getUserTag());
    if (null != userDTO && null != userDTO.getUserId() && userDTO.getUserId() != 0) {
      userDTO.setUserTag(tag);
      userDTO.setActiveCode(getActiveCode(chekCodeResult.getModule()));
      Result<Boolean> updateResult = userService.updateUserProfileInfo(userDTO);// 更新数据
      if (!updateResult.getSuccess() || !updateResult.getModule()) {
        return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "服务器异常，获取验证码失败");// 如果没有更新成功，则提示注册失败
      }
    } else {
      userDTO = getUserDTO(phone, chekCodeResult.getModule());// 发送验证码的同时插入数据，到提交的时候只需要更新数据库
      userDTO.setUserTag(tag);
      Result<Long> userIdResult = userService.registe(userDTO);// 插入数据并返回userId
      if ((userIdResult.getModule()).equals(0L) || !userIdResult.getSuccess()) {
        return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "服务器异常，获取验证码失败");
      }
    }
    return chekCodeResult;
  }

  /**
   * 获取天使会员用户标志位
   * 
   * @param userTagString
   * @param userTag
   * @return
   */
  private Integer getUserTag(String userTagString, Integer userTag) {
    if (userTag != null) {
      return userTag | 1;
    }
    byte[] tag = new byte[8];
    for (int i = 0; i < 8; i++) {
      tag[i] = 0;
    }
    if (StringUtils.isNotEmpty(userTagString) && userTagString.equals(UserSource.angelMember)) {
      tag[7] = 1;
    }

    return bytes2Int(tag, 0, 8);
  }

  private int bytes2Int(byte[] b, int start, int len) {
    int sum = 0;
    int end = start + len;
    for (int i = start; i < end; i++) {
      int n = ((int) b[i]) & 0xff;
      n <<= (--len) * 8;
      sum += n;
    }
    return sum;
  }

  public String updateUserTag() throws IOException {
    List<Long> userIds = getUserIds();
    List<UserDTO> userDTOs = Lists.newArrayList();
    for (Long userId : userIds) {
      UserDTO updateDTO = new UserDTO();
      updateDTO.setUserId(userId);
      updateDTO.setUserTag(3);
      Result<Boolean> result = userService.updateUserProfileInfo(updateDTO);
      if (!result.getSuccess() || null == result.getModule()) {
        return toJsonData(result);
      }
      if (!result.getModule()) {
        userDTOs.add(updateDTO);
      }
    }
    Boolean result = false;
    if (CollectionUtils.isNotEmpty(userDTOs)) {
      for (UserDTO userDTO : userDTOs) {
        LOGGER.error(userDTO.getUserId() + " 未更新");
      }
    } else {
      result = true;
    }

    return toJsonData(Result.getSuccDataResult(result));
  }

  private List<Long> getUserIds() throws IOException {
    FileReader reader = new FileReader("/usr/works/target/request.txt");
    BufferedReader br = new BufferedReader(reader);
    String content = null;
    List<Long> userIds = Lists.newArrayList();
    while ((content = br.readLine()) != null) {
      Long userId = Long.valueOf(content);
      userIds.add(userId);
    }
    br.close();
    reader.close();
    return userIds;
  }

  /**
   * 根据userId查询用户信息
   * 
   * @param userId
   * @return
   */
  public String queryUserById(Long userId) {
    if (null == userId) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "user id为空"));
    }
    Result<? extends BaseUserDTO> result = userService.queryUserByUserId(userId, true);
    if (null == result || !result.getSuccess() || null == result.getModule()) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.TOKEN_UNEXIST, "该用户不存在"));
    }
    UserDTO userDTO = (UserDTO) result.getModule();
    if (userDTO.getUserId() == 0) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.TOKEN_UNEXIST, "该用户不存在"));
    }
    if (StringUtils.isEmpty(userDTO.getInviteCode())) {
      userDTO.setInviteCode(CodeFactoryUtils.createInviteCode(userId));
      Result<Boolean> updateResult = userService.updateUserProfileInfo(userDTO);
      if (null == updateResult || !updateResult.getSuccess() || null == updateResult.getModule()
          || !updateResult.getModule()) {
        LOGGER.error("更新用户邀请码失败：" + JackSonUtil.getJson(updateResult));
        return toJsonData(Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR,
            "服务器错误，获取用户邀请码失败！"));
      }
    }
    BaseUserInfoVO baseUserInfoVO = new BaseUserInfoVO();
    BeanCopierUtils.copyProperties(userDTO, baseUserInfoVO);
    return toJsonData(Result.getSuccDataResult(baseUserInfoVO));
  }

  /**
   * 用户优惠券信息
   * 
   * @param phone
   * @return
   * @throws ParseException
   */
  public String queryCouponInfo(Long phone) throws ParseException {
    if (null == phone) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "手机号为空"));
    }
    Result<? extends BaseUserDTO> result = userService.queryUserByPhone(phone, true);
    if (null == result || !result.getSuccess() || null == result.getModule()) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.USER_UNEXIST, "该用户不存在"));
    }
    Map<String, Object> map = Maps.newHashMap();
    UserDTO userDTO = (UserDTO) result.getModule();
    if (userDTO.getUserId() == 0) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.USER_UNEXIST, "该用户不存在"));
    }
    Integer tag = userDTO.getUserTag();
    StringBuffer sb = new StringBuffer();
    if (checkUserTag(tag, 0x2)) {
      sb.append("天使会员");
    } else {
      if (checkUserTag(tag, 0x8)) {
        sb.append("老用户");
      } else {
        sb.append("新用户");
      }
    }
    QueryParaDTO query = new QueryParaDTO();
    query.setUserId(userDTO.getUserId());
    query.setIncludeAllCoupon(true);
    Result<List<UserCouponDTO>> userCouponResult = userCouponService.getUserCoupon(query);
    List<String> coupons = Lists.newArrayList();
    if (null != userCouponResult && userCouponResult.getSuccess()
        && null != userCouponResult.getModule()) {
      List<UserCouponDTO> userCouponVOs = userCouponResult.getModule();
      for (UserCouponDTO userCouponVO : userCouponVOs) {
        if (userCouponVO.getStatus() == 1) {
          coupons.add(userCouponVO.getCouponTitle());
        }
      }
    }
    map.put("tag", sb);// 用户标签
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Long time = Long.valueOf(userDTO.getActiveCode().split(";")[1]);
    map.put("userId", userDTO.getUserId());// 用户id
    map.put("loginTime", sdf.format(time));// 上次登陆时间
    map.put("userCoupons", coupons);// 优惠券
    map.put("checkCode", userDTO.getActiveCode().split(";")[0]);// 最新验证码
    return JackSonUtil.getJson(Result.getSuccDataResult(map));
  }

  /**
   * 被邀请用户注册
   * 
   * @param userRegistVO
   * @return
   */
  public Result<Long> inviteeRegist(UserRegistVO userRegistVO) {
    if (null == userRegistVO || null == userRegistVO.getPhone()) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "注册手机号 为空 ");
    }
    if (null == userRegistVO.getRegistType()) {
      userRegistVO.setRegistType(RegistTypeContent.INVITE_REGIST.shortValue());
    }
    Result<? extends BaseUserDTO> inviteeResult =
        userService.queryUserByPhone(userRegistVO.getPhone(), false);// 查询被邀请用户是否已经是鲜在时会员
    BaseUserDTO baseUserDTO = inviteeResult.getModule();
    if (null != baseUserDTO
        && baseUserDTO.getUserId() != 0
        && baseUserDTO.getStatus().shortValue() == UserStatusConstants.USER_STATUS_REGISTRATION_COMPLETE
            .shortValue()) {
      // 如果是的话则提示用户
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_DATA_AREADY_EXIST,
          "您已是鲜在时会员，请勿重复注册");
    }

    // 根据唯一的邀请码查询邀请人信息
    Result<BaseUserDTO> baseUserResult =
        userService.queryBaseUserByInviteCode(userRegistVO.getInviteCode());
    if (null == baseUserResult || !baseUserResult.getSuccess()
        || null == baseUserResult.getModule()) {
      LOGGER.error(JackSonUtil.getJson(baseUserResult));
      // 查询不到则提示用户
      return Result.getErrDataResult(baseUserResult.getResultCode(), "该邀请码已过期！");
    }
    Long userId = baseUserResult.getModule().getUserId();
    Result<Long> userResult = regist(userRegistVO);// 被邀请用户注册
    if (null == userResult || !userResult.getSuccess() || null == userResult.getModule()) {
      return userResult;
    }
    // 获取用户关系信息
    UserRelationDTO userRelationDTO = getUserRelation(userId, userResult.getModule());
    // 查询该用户是否已经存在用户关系表了
    UserRelationQuery query = new UserRelationQuery();
    query.setUserId(userId);
    query.setRelatedId(userResult.getModule());
    query.setType(1);
    query.setStatus((short) 1);
    // 查询用户关系表
    Result<List<UserRelationDTO>> userRelationResult =
        userRelationService.queryUserRelationByQuery(query);
    // 如果存在则更新，否则插入
    if (CollectionUtils.isNotEmpty(userRelationResult.getModule())) {
      userRelationDTO.setId(userRelationResult.getModule().get(0).getId());
      Result<Boolean> updateResult = userRelationService.updateUserRelation(userRelationDTO);
      if (!updateResult.getSuccess() || null == updateResult.getModule()
          || !updateResult.getModule()) {
        LOGGER.error("更新用户关系出错：" + JackSonUtil.getJson(updateResult));
      }
    } else {
      Result<Boolean> inserResult = userRelationService.insertUserRelation(userRelationDTO);
      if (!inserResult.getSuccess() || null == inserResult.getModule() || !inserResult.getModule()) {
        LOGGER.error("添加用户关系失败：" + JackSonUtil.getJson(inserResult));
      }
    }

    int sendPackageId = 8;// 后台针对老带新，固定配置8为key的优惠券组
    SendParaDTO send = new SendParaDTO();
    send.setPackageId(sendPackageId);
    send.setSendType(SendTypeConstants.BY_COUPON_PACKAGE);
    send.setUserId(userRelationDTO.getRelatedId());
    Result<Boolean> sendCoupons = userCouponService.sendCoupon(send);
    synchronized (userId) {
      if (null == sendCoupons || !sendCoupons.getSuccess() || null == sendCoupons.getModule()
          || !sendCoupons.getModule()) {
        // 如果发送优惠券失败，再次发送
        LOGGER.error("重新发送优惠券结果：" + JackSonUtil.getJson(userCouponService.sendCoupon(send)));
      }
    }

    return userResult;
  }

  /**
   * 设置用户关系信息
   * 
   * @param userId
   * @param relatedId
   * @return
   */
  public UserRelationDTO getUserRelation(Long userId, Long relatedId) {
    UserRelationDTO userRelationDTO = new UserRelationDTO();
    userRelationDTO.setUserId(userId);
    userRelationDTO.setRelatedId(relatedId);
    userRelationDTO.setRelationship("邀请与被邀请关系");
    userRelationDTO.setType(1);
    userRelationDTO.setStatus((short) 1);
    userRelationDTO.setChannel(0);
    userRelationDTO.setRelationTag(0);
    return userRelationDTO;
  }
}
