package com.xianzaishi.wapcenter.component.mine;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.usercenter.client.user.UserService;
import com.xianzaishi.usercenter.client.user.dto.BaseUserDTO;
import com.xianzaishi.usercenter.client.user.dto.UserDTO;

@Component("userBaseComponent")
public class UserBaseComponent {

  private static final Logger LOGGER = Logger.getLogger(UserBaseComponent.class);

  @Autowired
  protected UserService userService;

  /**
   * 用户中心根据token获取用户信息
   * 
   * @param token
   * @return
   */
  public BaseUserDTO queryUserByToken(String token) {
    if (null == token) {
      LOGGER.error("token is null");
      return null;
    }
    Result<BaseUserDTO> baseResult = userService.queryBaseUserByToken(token);// 根据token查询用户基本信息
    if (null == baseResult || !baseResult.getSuccess() || null == baseResult.getModule()) {
      LOGGER.error("该用户不存在，请先登录,token:"+token);
      return null;
    }
    BaseUserDTO baseUserDTO = baseResult.getModule();// 根据token获取用户基本信息
    if (null == baseUserDTO.getUserId() || baseUserDTO.getUserId() == 0) {
      LOGGER.error("该用户不存在，请先登录,token:"+token);
      return null;
    }
    Result<? extends BaseUserDTO> userResult =
        userService.queryUserByUserId(baseUserDTO.getUserId(), true);// 根据用户id查询用户全部信息
    LOGGER.info("获取用户信息结果：" + JackSonUtil.getJson(userResult));
    BaseUserDTO userDTO = (UserDTO) userResult.getModule();// 获取全部的用户数据
    return userDTO;// 返回用户中心数据
  }
  
  /**
   * 用户中心根据id获取用户信息
   * 
   * @param token
   * @return
   */
  public BaseUserDTO queryUserById(long userId) {
    if (userId <= 0) {
      LOGGER.error("id is error");
      return null;
    }
    Result<? extends BaseUserDTO> baseResult = userService.queryUserByUserId(userId, false);// 根据token查询用户基本信息
    if (null == baseResult || !baseResult.getSuccess() || null == baseResult.getModule()) {
      LOGGER.error("该用户不存在，请先登录,userId:"+userId);
      return null;
    }
    BaseUserDTO baseUserDTO = baseResult.getModule();// 根据token获取用户基本信息
    if (null == baseUserDTO.getUserId() || baseUserDTO.getUserId() == 0) {
      LOGGER.error("该用户不存在，请先登录,userId:"+userId);
      return null;
    }
    Result<? extends BaseUserDTO> userResult =
        userService.queryUserByUserId(baseUserDTO.getUserId(), true);// 根据用户id查询用户全部信息
    LOGGER.info("获取用户信息结果：" + JackSonUtil.getJson(userResult));
    BaseUserDTO userDTO = (BaseUserDTO) userResult.getModule();// 获取全部的用户数据
    return userDTO;// 返回用户中心数据
  }
}
