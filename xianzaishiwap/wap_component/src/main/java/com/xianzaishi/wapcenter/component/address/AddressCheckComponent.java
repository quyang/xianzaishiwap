package com.xianzaishi.wapcenter.component.address;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.wapcenter.client.address.vo.AddressGeocodeVO;
import com.xianzaishi.wapcenter.client.address.vo.AddressResultVO;

/**
 * 地址验证
 * 
 * @author dongpo
 * 
 */
@Component("addressCheckComponent")
public class AddressCheckComponent {
  private static final Logger LOGGER = Logger.getLogger(AddressCheckComponent.class);
  
  private static final String IBS_KEY = "8d0f9c46621862f005d11e6f1d4be412";// 高德地图key
  
  private static final String IBS_URL = "http://restapi.amap.com/v3/geocode/geo";// 高德地图API地址
  
  private static final String KYE_KEY = "key";//key键
  private static final String OUTPUT_KEY = "output";//output键
  private static final String ADDRESS_KEY = "address";//address键

  private static final Integer CONNECTION_TIMEOUT = 10000;//链接超时限制

  /**
   * 验证地址（POI）正确性
   * @param address
   * @return 返回locatio坐标
   */
  public String checkAddress(String address) {
    if (StringUtils.isEmpty(address)) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    List<NameValuePair> params = Lists.newArrayList();//请求高德API参数
    params.add(new BasicNameValuePair(KYE_KEY, IBS_KEY));
    params.add(new BasicNameValuePair(OUTPUT_KEY, "JSON"));
    params.add(new BasicNameValuePair(ADDRESS_KEY, address));
    StringBuffer urlWithParams = new StringBuffer();//带参数的url
    urlWithParams.append(IBS_URL);
    StringBuffer sb = new StringBuffer();
    sb.append("?");
    Iterator<NameValuePair> item = params.iterator();
    while (item.hasNext()) {
      sb.append(item.next());// 连接参数
      if (item.hasNext()) {
        sb.append("&");
      }
    }
    sb.toString().replace(':', '=');//将kv对转换为赋值形式
    urlWithParams.append(sb);
    AddressResultVO addressResultVO =
        (AddressResultVO) JackSonUtil.jsonToObject(doHttpGet(urlWithParams.toString()),
            AddressResultVO.class);
    String location = null;
    if (null != addressResultVO && addressResultVO.getStatus() == "1"
        && addressResultVO.getCount() == "1") {
      AddressGeocodeVO addressGeocodeVO = addressResultVO.getGeocodes().get(0);
      location = addressGeocodeVO.getLocation();
    }
    return location;
  }

  /**
   * get请求高德地图API
   * 
   * @param urlWithParams
   * @return 返回响应结果
   */
  public String doHttpGet(String urlWithParams) {
    CloseableHttpClient httpclient = HttpClients.createDefault();
    HttpGet httpget = new HttpGet(urlWithParams);

    // 配置请求的超时设置
    RequestConfig requestConfig =
        RequestConfig.custom().setConnectionRequestTimeout(CONNECTION_TIMEOUT)
            .setConnectTimeout(CONNECTION_TIMEOUT).setSocketTimeout(CONNECTION_TIMEOUT).build();
    httpget.setConfig(requestConfig);

    CloseableHttpResponse response;
    String result = null;
    try {
      response = httpclient.execute(httpget);
      LOGGER.info("StatusCode -> " + response.getStatusLine().getStatusCode());

      HttpEntity entity = response.getEntity();
      String jsonStr = EntityUtils.toString(entity, Charset.forName("UTF-8"));
      LOGGER.info(jsonStr);
      result = jsonStr;
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      httpget.releaseConnection();
    }
    return result;
  }

}
