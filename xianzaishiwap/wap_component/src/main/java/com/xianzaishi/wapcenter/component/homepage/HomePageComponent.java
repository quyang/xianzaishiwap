package com.xianzaishi.wapcenter.component.homepage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.itemcenter.client.item.CommodityService;
import com.xianzaishi.itemcenter.client.item.dto.ItemBaseDTO.ItemStatusConstants;
import com.xianzaishi.itemcenter.client.item.dto.ItemBaseDTO.SaleStatusConstants;
import com.xianzaishi.itemcenter.client.item.dto.ItemDTO;
import com.xianzaishi.itemcenter.client.item.query.ItemListQuery;
import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.client.sysproperty.SystemPropertyService;
import com.xianzaishi.itemcenter.client.sysproperty.dto.SystemConfigDTO;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.pic.PicUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchasecenter.client.activity.ActivityPageService;
import com.xianzaishi.purchasecenter.client.activity.dto.ActivityPageDTO;
import com.xianzaishi.purchasecenter.client.activity.dto.ActivityStepCatDTO;
import com.xianzaishi.purchasecenter.client.activity.dto.ActivityStepContentDTO;
import com.xianzaishi.purchasecenter.client.activity.dto.ActivityStepDTO;
import com.xianzaishi.purchasecenter.client.activity.dto.ActivityStepItemDTO;
import com.xianzaishi.purchasecenter.client.activity.dto.ActivityStepPicDTO;
import com.xianzaishi.wapcenter.client.activity.vo.ActivityStepCatVO;
import com.xianzaishi.wapcenter.client.activity.vo.ActivityStepContentVO;
import com.xianzaishi.wapcenter.client.activity.vo.ActivityStepPicVO;
import com.xianzaishi.wapcenter.client.activity.vo.ActivityStepVO;
import com.xianzaishi.wapcenter.client.activity.vo.HomePageResult;
import com.xianzaishi.wapcenter.client.market.vo.BaseItemVO;
import com.xianzaishi.wapcenter.component.itemlist.ItemListComponent;

/**
 * 首页和活动页实现类
 * 
 * @author dongpo
 * 
 */
@Component("homePageComponent")
public class HomePageComponent {

  private static final Logger LOGGER = Logger.getLogger(HomePageComponent.class);

  public static final String stepfile = "/usr/works/systemconfig/step.properties";
  public static final String stepProperties = "_step.properties";
  private static final String ITEM_LIST_KEY = "itemList";
  private static final String HTTPHEAD = "http://";
  private static final String HTTPSHEAD = "https://";

  @Autowired
  private ActivityPageService activityPageService;

  @Autowired
  private CommodityService commodityService;

  @Autowired
  private ItemListComponent itemListComponent;

  @Autowired
  private SystemPropertyService systemPropertyService;

  @Autowired
  private SkuService skuService;
  
  /**
   * 获取首页或者活动页数据
   * 
   * @param pageId
   * @param hasAllFloor
   * @return
   * @throws IOException
   */
  public String queryHomePage(Integer pageId, Boolean hasAllFloor, int version) throws IOException {
    if (null == pageId) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "pageId 为空"));
    }
    if (!(pageId.equals(1) || pageId.equals(30))) {
      return getHomePage(pageId, null);
    }
    List<Integer> floorIds = new ArrayList<>();
    StringBuffer stepId = new StringBuffer();
    Result<SystemConfigDTO> systemConfig = systemPropertyService.getSystemConfig(4);
    if (null != systemConfig && systemConfig.getSuccess() && null != systemConfig.getModule()) {
      String[] configInfos = systemConfig.getModule().getConfigInfo().split(",");
      for (String line : configInfos) {
        stepId.append(line.trim());
        stepId.append("\n");
      }
    }
    if (StringUtils.isNotEmpty(stepId.toString())) {
      Map<String, String> floors = getStepIds(stepId.toString());
      LOGGER.info("楼层段数：" + floors.size());
      if (MapUtils.isNotEmpty(floors)) {
        String floorsKey = pageId + "_" + version;
        if (!hasAllFloor) {
          if (null != floors.get(floorsKey + "_f")) {
            String[] firstStepIds = floors.get(floorsKey + "_f").split("\\\\");
            for (int i = 0; i < firstStepIds.length; i++) {
              floorIds.add(Integer.parseInt(firstStepIds[i]));
            }
          }
        } else {
          if (null != floors.get(floorsKey + "_s")) {
            String[] secondStepIds = floors.get(floorsKey + "_s").split("\\\\");
            for (int i = 0; i < secondStepIds.length; i++) {
              floorIds.add(Integer.parseInt(secondStepIds[i]));
            }
          }
        }
      }
    } else {// 容错逻辑
      floorIds = getDefaultStep(pageId, version, hasAllFloor);
    }
    return getHomePage(pageId, floorIds);
    // Result<ActivityPageDTO> homePageResult = activityPageService.getPage(pageId, floorIds);
    // if (!homePageResult.getSuccess()) {
    // return toJsonData(homePageResult);
    // }
    // ActivityPageDTO activityPageDTO = homePageResult.getModule();
    // if (null == activityPageDTO) {
    // return toJsonData(Result.getErrDataResult(ServerResultCode.AVTIVITY_PAGE_NOT_EXIT_ERROR,
    // "页面不存在"));
    // }
    // List<ActivityStepDTO> activityStepDTOs = activityPageDTO.getStepList();
    // if (null == activityStepDTOs || activityStepDTOs.size() == 0) {
    // return toJsonData(Result.getErrDataResult(ServerResultCode.AVTIVITY_PAGE_NOT_EXIT_ERROR,
    // "楼层数据为空"));
    // }
    // List<ActivityStepVO> activityStepVOs = Lists.newArrayList();
    // BeanCopierUtils.copyListBean(activityStepDTOs, activityStepVOs, ActivityStepVO.class);
    // for (ActivityStepDTO activityStepDTO : activityStepDTOs) {
    // int index = activityStepDTOs.indexOf(activityStepDTO);
    // ActivityStepVO activityStepVO = activityStepVOs.get(index);//
    // activityStepVOs的size和activityStepDTOs的size一样，所以不会数组越界
    // List<ActivityStepPicDTO> activityStepPicDTOs = activityStepDTO.getPicList();
    // List<ActivityStepItemDTO> activityStepItemDTOs = activityStepDTO.getItemList();
    // if (null != activityStepPicDTOs && activityStepPicDTOs.size() != 0) {
    // List<ActivityStepPicVO> activityStepPicVOs = getStepPic(activityStepPicDTOs);// 获取楼层图片数据
    // activityStepVO.setPicList(activityStepPicVOs);
    // }
    //
    // if (null != activityStepItemDTOs && activityStepItemDTOs.size() != 0) {
    // List<BaseItemVO> baseItemVOs = getStepItem(activityStepItemDTOs);// 获取楼层商品数据
    // if (null != baseItemVOs && baseItemVOs.size() != 0) {
    // activityStepVO.setItems(baseItemVOs);
    // }
    // }
    // activityStepVO.setPageName(activityPageDTO.getTitle());
    // }
    //
    //
    // return toJsonData(Result.getSuccDataResult(activityStepVOs));
  }

  /**
   * 默认情况下的楼层数据
   * 
   * @return
   */
  private List<Integer> getDefaultStep(int pageId, int version, Boolean hasAllFloor) {
    List<Integer> floorIds = Lists.newArrayList();
    if (pageId == 30) {
      if (version == 0) {
        if (!hasAllFloor) {
          floorIds.add(134);
          floorIds.add(135);
          floorIds.add(137);
          floorIds.add(138);
          floorIds.add(139);
          floorIds.add(140);
          floorIds.add(155);
        } else {
          floorIds.add(143);
          floorIds.add(147);
          floorIds.add(144);
          floorIds.add(145);
          floorIds.add(146);
          floorIds.add(156);
          floorIds.add(157);
          floorIds.add(158);
          floorIds.add(159);
          floorIds.add(160);
          floorIds.add(161);
          floorIds.add(162);
          floorIds.add(148);
          floorIds.add(149);
        }
      } else if (version == 1) {
        if (!hasAllFloor) {
          floorIds.add(134);
          floorIds.add(160);
          floorIds.add(164);
          floorIds.add(135);
          floorIds.add(10);
          floorIds.add(161);
        } else {
          floorIds.add(143);
          floorIds.add(144);
          floorIds.add(145);
          floorIds.add(146);
          floorIds.add(147);
          floorIds.add(148);
          floorIds.add(163);
          floorIds.add(157);
          floorIds.add(158);
          floorIds.add(159);
          floorIds.add(162);
          floorIds.add(149);
        }
      }
    } else {
      if (!hasAllFloor) {
        floorIds.add(13);
        floorIds.add(14);
        floorIds.add(15);
      } else {
        floorIds.add(16);
        floorIds.add(17);
        floorIds.add(18);
        floorIds.add(42);
        floorIds.add(43);
        floorIds.add(44);
        floorIds.add(19);
        floorIds.add(20);
      }
    }
    return floorIds;
  }

  private Map<String, String> getStepIds(String datas) {
    if (StringUtils.isBlank(datas)) {
      return null;
    }
    Map<String, String> map = Maps.newHashMap();
    String[] steps = datas.split("\n");
    for (String step : steps) {
      String[] kv = step.split("=");
      map.put(kv[0], kv[1]);
    }
    return map;
  }

  /**
   * 获取楼层图片
   * 
   * @param activityStepPicDTOs
   * @return
   */
  private List<ActivityStepPicVO> getStepPic(List<ActivityStepPicDTO> activityStepPicDTOs) {
    List<ActivityStepPicVO> activityStepPicVOs = Lists.newArrayList();
    BeanCopierUtils.copyListBean(activityStepPicDTOs, activityStepPicVOs, ActivityStepPicVO.class);
    for(ActivityStepPicVO pic:activityStepPicVOs){
      String targetId = pic.getTargetId();
      if(StringUtils.isNotEmpty(targetId)){
        targetId = targetId.trim();
        if(targetId.startsWith(HTTPHEAD));{
          targetId = targetId.replaceFirst(HTTPHEAD, HTTPSHEAD);
        }
        pic.setTargetId(targetId);
      }
      String picUrl = pic.getPicUrl();
      if(StringUtils.isNotEmpty(picUrl)){
        picUrl = picUrl.trim();
        if(picUrl.startsWith(HTTPHEAD));{
          picUrl = picUrl.replaceFirst(HTTPHEAD, HTTPSHEAD);
        }
        pic.setPicUrl(picUrl);
      }
    }
    return activityStepPicVOs;
  }

  /**
   * 获取文案楼层
   * 
   * @param activityStepPicDTOs
   * @return
   */
  private List<ActivityStepContentVO> getStepContent(
      List<ActivityStepContentDTO> activityStepContentDTOs) {
    List<ActivityStepContentVO> activityStepContentVOs = Lists.newArrayList();
    BeanCopierUtils.copyListBean(activityStepContentDTOs, activityStepContentVOs,
        ActivityStepContentVO.class);
    return activityStepContentVOs;
  }

  /**
   * 获取类目数据
   * 
   * @param activityStepCatDTOs
   * @return
   */
  private List<ActivityStepCatVO> getStepCat(List<ActivityStepCatDTO> activityStepCatDTOs) {
    List<ActivityStepCatVO> activityStepPicVOs = Lists.newArrayList();
    BeanCopierUtils.copyListBean(activityStepCatDTOs, activityStepPicVOs, ActivityStepCatVO.class);
    return activityStepPicVOs;
  }

  /**
   * 获取楼层商品数据
   * 
   * @param activityStepItemDTOs
   * @return
   */
  private List<BaseItemVO> getStepItem(List<ActivityStepItemDTO> activityStepItemDTOs) {
    List<BaseItemVO> baseItemVOs = Lists.newArrayList();
    List<Long> itemIds = Lists.newArrayList();
    for (ActivityStepItemDTO activityStepItemDTO : activityStepItemDTOs) {
      Long itemId = activityStepItemDTO.getItemId();
      itemIds.add(itemId);
    }
    ItemListQuery itemListQuery = new ItemListQuery();
    itemListQuery.setPageNum(0);
    itemListQuery.setPageSize(100);
    itemListQuery.setItemIds(itemIds);
    List<Short> saleStatus = Lists.newArrayList();
    saleStatus.add(SaleStatusConstants.SALE_ONLY_ONLINE);
    saleStatus.add(SaleStatusConstants.SALE_ONLINE_AND_MARKET);
    itemListQuery.setSaleStatusList(saleStatus);
    itemListQuery.setStatus(ItemStatusConstants.ITEM_STATUS_PASS_AND_UP_SHELVES);
    Result<Map<String, Object>> commodityResult = commodityService.queryCommodies(itemListQuery);
    if (!commodityResult.getSuccess() || null == commodityResult.getModule()) {
      LOGGER.error(JackSonUtil.getJson(commodityResult));
      return null;
    }
    Map<String, Object> map = commodityResult.getModule();
    List<ItemDTO> itemDTOs = (List<ItemDTO>) map.get(ITEM_LIST_KEY);
    if (null == itemDTOs || itemDTOs.size() == 0) {
      LOGGER.info("商品数据为空");
      return null;
    }
    itemIds.clear();
    for (ItemDTO itemDTO : itemDTOs) {
      itemIds.add(itemDTO.getItemId());
    }
    baseItemVOs = itemListComponent.itemDTOs2BaseItemVOs(itemDTOs);// 获取商品列表，商品本身就有图片
    for (ActivityStepItemDTO activityStepItemDTO : activityStepItemDTOs) {
      if (!itemIds.contains(activityStepItemDTO.getItemId())) {
        continue;
      }
      int index = itemIds.indexOf(activityStepItemDTO.getItemId());
      if (StringUtils.isNotEmpty(activityStepItemDTO.getPicUrl()) && index < baseItemVOs.size()) {// 如果活动页有商品图片，那么原来的商品图片替换成活动页配的商品图片
        baseItemVOs.get(index).setPicUrl(activityStepItemDTO.getPicUrl());
      }
      String picUrl =  baseItemVOs.get(index).getPicUrl();
      if(null != picUrl && picUrl.startsWith(HTTPHEAD)){
        picUrl = picUrl.replaceFirst(HTTPHEAD, HTTPSHEAD);
        baseItemVOs.get(index).setPicUrl(picUrl);
      }
    }
    return baseItemVOs;
  }

  /**
   * 获取楼层商品数据
   * 
   * @param activityStepItemDTOs
   * @return
   */
  public List<BaseItemVO> getItemBySkuIds(List<Long> skuIds) {
    if(CollectionUtils.isEmpty(skuIds)){
      return Collections.emptyList();
    }else{
      SkuQuery skuQ = SkuQuery.querySkuBySkuIds(skuIds);
      Result<List<ItemCommoditySkuDTO>> skuListResult = skuService.queryItemSkuList(skuQ);
      if(null == skuListResult || !skuListResult.getSuccess() || CollectionUtils.isEmpty(skuListResult.getModule())){
        return Collections.emptyList();
      }
      List<Long> itemIds =Lists.newArrayList();
      for(ItemCommoditySkuDTO sku:skuListResult.getModule()){
        itemIds.add(sku.getItemId());
      }
      
      ItemListQuery itemListQuery = new ItemListQuery();
      itemListQuery.setPageNum(0);
      itemListQuery.setPageSize(100);
      itemListQuery.setItemIds(itemIds);
      List<Short> saleStatus = Lists.newArrayList();
      saleStatus.add(SaleStatusConstants.SALE_ONLY_ONLINE);
      saleStatus.add(SaleStatusConstants.SALE_ONLINE_AND_MARKET);
      itemListQuery.setSaleStatusList(saleStatus);
      itemListQuery.setStatus(ItemStatusConstants.ITEM_STATUS_PASS_AND_UP_SHELVES);
      Result<Map<String, Object>> commodityResult = commodityService.queryCommodies(itemListQuery);
      if (!commodityResult.getSuccess() || null == commodityResult.getModule()) {
        LOGGER.error(JackSonUtil.getJson(commodityResult));
        return null;
      }
      Map<String, Object> map = commodityResult.getModule();
      List<ItemDTO> itemDTOs = (List<ItemDTO>) map.get(ITEM_LIST_KEY);
      if (null == itemDTOs || itemDTOs.size() == 0) {
        LOGGER.info("商品数据为空");
        return null;
      }
      List<BaseItemVO> baseItemVOs = itemListComponent.itemDTOs2BaseItemVOs(itemDTOs);// 获取商品列表，商品本身就有图片
      for(BaseItemVO itemVO:baseItemVOs){
        itemVO.setPicUrl(PicUtil.processChangeHttpsHttpPicHeader(itemVO.getPicUrl(), 1));
      }
      return baseItemVOs;
    }
  }

  /**
   * 将传进来的数据转换为json数据
   * 
   * @param result
   * @return
   */
  public String toJsonData(Object result) {
    String jsonResult = JackSonUtil.getJson(result);
    return jsonResult;
  }

  /**
   * 根据id列表查询楼层数据
   * 
   * @param pageId
   * @param stepIds，注：stepId=1代表类目楼层，属于独立楼层，单独取数据查询
   * @return
   */
  public String getHomePage(Integer pageId, List<Integer> stepIds) {
    if (null == pageId) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }

    Result<ActivityPageDTO> homePageResult = activityPageService.getPage(pageId, stepIds);
    if (!homePageResult.getSuccess()) {
      return toJsonData(homePageResult);
    }
    ActivityPageDTO activityPageDTO = homePageResult.getModule();
    if (null == activityPageDTO) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.AVTIVITY_PAGE_NOT_EXIT_ERROR,
          "请求页面不存在"));
    }
    List<ActivityStepDTO> activityStepDTOs = activityPageDTO.getStepList();
    if (CollectionUtils.isEmpty(activityStepDTOs)) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.AVTIVITY_PAGE_NOT_EXIT_ERROR,
          "楼层数据为空"));
    }
    List<ActivityStepVO> activityStepVOs = Lists.newArrayList();
    // BeanCopierUtils.copyListBean(activityStepDTOs, activityStepVOs, ActivityStepVO.class);
    for (ActivityStepDTO activityStepDTO : activityStepDTOs) {
      ActivityStepVO vo =
          getActivityStepVOFromActivityStepDTO(activityStepDTO, activityPageDTO.getTitle());
      if (null != vo) {
        activityStepVOs.add(vo);
      }
      // int index = activityStepDTOs.indexOf(activityStepDTO);
      // ActivityStepVO activityStepVO = activityStepVOs.get(index);//
      // activityStepVOs的size和activityStepDTOs的size一样，所以不会数组越界
      // List<ActivityStepPicDTO> activityStepPicDTOs = activityStepDTO.getPicList();
      // List<ActivityStepItemDTO> activityStepItemDTOs = activityStepDTO.getItemList();
      // if (null != activityStepPicDTOs && activityStepPicDTOs.size() != 0) {
      // List<ActivityStepPicVO> activityStepPicVOs = getStepPic(activityStepPicDTOs);// 获取楼层图片数据
      // activityStepVO.setPicList(activityStepPicVOs);
      // }
      //
      // if (null != activityStepItemDTOs && activityStepItemDTOs.size() != 0) {
      // List<BaseItemVO> baseItemVOs = getStepItem(activityStepItemDTOs);// 获取楼层商品数据
      // if (null != baseItemVOs && baseItemVOs.size() != 0) {
      // activityStepVO.setItems(baseItemVOs);
      // }
      // }
      // activityStepVO.setPageName(activityPageDTO.getTitle());
    }


    return toJsonData(HomePageResult.getResult(activityStepVOs, true,
        ServerResultCode.SUCCESS_CODE, ""));
  }

  private ActivityStepVO getActivityStepVOFromActivityStepDTO(ActivityStepDTO activityStepDTO,
      String pageName) {
    if (null == activityStepDTO) {
      return null;
    }

    ActivityStepVO result = new ActivityStepVO();
    result.setPageName(pageName);
    result.setStepId(activityStepDTO.getStepId());
    result.setTitle(activityStepDTO.getTitle());
    result.setStepType(activityStepDTO.getStepType());
    result.setBegin(activityStepDTO.getBegin());
    result.setEnd(activityStepDTO.getEnd());
    if (CollectionUtils.isNotEmpty(activityStepDTO.getPicList())) {
      result.setPicList(getStepPic(activityStepDTO.getPicList()));
    }
    if (CollectionUtils.isNotEmpty(activityStepDTO.getContentList())) {
      result.setContentList(getStepContent(activityStepDTO.getContentList()));
    }
    List<BaseItemVO> voList = null;
    if (CollectionUtils.isNotEmpty(activityStepDTO.getItemList())) {
      voList = getStepItem(activityStepDTO.getItemList());
      if (CollectionUtils.isNotEmpty(voList)) {
        result.setItems(voList);
      }
    }
    if (CollectionUtils.isNotEmpty(activityStepDTO.getCatList())) {
      result.setCatList(getStepCat(activityStepDTO.getCatList()));
      result.setTitle("类目楼层");
    }
    List<ActivityStepDTO> stepList = activityStepDTO.getInnerStepList();
    if (CollectionUtils.isNotEmpty(stepList)) {
      List<ActivityStepVO> innerVOList = Lists.newArrayList();
      for (ActivityStepDTO step : stepList) {
        ActivityStepVO vo = getActivityStepVOFromActivityStepDTO(step, pageName);
        if (null != vo) {
          innerVOList.add(vo);
        }
      }
      if (CollectionUtils.isNotEmpty(innerVOList)) {
        result.setInnerStepInfo(innerVOList);
      }
    }
    return result;
  }
}
