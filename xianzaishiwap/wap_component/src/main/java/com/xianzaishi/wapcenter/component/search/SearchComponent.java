package com.xianzaishi.wapcenter.component.search;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.xianzaishi.itemcenter.client.item.CommodityService;
import com.xianzaishi.itemcenter.client.item.dto.ItemDTO;
import com.xianzaishi.itemcenter.client.item.dto.ItemBaseDTO.ItemStatusConstants;
import com.xianzaishi.itemcenter.client.item.dto.ItemBaseDTO.SaleStatusConstants;
import com.xianzaishi.itemcenter.client.item.query.ItemListQuery;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchasecenter.client.activity.ActivityPageService;
import com.xianzaishi.purchasecenter.client.activity.dto.ActivityPageDTO;
import com.xianzaishi.purchasecenter.client.activity.dto.ActivityStepDTO;
import com.xianzaishi.wapcenter.client.market.vo.BaseItemVO;
import com.xianzaishi.wapcenter.component.itemlist.ItemListComponent;

/**
 * 搜索接口实现类
 * 
 * @author dongpo
 * 
 */
@Component("searchComponent")
public class SearchComponent {
  private static final Logger LOGGER = Logger.getLogger(SearchComponent.class);

  @Autowired
  private CommodityService commodityService;

  @Autowired
  private ActivityPageService activityPageService;
  
  @Autowired
  private ItemListComponent itemListComponent;

  private static final String MAP_KEY = "items";

  /**
   * 搜索商品
   * 
   * @param itemKeyWord
   * @param pageSize
   * @param pageNum
   * @return
   */
  public String queryItemByKeyWord(String itemKeyWord, Integer pageSize, Integer pageNum) {
    if (null == itemKeyWord) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "itemKeyWord为空"));
    }
    if (null == pageSize) {
      pageSize = 20;// pageSize为空的时候设一个默认值
    }
    if (null == pageNum) {
      pageNum = 0;
    }
    ItemListQuery query = new ItemListQuery();
    query.setItemTitle(itemKeyWord);
    List<Short> saleStatus = Lists.newArrayList();
    saleStatus.add(SaleStatusConstants.SALE_ONLY_ONLINE);
    saleStatus.add(SaleStatusConstants.SALE_ONLINE_AND_MARKET);
    query.setSaleStatusList(saleStatus);
    query.setStatus(ItemStatusConstants.ITEM_STATUS_PASS_AND_UP_SHELVES);
    query.setPageSize(pageSize);
    query.setPageNum(pageNum);
    Result<Map<String, Object>> mapResult = commodityService.queryCommodies(query);
    if (null == mapResult || !mapResult.getSuccess() || null == mapResult.getModule()) {
      LOGGER.info("搜索失败原因：" + JackSonUtil.getJson(mapResult));
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.ITEM_UNEXIST, "没有找到与"
          + itemKeyWord + "相关的商品"));
    }
    Map<String, Object> map = mapResult.getModule();
    List<ItemDTO> itemDTOs = (List<ItemDTO>) map.get("itemList");
    List<BaseItemVO> baseItemVOs = itemListComponent.itemDTOs2BaseItemVOs(itemDTOs);
    mapResult.getModule().remove("itemList");
    mapResult.getModule().put(MAP_KEY, baseItemVOs);
    return toJsonData(mapResult);

  }

  /**
   * 查询热搜词
   * 
   * @param pageId
   * @param stepId
   * @return
   */
  public String queryHotWord(Integer pageId, Integer stepId) {
    Result<ActivityPageDTO> hotSearchResult =
        activityPageService.getPage(pageId, Arrays.asList(stepId));
    if (null == hotSearchResult || !hotSearchResult.getSuccess()
        || CollectionUtils.isEmpty(hotSearchResult.getModule().getStepList())) {
      LOGGER.error("获取热搜词失败原因：" + JackSonUtil.getJson(hotSearchResult));
      return toJsonData(hotSearchResult);
    }
    ActivityPageDTO activityPageDTO = hotSearchResult.getModule();

    List<ActivityStepDTO> activityStepDTOs = activityPageDTO.getStepList();
    String hotWord = activityStepDTOs.get(0).getTitle();// 热词
    String[] words = hotWord.split("#");
    return toJsonData(Result.getSuccDataResult(Arrays.asList(words)));
  }

  /**
   * 将传进来的数据转换为json数据
   * 
   * @param result
   * @return
   */
  public String toJsonData(Object result) {
    String jsonResult = JackSonUtil.getJson(result);
    return jsonResult;
  }

}
