package com.xianzaishi.wapcenter.component.cmcategory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.itemcenter.client.item.CommodityService;
import com.xianzaishi.itemcenter.client.item.dto.ItemBaseDTO.ItemStatusConstants;
import com.xianzaishi.itemcenter.client.item.dto.ItemBaseDTO.SaleStatusConstants;
import com.xianzaishi.itemcenter.client.item.dto.ItemDTO;
import com.xianzaishi.itemcenter.client.item.query.ItemListQuery;
import com.xianzaishi.itemcenter.client.stdcategory.CmCategoryService;
import com.xianzaishi.itemcenter.client.stdcategory.StdCategoryService;
import com.xianzaishi.itemcenter.client.stdcategory.dto.CategoryDTO;
import com.xianzaishi.itemcenter.client.stdcategory.dto.CmCategoryDTO;
import com.xianzaishi.itemcenter.client.sysproperty.SystemPropertyService;
import com.xianzaishi.itemcenter.client.sysproperty.dto.SystemConfigDTO;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.result.PagedResult;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.wapcenter.client.cmcategory.vo.CategoryVO;
import com.xianzaishi.wapcenter.client.cmcategory.vo.LeafCategoryVO;
import com.xianzaishi.wapcenter.client.cmcategory.vo.RootCategoryVO;
import com.xianzaishi.wapcenter.client.market.vo.BaseItemVO;
import com.xianzaishi.wapcenter.component.UserSource;
import com.xianzaishi.wapcenter.component.initlistener.ContextInitListener;
import com.xianzaishi.wapcenter.component.itemlist.ItemListComponent;

/**
 * 前台类目实现类
 * 
 * @author dongpo
 * 
 */
@Component("cmCategoryComponent")
public class CmCategoryComponent {
  private static final Logger LOGGER = Logger.getLogger(CmCategoryComponent.class);

  // 类目key
  private static final String MAP_CATEGORY_KEY = "categories";

  // 商品key
  private static final String MAP_ITEM_KEY = "items";

  // 图片key
  private static final String MAP_PIC_KEY = "picture";

  // 跳转链接key
  private static final String MAP_JUMP_LINK_KEY = "jumpLink";

  // 商品数量
  private static final String MAP_ITEM_COUNT_KEY = "itemCount";

  // 广告位图片
  private static final String MAP_AD_PICTURE_KEY = "adPicture";

  private static final String PREFIX = "http://img.xianzaishi.com/";

  @Autowired
  private CmCategoryService cmCategoryService;

  @Autowired
  private CommodityService commodityService;

  @Autowired
  private ItemListComponent itemListComponent;

  @Autowired
  private SystemPropertyService systemPropertyService;

  @Autowired
  private StdCategoryService stdCategoryService;

  /**
   * 请求默认类目列表
   * 
   * @param homePageCategory
   * @return
   */
  public String queryDefaultCategoryList(Boolean homePageCategory) {
    Result<List<CmCategoryDTO>> cmCategoryResult = cmCategoryService.getdefaultCategoryList();// 获取类目数据
    if (!cmCategoryResult.getSuccess()) {
      return toJsonData(cmCategoryResult);
    }
    List<CmCategoryDTO> cmCategoryDTOs = cmCategoryResult.getModule();
    if (CollectionUtils.isEmpty(cmCategoryDTOs)) {
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "类目数据为空"));
    }
    List<CategoryVO> categoryVOs = Lists.newArrayList();
    BeanCopierUtils.copyListBean(cmCategoryDTOs, categoryVOs, CategoryVO.class);
    String[] catPicList = null;
    if (homePageCategory) {
      Result<SystemConfigDTO> systemConfig = systemPropertyService.getSystemConfig(3);
      if (null == systemConfig || !systemConfig.getSuccess() || null == systemConfig.getModule()) {
        catPicList = UserSource.listCatPic;
      } else {
        catPicList = systemConfig.getModule().getConfigInfo().split(",");
      }
    } else {
      catPicList = UserSource.listCatPic;
    }
    for (int i = 0; i < categoryVOs.size(); i++) {
      categoryVOs.get(i).setMemo(UserSource.catMemo[i]);
      if (i < catPicList.length) {
        categoryVOs.get(i).setPic(PREFIX + catPicList[i]);
      }
    }

    return JackSonUtil.getJson(Result.getSuccDataResult(categoryVOs));
  }

  /**
   * 请求叶子类目数据
   * 
   * @param frontCatId
   * @param pageNum
   * @param pageSize
   * @return
   */
  public Result<Map<String, Object>> queryLeafCategory(Integer frontCatId, Integer pageSize,
      Integer pageNum) {
    if (null == frontCatId) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空");
    }
    if (null == pageSize) {
      pageSize = 10;
    }
    if (null == pageNum) {
      pageNum = 0;
    }
    ItemListQuery query = new ItemListQuery();
    query.setFrontCatId(frontCatId);
    query.setPageSize(pageSize);
    query.setPageNum(pageNum);
    List<Short> saleStatus = Lists.newArrayList();
    saleStatus.add(SaleStatusConstants.SALE_ONLY_ONLINE);
    saleStatus.add(SaleStatusConstants.SALE_ONLINE_AND_MARKET);
    query.setSaleStatusList(saleStatus);
    query.setStatus(ItemStatusConstants.ITEM_STATUS_PASS_AND_UP_SHELVES);
    Result<Map<String, Object>> leafCategoryResult = commodityService.queryCommodies(query);
    if (null == leafCategoryResult || !leafCategoryResult.getSuccess()) {
      return leafCategoryResult;
    }
    Map<String, Object> map = leafCategoryResult.getModule();
    if (MapUtils.isEmpty(map)) {
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "类目数据为空");
    }
    List<CmCategoryDTO> cmCategoryDTOs =
        (List<CmCategoryDTO>) map.get(CommodityService.ITEM_CAT_KEY);
    List<ItemDTO> itemDTOs = (List<ItemDTO>) map.get(CommodityService.ITEM_LIST_KEY);
    if (CollectionUtils.isEmpty(cmCategoryDTOs)) {
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "类目数据为空");
    }
    List<LeafCategoryVO> leafCategoryVOs = Lists.newArrayList();
    for (CmCategoryDTO cmCategoryDTO : cmCategoryDTOs) {
      LeafCategoryVO leafCategoryVO = new LeafCategoryVO();
      leafCategoryVO.setCatId(cmCategoryDTO.getCatId());
      leafCategoryVO.setCatName(cmCategoryDTO.getName());
      leafCategoryVO.setParentId(frontCatId);
      leafCategoryVOs.add(leafCategoryVO);
    }
    List<BaseItemVO> baseItemVOs = itemListComponent.itemDTOs2BaseItemVOs(itemDTOs);
    Map<String, Object> mapResult = Maps.newHashMap();
    mapResult.put(MAP_CATEGORY_KEY, leafCategoryVOs);
    mapResult.put(MAP_ITEM_KEY, baseItemVOs);
    Long count = (Long) map.get(CommodityService.ITEM_COUNT_KEY);
    mapResult.put(MAP_ITEM_COUNT_KEY, count);
    return Result.getSuccDataResult(mapResult);
  }

  /**
   * 二级类目请求商品数据
   * 
   * @param itemListQuery
   * @return
   */
  public Result<Map<String, Object>> queryCommodiesData(ItemListQuery itemListQuery) {
    if (null == itemListQuery) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空");
    }
    LOGGER.info(JackSonUtil.getJson(itemListQuery));
    Result<Map<String, Object>> result = commodityService.queryCommodies(itemListQuery);// 请求后台数据
    List<ItemDTO> itemDTOs =
        (List<ItemDTO>) (result.getModule()).get(CommodityService.ITEM_LIST_KEY);
    if (null == itemDTOs) {
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "商品数据为空");
    }
    List<BaseItemVO> baseItemVOs = itemListComponent.itemDTOs2BaseItemVOs(itemDTOs);

    result.getModule().remove("itemList");
    result.getModule().put(MAP_ITEM_KEY, baseItemVOs);
    return result;
  }

  /**
   * 将传进来的数据转换为json数据
   * 
   * @param result
   * @return
   */
  public String toJsonData(Object result) {
    String jsonResult = JackSonUtil.getJson(result);
    return jsonResult;
  }

  /**
   * 请求外卖叶子节点
   * 
   * @param parentId
   * @param pageSize
   * @param pageNum
   * @return
   */
  public PagedResult<Map<String, Object>> queryTakeOutLeafCategory(Integer parentId,
      Integer pageSize, Integer pageNum) {
    Result<Map<String, Object>> leafCategoryResult = queryLeafCategory(parentId, pageSize, pageNum);
    if (null == leafCategoryResult || !leafCategoryResult.getSuccess()
        || MapUtils.isEmpty(leafCategoryResult.getModule())) {
      String errorMsg = (null == leafCategoryResult ? "请求数据失败" : leafCategoryResult.getErrorMsg());
      return PagedResult.getErrDataResult(ServerResultCode.SERVER_ERROR, errorMsg);
    }
    Map<String, Object> leafCategoryMap = leafCategoryResult.getModule();
    List<LeafCategoryVO> leafCategoryVOs =
        (List<LeafCategoryVO>) leafCategoryMap.get(MAP_CATEGORY_KEY);
    if (CollectionUtils.isEmpty(leafCategoryVOs)) {
      return PagedResult.getErrDataResult(ServerResultCode.SERVER_ERROR, "类目数据为空");
    }
    // 先按照时间将叶子类目排序
    Result<SystemConfigDTO> systemConfigResult = systemPropertyService.getSystemConfig(13);
    Map<String, String> catIdMap = Maps.newHashMap();
    if (null != systemConfigResult && systemConfigResult.getSuccess()
        && null != systemConfigResult.getModule()
        && StringUtils.isNotBlank(systemConfigResult.getModule().getConfigInfo())) {
      catIdMap =
          (Map<String, String>) JackSonUtil.jsonToObject(systemConfigResult.getModule()
              .getConfigInfo(), Map.class);
    }
    List<LeafCategoryVO> leafCategorySortVOs = Lists.newArrayList();// 获取前面的leafCategoryVO
    List<LeafCategoryVO> leafCategoryLeftVOs = Lists.newArrayList();// 获取剩余的leafCategoryVO
    for (LeafCategoryVO leafCategoryVO : leafCategoryVOs) {
      LeafCategoryVO leafCategorySortVO = categorySortByTime(leafCategoryVO, catIdMap);
      if (null != leafCategorySortVO) {
        leafCategorySortVOs.add(leafCategorySortVO);
      } else {
        leafCategoryLeftVOs.add(leafCategoryVO);
      }
    }
    List<LeafCategoryVO> leafCategoryResultVOs = Lists.newArrayList();
    leafCategoryResultVOs.addAll(leafCategorySortVOs);
    leafCategoryResultVOs.addAll(leafCategoryLeftVOs);
    // 获取所有类目商品并排序输出
    List<Integer> catIds = Lists.newArrayList();
    for (LeafCategoryVO leafCategoryResultVO : leafCategoryResultVOs) {
      catIds.add(leafCategoryResultVO.getCatId());
    }
    LOGGER.error(",catIdMap:" + JackSonUtil.getJson(catIdMap) + ",leafCategoryResultVOs"
        + JackSonUtil.getJson(leafCategoryResultVOs) + ",catIds:" + JackSonUtil.getJson(catIds));
    ItemListQuery itemListQuery = new ItemListQuery();
    itemListQuery.setPageNum(pageNum);
    itemListQuery.setPageSize(pageSize);
    itemListQuery.setCmCat2Ids(catIds);
    itemListQuery.setInOrder(true);
    List<Short> saleStatus = Lists.newArrayList();
    saleStatus.add(SaleStatusConstants.SALE_ONLY_ONLINE);
    saleStatus.add(SaleStatusConstants.SALE_ONLINE_AND_MARKET);
    itemListQuery.setSaleStatusList(saleStatus);
    itemListQuery.setStatus(ItemStatusConstants.ITEM_STATUS_PASS_AND_UP_SHELVES);// 上架商品
    Result<Map<String, Object>> itemResult = queryCommodiesData(itemListQuery);
    List<BaseItemVO> baseItemVOs = Lists.newArrayList();
    Long count = 0L;
    if (null != itemResult && itemResult.getSuccess()
        && MapUtils.isNotEmpty(itemResult.getModule())) {
      baseItemVOs = (List<BaseItemVO>) itemResult.getModule().get(MAP_ITEM_KEY);
      count = (Long) itemResult.getModule().get(MAP_ITEM_COUNT_KEY);
    }
    Map<String, Object> resultMap = Maps.newHashMap();
    resultMap.put(MAP_ITEM_KEY, baseItemVOs);
    resultMap.put(MAP_CATEGORY_KEY, leafCategoryResultVOs);
    Result<SystemConfigDTO> pictureConfigResult = systemPropertyService.getSystemConfig(14);
    String pictureUrl = null;
    String jumpLink = null;
    if (null != pictureConfigResult && pictureConfigResult.getSuccess()
        && null != pictureConfigResult.getModule()) {
      SystemConfigDTO pictureConfigDTO = pictureConfigResult.getModule();
      String pictureInfo = pictureConfigDTO.getConfigInfo();
      Map<String, String> configMap =
          (Map<String, String>) JackSonUtil.jsonToObject(pictureInfo, Map.class);
      pictureUrl = configMap.get("pic");
      jumpLink = configMap.get("link");
    }
    resultMap.put(MAP_PIC_KEY, pictureUrl);
    resultMap.put(MAP_JUMP_LINK_KEY, jumpLink);
    return PagedResult.getSuccDataResult(resultMap, count.intValue(),
        getTotalPage(count.intValue(), pageSize), pageSize, pageNum);
  }

  /**
   * 类目按照时间排序
   * 
   * @param leafCategoryVO
   * @param catIdMap
   * @return
   */
  private LeafCategoryVO categorySortByTime(LeafCategoryVO leafCategoryVO,
      Map<String, String> catIdMap) {
    if (null == leafCategoryVO || MapUtils.isEmpty(catIdMap)) {
      return null;
    }
    Integer catId = leafCategoryVO.getCatId();
    for (String key : catIdMap.keySet()) {
      String[] timeRegion = key.trim().split("#");
      SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
      if (timeRegion.length < 1 || !checkDateValid(timeRegion[0]) || !checkDateValid(timeRegion[1])) {
        continue;
      }
      try {
        long start = sdf.parse(timeRegion[0]).getTime();
        long now = sdf.parse(sdf.format(new Date())).getTime();
        long end = sdf.parse(timeRegion[1]).getTime();
        List<String> catIds = Arrays.asList(catIdMap.get(key).split(","));
        if (start < end) {
          if ((now > start && now <= end) && catIds.contains(String.valueOf(catId))) {
            return leafCategoryVO;
          }
        } else if (start > end) {
          if ((now > start || now <= end) && catIds.contains(String.valueOf(catId))) {
            return leafCategoryVO;
          }
        }
      } catch (ParseException e) {
        e.printStackTrace();
      }
    }
    return null;
  }

  /**
   * 获取分页数量
   * 
   * @param number 数据总数量
   * @param pageSize 分页大小
   * @return
   */
  private Integer getTotalPage(Integer number, Integer pageSize) {
    Integer totalPage = 1;
    if (null == pageSize) {
      return totalPage;
    }
    if (number % pageSize != 0) {
      totalPage = (number / pageSize) + 1;
    } else {
      totalPage = number / pageSize;
    }
    return totalPage;
  }

  /**
   * 判断日期是否为合法日期
   * 
   * @param inDate
   * @return
   */
  private boolean checkDateValid(String inDate) {
    if (inDate == null) {
      return false;
    }
    SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
    timeFormat.setLenient(false);
    try {
      timeFormat.parse(inDate.trim());
    } catch (ParseException pe) {
      return false;
    }
    return true;
  }

  /**
   * 查询外卖商品数据
   * @param itemListQuery
   * @return
   */
  public PagedResult<List<BaseItemVO>> queryTakeOutCommodiesData(ItemListQuery itemListQuery) {
    if (null == itemListQuery) {
      return PagedResult.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空");
    }
    LOGGER.info(JackSonUtil.getJson(itemListQuery));
    Result<Map<String, Object>> result = commodityService.queryCommodies(itemListQuery);// 请求后台数据
    if (null == result || !result.getSuccess() || MapUtils.isEmpty(result.getModule())) {
      String errorMsg = (null == result ? "请求数据失败" : result.getErrorMsg());
      return PagedResult.getErrDataResult(ServerResultCode.SERVER_ERROR, errorMsg);
    }
    Map<String, Object> resultMap = result.getModule();
    List<ItemDTO> itemDTOs = (List<ItemDTO>) resultMap.get(CommodityService.ITEM_LIST_KEY);
    if (null == itemDTOs) {
      return PagedResult.getErrDataResult(ServerResultCode.SERVER_ERROR, "商品数据为空");
    }
    List<BaseItemVO> baseItemVOs = itemListComponent.itemDTOs2BaseItemVOs(itemDTOs);
    Long count = (Long) resultMap.get(CommodityService.ITEM_COUNT_KEY);
    Integer pageSize = itemListQuery.getPageSize();
    Integer pageNum = itemListQuery.getPageNum();
    Integer totalPage = getTotalPage(count.intValue(), pageSize);
    return PagedResult.getSuccDataResult(baseItemVOs, count.intValue(), totalPage, pageSize,
        pageNum);
  }

  /**
   * 查询一级类目数据
   * 
   * @return
   */
  public Result<Map<String, Object>> queryRootCategory() {
    Result<List<CmCategoryDTO>> cmCategoryResult = cmCategoryService.getdefaultCategoryList();// 获取类目数据
    if (null == cmCategoryResult || !cmCategoryResult.getSuccess()
        || null == cmCategoryResult.getModule()) {
      String errorMsg = (null == cmCategoryResult ? "请求数据失败" : cmCategoryResult.getErrorMsg());
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, errorMsg);
    }
    List<CmCategoryDTO> cmCategoryDTOs = cmCategoryResult.getModule();
    if (CollectionUtils.isEmpty(cmCategoryDTOs)) {
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "类目数据为空");
    }
    List<RootCategoryVO> rootCategoryVOs = Lists.newArrayList();
    BeanCopierUtils.copyListBean(cmCategoryDTOs, rootCategoryVOs, RootCategoryVO.class);
    // 请求一级类目图片
    List<RootCategoryVO> catPicList = Lists.newArrayList();
    Result<SystemConfigDTO> systemConfig = systemPropertyService.getSystemConfig(15);
    if (null != systemConfig && systemConfig.getSuccess() && null != systemConfig.getModule()) {
      catPicList =
          (List<RootCategoryVO>) JackSonUtil.jsonToList(systemConfig.getModule().getConfigInfo(),
              RootCategoryVO.class);
    }
    Map<Integer, String> picMap = Maps.newHashMap();
    Map<Integer, String> memoMap = Maps.newHashMap();
    Map<Integer, String> h5UrlMap = Maps.newHashMap();
    for (RootCategoryVO rootCategoryVO : catPicList) {
      picMap.put(rootCategoryVO.getCatId(), rootCategoryVO.getPicture());
      memoMap.put(rootCategoryVO.getCatId(), rootCategoryVO.getMemo());
      h5UrlMap.put(rootCategoryVO.getCatId(), rootCategoryVO.getH5Url());
    }
    for (int i = 0; i < rootCategoryVOs.size(); i++) {
      RootCategoryVO rootCategoryVO = rootCategoryVOs.get(i);
      if (StringUtils.isNotBlank(memoMap.get(rootCategoryVO.getCatId()))) {
        rootCategoryVO.setMemo(memoMap.get(rootCategoryVO.getCatId()));
      }
      if (StringUtils.isNotBlank(picMap.get(rootCategoryVO.getCatId()))) {
        rootCategoryVO.setPicture(PREFIX + picMap.get(rootCategoryVO.getCatId()));
      }
      if (StringUtils.isNotBlank(h5UrlMap.get(rootCategoryVO.getCatId()))) {
        rootCategoryVO.setH5Url(h5UrlMap.get(rootCategoryVO.getCatId()));
      }
    }
    RootCategoryVO rootCategoryVO = rootCategoryVOs.get(0);
    Result<Map<String, Object>> subCategoryResult = querySubCategory(rootCategoryVO.getCatId());
    if (null != subCategoryResult && subCategoryResult.getSuccess()
        && null != subCategoryResult.getModule()) {
      rootCategoryVO.setSubCategoryVOs((List<RootCategoryVO>) subCategoryResult.getModule().get(
          MAP_CATEGORY_KEY));
    }
    Map<String, Object> result = Maps.newHashMap();
    result.put(MAP_CATEGORY_KEY, rootCategoryVOs);
    String picUrl = null;
    if(StringUtils.isNotBlank(picMap.get(rootCategoryVO.getCatId()))){
      picUrl = PREFIX + picMap.get(rootCategoryVO.getCatId());
    }
    result.put(MAP_AD_PICTURE_KEY,picUrl);
    result.put(MAP_JUMP_LINK_KEY, h5UrlMap.get(rootCategoryVO.getCatId()));
    return Result.getSuccDataResult(result);
  }

  /**
   * 查询子类目
   * 
   * @param catId
   * @return
   */
  public Result<Map<String, Object>> querySubCategory(Integer parentId) {
    if (null == parentId) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空");
    }
    ItemListQuery query = new ItemListQuery();
    query.setFrontCatId(parentId);

    List<Short> saleStatus = Lists.newArrayList();
    saleStatus.add(SaleStatusConstants.SALE_ONLY_ONLINE);
    saleStatus.add(SaleStatusConstants.SALE_ONLINE_AND_MARKET);
    query.setSaleStatusList(saleStatus);
    query.setStatus(ItemStatusConstants.ITEM_STATUS_PASS_AND_UP_SHELVES);
    Result<Map<String, Object>> subCategoryResult = commodityService.queryCommodies(query);
    if (null == subCategoryResult || !subCategoryResult.getSuccess()
        || MapUtils.isEmpty(subCategoryResult.getModule())) {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_DATA_NOT_EXIST, "类目数据为空");
    }
    Map<String, Object> map = subCategoryResult.getModule();
    List<CmCategoryDTO> cmCategoryDTOs =
        (List<CmCategoryDTO>) map.get(CommodityService.ITEM_CAT_KEY);
    if (CollectionUtils.isEmpty(cmCategoryDTOs)) {
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "类目数据为空");
    }
    List<RootCategoryVO> subCategoryVOs = Lists.newArrayList();
    for (CmCategoryDTO categoryDTO : cmCategoryDTOs) {
      RootCategoryVO subCategoryVO = new RootCategoryVO();
      subCategoryVO.setCatId(categoryDTO.getCatId());
      subCategoryVO.setName(categoryDTO.getName());
      subCategoryVOs.add(subCategoryVO);
    }
    List<Integer> catIds = Lists.newArrayList();
    for (RootCategoryVO subCategoryVO : subCategoryVOs) {
      catIds.add(subCategoryVO.getCatId());
    }
    // TODO:优化
    Iterator<RootCategoryVO> iterator = subCategoryVOs.iterator();
    while (iterator.hasNext()) {
      RootCategoryVO subCategoryVO = iterator.next();
      Integer catId = subCategoryVO.getCatId();
      Result<List<CategoryDTO>> leafCategoryResult = stdCategoryService.querySubcategoryById(catId);
      if (null == leafCategoryResult || !leafCategoryResult.getSuccess()
          || CollectionUtils.isEmpty(leafCategoryResult.getModule())) {
        return Result.getErrDataResult(ServerResultCode.SERVER_DB_DATA_NOT_EXIST, "子类目数据为空");
      }
      List<CategoryDTO> leafCategoryDTOs = leafCategoryResult.getModule();
      List<RootCategoryVO> leafCategoryVOs = Lists.newArrayList();
      for (CategoryDTO categoryDTO : leafCategoryDTOs) {
        Integer categoryId = categoryDTO.getCatId();
        if(!ContextInitListener.catPictureMap.containsKey(categoryId)){
          continue;
        }
        RootCategoryVO categoryVO = new RootCategoryVO();
        categoryVO.setCatId(categoryId);
        categoryVO.setName(categoryDTO.getCatName());
        categoryVO.setPicture(PREFIX + ContextInitListener.catPictureMap.get(categoryId));
        leafCategoryVOs.add(categoryVO);
      }
      for (int i = 0; i < leafCategoryVOs.size(); i++) {
        leafCategoryVOs.get(i).setRootCatId(parentId);
      }
      if(CollectionUtils.isEmpty(leafCategoryVOs)){
        iterator.remove();
      }
      subCategoryVO.setSubCategoryVOs(leafCategoryVOs);
    }
    // 请求一级类目图片
    List<RootCategoryVO> catPicList = Lists.newArrayList();
    Result<SystemConfigDTO> systemConfig = systemPropertyService.getSystemConfig(15);
    if (null != systemConfig && systemConfig.getSuccess() && null != systemConfig.getModule()) {
      catPicList =
          (List<RootCategoryVO>) JackSonUtil.jsonToList(systemConfig.getModule().getConfigInfo(),
              RootCategoryVO.class);
    }
    Map<Integer, String> picMap = Maps.newHashMap();
    Map<Integer, String> h5UrlMap = Maps.newHashMap();
    for (RootCategoryVO rootCategoryVO : catPicList) {
      picMap.put(rootCategoryVO.getCatId(), rootCategoryVO.getPicture());
      h5UrlMap.put(rootCategoryVO.getCatId(), rootCategoryVO.getH5Url());
    }
    Map<String, Object> result = Maps.newHashMap();
    result.put(MAP_CATEGORY_KEY, subCategoryVOs);
    String picUrl = null;
    if(StringUtils.isNotBlank(picMap.get(parentId))){
      picUrl = PREFIX + picMap.get(parentId);
    }
    result.put(MAP_AD_PICTURE_KEY,picUrl);
    result.put(MAP_JUMP_LINK_KEY, h5UrlMap.get(parentId));
    return Result.getSuccDataResult(result);
  }
}
