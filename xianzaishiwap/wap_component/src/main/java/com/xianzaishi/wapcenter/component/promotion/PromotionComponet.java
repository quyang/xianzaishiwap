package com.xianzaishi.wapcenter.component.promotion;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.xianzaishi.couponcenter.client.discount.DiscountInfoService;
import com.xianzaishi.couponcenter.client.discount.dto.DiscountResultDTO;
import com.xianzaishi.couponcenter.client.discount.query.DiscountQueryDTO;
import com.xianzaishi.couponcenter.client.usercoupon.UserCouponService;
import com.xianzaishi.couponcenter.client.usercoupon.dto.BuySkuInfoDTO;
import com.xianzaishi.couponcenter.client.usercoupon.dto.UserCouponDTO;
import com.xianzaishi.couponcenter.client.usercoupon.dto.UserCouponDiscontResultDTO;
import com.xianzaishi.couponcenter.client.usercoupon.query.QueryParaDTO;
import com.xianzaishi.couponcenter.client.usercoupon.query.SendParaDTO;
import com.xianzaishi.couponcenter.client.usercoupon.query.SendParaDTO.SendTypeConstants;
import com.xianzaishi.couponcenter.client.usercoupon.query.UserCouponDiscontQueryDTO;
import com.xianzaishi.itemcenter.client.sysproperty.SystemPropertyService;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.usercenter.client.user.dto.BaseUserDTO;
import com.xianzaishi.wapcenter.client.mine.vo.UserAllPromotionVO;
import com.xianzaishi.wapcenter.client.promotion.BuyDetailVO;
import com.xianzaishi.wapcenter.client.promotion.BuySkuInfoVO;
import com.xianzaishi.wapcenter.component.mine.UserBaseComponent;

/**
 * 优惠相关接口
 * 
 * @author zhancang
 */
@Component("promotionComponet")
public class PromotionComponet {

  private static final int OFFLINE_DISCOUNT_90_10_COUPON_ID = 34;// 线下特殊满减优惠券id

  private static final Logger LOGGER = Logger.getLogger(PromotionComponet.class);

  @Autowired
  private UserCouponService userCouponService;

  @Autowired
  private UserBaseComponent userBaseComponent;

  @Autowired
  private SystemPropertyService systemPropertyService;

  @Autowired
  private DiscountInfoService discountInfoService;

  public Result<Boolean> addUserCoupon(String token, int requestCouponType, int couponId) {
    BaseUserDTO user = userBaseComponent.queryUserByToken(token);
    if (null == user) {
      return Result.getErrDataResult(ServerResultCode.USER_UNEXIST,
          "Query user not exit,input token:" + token);
    }

    // long userId = user.getUserId();
    if (requestCouponType != 1) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_UNSUPPORTEDQUERY,
          "Query coupon type not support,input:" + requestCouponType);
    }

    return addUserCouponPackage(token, 2, couponId);
  }

  /**
   * 查询用户针对当前购买情况可使用优惠券
   * 
   * @param token
   * @param
   * @return
   */
  public Result<UserCouponDiscontResultDTO> selectMathCoupon(String token, int payType,
      String buySkuDetailJson) {
    String error = null;
    BaseUserDTO user = userBaseComponent.queryUserByToken(token);
    if (null == user) {
      error = "[selectMathCoupon]Query user not exit,input token:" + token;
      LOGGER.error(error);
      return Result.getErrDataResult(ServerResultCode.USER_UNEXIST, error);
    }

    long userId = user.getUserId();

    if (payType != 1 && payType != 2) {// 1是线上，2是线下
      error = "[selectMathCoupon]Input payType is error,input payType:" + payType;
      LOGGER.error(error);
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, error);
    }

    if (StringUtils.isEmpty(buySkuDetailJson)) {
      error =
          "[selectMathCoupon]Input buySkuDetailJson is error,input buySkuDetailJson:"
              + buySkuDetailJson;
      LOGGER.error(error);
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, error);
    }

    Map<Long, String> buySkuDetail =
        (Map<Long, String>) JackSonUtil.jsonToMap(buySkuDetailJson,
            new TypeReference<Map<Long, String>>() {});
    if (null == buySkuDetail || CollectionUtils.isEmpty(buySkuDetail.keySet())) {
      error =
          "[selectMathCoupon]Input buySkuDetailJson change map error,input buySkuDetailJson:"
              + buySkuDetailJson;
      LOGGER.error(error);
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, error);
    }
    List<BuySkuInfoDTO> buySkuInfo = Lists.newArrayList();
    for (Long skuId : buySkuDetail.keySet()) {
      BuySkuInfoDTO sku = new BuySkuInfoDTO();
      sku.setSkuId(skuId);
      sku.setBuyCount(buySkuDetail.get(skuId));
      buySkuInfo.add(sku);
    }
    UserCouponDiscontQueryDTO query = new UserCouponDiscontQueryDTO();
    query.setBuySkuDetail(buySkuInfo);
    query.setPayType(payType);
    query.setUserId(userId);

    Result<UserCouponDiscontResultDTO> queryResult =
        userCouponService.selectMathCoupon(query);
    if (null != queryResult && queryResult.getSuccess()) {
      UserCouponDiscontResultDTO result = queryResult.getModule();
      updateUserCouponDiscontResultVOResultToCent(result);
      return Result.getSuccDataResult(result);
    }
    error =
        "[selectMathCoupon]Query return null,input token:" + token + ",payType" + payType
            + ",buySkuDetailJson" + buySkuDetailJson;
    LOGGER.error(error);
    return Result.getResult(new UserCouponDiscontResultDTO(), true, 0, "");
  }

  /**
   * 查询用户针对当前优惠券和购买情况，可优惠价格
   * 
   * @param
   * @param
   * @return
   */
  public Result<UserCouponDiscontResultDTO> calculateDiscountPriceWithCoupon(String buyDetailJson) {
    String error = null;
    if (StringUtils.isEmpty(buyDetailJson)) {
      error = "[calculateDiscountPriceWithCoupon]input parameter is null";
      LOGGER.error(error);
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, error);
    }

    Object query = JackSonUtil.jsonToObject(buyDetailJson, BuyDetailVO.class);
    if (null == query) {
      error =
          "[calculateDiscountPriceWithCoupon]input parameter change object failed,input is:"
              + buyDetailJson;
      LOGGER.error(error);
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, error);
    }

    BuyDetailVO buyDetail = (BuyDetailVO) query;
    long userId = buyDetail.getUserId();
    String userToken = buyDetail.getUserToken();
    BaseUserDTO user = null;
    if (userId > 0) {
      user = userBaseComponent.queryUserById(userId);
      if (null == user) {
        error = "[calculateDiscountPriceWithCoupon]Query user not exit,input userId:" + userId;
        LOGGER.error(error);
        return Result.getErrDataResult(ServerResultCode.USER_UNEXIST, error);
      }
    } else if (StringUtils.isNotEmpty(userToken)) {
      user = userBaseComponent.queryUserByToken(userToken);
      if (null == user) {
        error = "[calculateDiscountPriceWithCoupon]Query user not exit,input token:" + userToken;
        LOGGER.error(error);
        return Result.getErrDataResult(ServerResultCode.USER_UNEXIST, error);
      }
    }
    if (null == user) {
      error =
          "[calculateDiscountPriceWithCoupon]Query user not exit,input token:" + userToken
              + ",userId:" + userId;
      LOGGER.error(error);
      return Result.getErrDataResult(ServerResultCode.USER_UNEXIST, error);
    }

    int payType = buyDetail.getPayType();
    if (payType != 1 && payType != 2) {// 1是线上，2是线下
      error = "[calculateDiscountPriceWithCoupon]Input payType is error,input payType:" + payType;
      LOGGER.error(error);
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, error);
    }

    DiscountQueryDTO disQuery = new DiscountQueryDTO();
    if (CollectionUtils.isNotEmpty(buyDetail.getSkuInfoList())) {
      List<BuySkuInfoDTO> buySkuDetail = Lists.newArrayList();
      BeanCopierUtils.copyListBean(buyDetail.getSkuInfoList(), buySkuDetail, BuySkuInfoDTO.class);
      disQuery.setBuySkuDetail(buySkuDetail);
    } else {
      error = "[calculateDiscountPriceWithCoupon]Input buyInfo is null";
      LOGGER.error(error);
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, error);
    }
    if (buyDetail.getCouponId() > 0) {
      disQuery.setCouponId(buyDetail.getCouponId());
    } else {
      error = "[calculateDiscountPriceWithCoupon]Input coupon id is error";
      LOGGER.error(error);
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, error);
    }
    disQuery.setPayType(payType);
    disQuery.setUserId(user.getUserId());

    Result<DiscountResultDTO> discResult = discountInfoService.calculate(disQuery);
    if (null != discResult && discResult.getSuccess()) {
      DiscountResultDTO disResult = discResult.getModule();
      if(null == disResult || null == disResult.getOrinalPrice() || disResult.getOrinalPrice() <= 0){
        return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "GetDiscountFailed");
      }
      
      UserCouponDiscontResultDTO result = new UserCouponDiscontResultDTO();
      Integer discount = disResult.getDiscountPrice();
      Integer total = disResult.getOrinalPrice();
      if(null == discount || discount <= 0){
        result.setDiscountPrice(0);
      }else{
        result.setDiscountPrice(total-discount);
      }
      return Result.getSuccDataResult(result);
    }
    error = "[calculateDiscountPriceWithCoupon]Query return null,input parameter:" + buyDetailJson;
    LOGGER.error(error);
    return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, error);
  }

  /**
   * 查询用户针对当前购买情况可使用优惠券
   * 
   * @param token
   * @param
   * @return
   */
  public Result<List<UserCouponDTO>> selectUserCouponByPhone(String token, int queryType,
      int payType, String buySkuDetailJson) {
    String error = null;
    BaseUserDTO user = userBaseComponent.queryUserByToken(token);
    if (null == user) {
      error = "[selectUserCouponByPhone]Query user not exit,input token:" + token;
      LOGGER.error(error);
      return Result.getErrDataResult(ServerResultCode.USER_UNEXIST, error);
    }
    if(StringUtils.isEmpty(buySkuDetailJson))
    discountInfoService.removePromotionChainContext(user.getUserId());
    if (queryType == 0) {
      return selectUserCoupon(user, queryType, payType, null);
    }

    if (StringUtils.isEmpty(buySkuDetailJson)) {
      error =
          "[selectUserCoupon]Input buySkuDetailJson is error,input buySkuDetailJson:"
              + buySkuDetailJson;
      LOGGER.error(error);
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, error);
    }

    Map<Long, String> buySkuDetail =
        (Map<Long, String>) JackSonUtil.jsonToMap(buySkuDetailJson,
            new TypeReference<Map<Long, String>>() {});
    if (null == buySkuDetail || CollectionUtils.isEmpty(buySkuDetail.keySet())) {
      error =
          "[selectUserCoupon]Input buySkuDetailJson change map error,input buySkuDetailJson:"
              + buySkuDetailJson;
      LOGGER.error(error);
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, error);
    }
    List<BuySkuInfoVO> buySkuInfo = Lists.newArrayList();
    for (Long skuId : buySkuDetail.keySet()) {
      BuySkuInfoVO sku = new BuySkuInfoVO();
      sku.setSkuId(skuId);
      sku.setBuyCount(buySkuDetail.get(skuId));
      buySkuInfo.add(sku);
    }
    return selectUserCoupon(user, queryType, payType, buySkuInfo);
  }

  /**
   * 查询用户针对当前购买情况可使用优惠券
   * 
   * @param 
   * @param
   * @return
   */
  public Result<DiscountResultDTO> getUserDiscount(String buyDetailJson) {
    String error = null;
    if (StringUtils.isEmpty(buyDetailJson)) {
      error = "[selectUserCouponByShop]input parameter is null";
      LOGGER.error(error);
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, error);
    }

    Object query = JackSonUtil.jsonToObject(buyDetailJson, BuyDetailVO.class);
    if (null == query) {
      error =
          "[selectUserCouponByShop]input parameter change object failed,input is:" + buyDetailJson;
      LOGGER.error(error);
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, error);
    }

    BuyDetailVO buyDetail = (BuyDetailVO) query;
    long userId = buyDetail.getUserId();
    String userToken = buyDetail.getUserToken();
    BaseUserDTO user = null;
    if (userId > 0) {
      user = userBaseComponent.queryUserById(userId);
      if (null == user) {
        error = "[selectUserCouponByShop]Query user not exit,input userId:" + userId;
        LOGGER.error(error);
        return Result.getErrDataResult(ServerResultCode.USER_UNEXIST, error);
      }
    } else if (StringUtils.isNotEmpty(userToken)) {
      user = userBaseComponent.queryUserByToken(userToken);
      if (null == user) {
        error = "[selectUserCouponByShop]Query user not exit,input token:" + userToken;
        LOGGER.error(error);
        return Result.getErrDataResult(ServerResultCode.USER_UNEXIST, error);
      }
    }
    if (null == user) {
      error =
          "[selectUserCouponByShop]Query user not exit,input token:" + userToken + ",userId:"
              + userId;
      LOGGER.error(error);
      return Result.getErrDataResult(ServerResultCode.USER_UNEXIST, error);
    }
    
    DiscountQueryDTO disquery = new DiscountQueryDTO();
    disquery.setUserId(userId);
    
    List<BuySkuInfoDTO> buySkuDetail = Lists.newArrayList();
    BeanCopierUtils.copyListBean(buyDetail.getSkuInfoList(), buySkuDetail, BuySkuInfoDTO.class);
    
    disquery.setBuySkuDetail(buySkuDetail);
    disquery.setPayType(buyDetail.getPayType());
    
    Result<DiscountResultDTO> queryResult = discountInfoService.calculate(disquery);
    if (null != queryResult && queryResult.getSuccess() && null != queryResult.getModule()) {
      if(CollectionUtils.isNotEmpty(queryResult.getModule().getUserCouponList())){
        List<UserCouponDTO> result = queryResult.getModule().getUserCouponList();
        updateToCent(result);
      }
      return Result.getSuccDataResult(queryResult.getModule());
    }
    return Result.getSuccDataResult(null);
  }
  /**
   * 查询用户针对当前购买情况可使用优惠券
   * 
   * @param 
   * @param
   * @return
   */
  public Result<List<UserCouponDTO>> selectUserCouponByShop(String buyDetailJson) {
    String error = null;
    if (StringUtils.isEmpty(buyDetailJson)) {
      error = "[selectUserCouponByShop]input parameter is null";
      LOGGER.error(error);
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, error);
    }

    Object query = JackSonUtil.jsonToObject(buyDetailJson, BuyDetailVO.class);
    if (null == query) {
      error =
          "[selectUserCouponByShop]input parameter change object failed,input is:" + buyDetailJson;
      LOGGER.error(error);
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, error);
    }

    BuyDetailVO buyDetail = (BuyDetailVO) query;
    long userId = buyDetail.getUserId();
    String userToken = buyDetail.getUserToken();
    BaseUserDTO user = null;
    if (userId > 0) {
      user = userBaseComponent.queryUserById(userId);
      if (null == user) {
        error = "[selectUserCouponByShop]Query user not exit,input userId:" + userId;
        LOGGER.error(error);
        return Result.getErrDataResult(ServerResultCode.USER_UNEXIST, error);
      }
    } else if (StringUtils.isNotEmpty(userToken)) {
      user = userBaseComponent.queryUserByToken(userToken);
      if (null == user) {
        error = "[selectUserCouponByShop]Query user not exit,input token:" + userToken;
        LOGGER.error(error);
        return Result.getErrDataResult(ServerResultCode.USER_UNEXIST, error);
      }
    }
    if (null == user) {
      error =
          "[selectUserCouponByShop]Query user not exit,input token:" + userToken + ",userId:"
              + userId;
      LOGGER.error(error);
      return Result.getErrDataResult(ServerResultCode.USER_UNEXIST, error);
    }
    discountInfoService.removePromotionChainContext(userId);
    return selectUserCoupon(user, buyDetail.getQueryCouponType(), buyDetail.getPayType(),
        buyDetail.getSkuInfoList());
  }
  
  /**
	 * 查询用户针对当前购买情况可使用优惠券
	 * 
	 * @param user
	 *            用户信息
	 * @param queryType
	 *            查询方式（0:查询用户所有可用优惠券，1:根据当前购买情况查询购买的优惠券）
	 * @param payType
	 *            支付方式（1:线上支付，2:线下支付）
	 * @param buySkuInfo
	 *            购买sku的情况
	 * @return
	 */
	public Result<List<UserCouponDTO>> queryPromotionInfo(UserAllPromotionVO user) {
		String error = null;
		
		if (null == user) {
			error = "[selectUserCoupon]Query user not exit,input user is null";
			LOGGER.error(error);
			return Result.getErrDataResult(ServerResultCode.USER_UNEXIST, error);
		}

		long userId = user.getUserId();
		UserCouponDiscontQueryDTO query = new UserCouponDiscontQueryDTO();
		query.setUserId(userId);

		QueryParaDTO para = new QueryParaDTO();
		para.setUserId(userId);
		Result<List<UserCouponDTO>> result = userCouponService.getUserCoupon(para);
		if (null != result && result.getSuccess()) {
			List<UserCouponDTO> couponResult = result.getModule();
			updateToCent(couponResult);
			updateCouponChannel(couponResult);
			return Result.getSuccDataResult(couponResult);
		}

		return Result.getSuccDataResult(null);

	}


  /**
   * 查询用户针对当前购买情况可使用优惠券
   * 
   * @param user 用户信息
   * @param queryType 查询方式（0:查询用户所有可用优惠券，1:根据当前购买情况查询购买的优惠券）
   * @param payType 支付方式（1:线上支付，2:线下支付）
   * @param buySkuInfo 购买sku的情况
   * @return
   */
  private Result<List<UserCouponDTO>> selectUserCoupon(BaseUserDTO user, int queryType, int payType,
      List<BuySkuInfoVO> buySkuInfo) {
    String error = null;
    if (null == user) {
      error = "[selectUserCoupon]Query user not exit,input user is null";
      LOGGER.error(error);
      return Result.getErrDataResult(ServerResultCode.USER_UNEXIST, error);
    }

    long userId = user.getUserId();
    
    if (0 == queryType) {
      QueryParaDTO para = new QueryParaDTO();
      para.setUserId(userId);
      Result<List<UserCouponDTO>> result =
          userCouponService.getUserCoupon(para);
      if (null != result && result.getSuccess()) {
        List<UserCouponDTO> couponResult = result.getModule();
        updateToCent(couponResult);
        updateCouponChannel(couponResult);
        return Result.getSuccDataResult(couponResult);
      }
      error =
          "[selectUserCoupon]Query return null,input user:" + user.getUserId() + ",queryType"
              + queryType;
      LOGGER.error(error);
      return Result.getSuccDataResult(null);
    } else if (1 != queryType) {
      error = "[selectUserCoupon]Query parameter queryType error,queryType" + queryType;
      LOGGER.error(error);
      return Result.getSuccDataResult(null);
    }

    if (payType != 1 && payType != 2) {// 1是线上，2是线下
      error = "[selectUserCoupon]Input payType is error,input payType:" + payType;
      LOGGER.error(error);
      return Result.getSuccDataResult(null);
    }

    if (CollectionUtils.isEmpty(buySkuInfo)) {
      error = "[selectUserCoupon]Input buySkuDetailJson is null";
      LOGGER.error(error);
      return Result.getSuccDataResult(null);
    }
    
    DiscountQueryDTO query = new DiscountQueryDTO();
    query.setUserId(userId);
    
    List<BuySkuInfoDTO> buySkuDetail = Lists.newArrayList();
    BeanCopierUtils.copyListBean(buySkuInfo, buySkuDetail, BuySkuInfoDTO.class);
    
    query.setBuySkuDetail(buySkuDetail);
    query.setPayType(payType);
    
    Result<DiscountResultDTO> queryResult = discountInfoService.calculate(query);
//    Result<UserCouponDiscontResultDTO> queryResult = userCouponService.selectMathCoupon(query);
    if (null != queryResult && queryResult.getSuccess() && null != queryResult.getModule() && CollectionUtils.isNotEmpty(queryResult.getModule().getUserCouponList())) {//TODO
      List<UserCouponDTO> result = queryResult.getModule().getUserCouponList();
      updateToCent(result);
//      if (2 == payType) {
//        updateOfflineCouponDispaly(result);
//      }
      return Result.getSuccDataResult(result);
    }
    error =
        "[selectUserCoupon]Query return null,input userId:" + user.getUserId() + ",payType"
            + payType + ",buySkuDetailJson" + JackSonUtil.getJson(buySkuInfo);
    LOGGER.error(error);
    return Result.getSuccDataResult(null);
  }

  /**
   * 更新返回结果到元为单位和分为单位
   */
  private void updateUserCouponDiscontResultVOResultToCent(UserCouponDiscontResultDTO inputResult) {
    if (null == inputResult || CollectionUtils.isEmpty(inputResult.getUsedCouponList())) {
      return;
    }
    List<UserCouponDTO> couponVOList = inputResult.getUsedCouponList();
    updateToCent(couponVOList);
  }

  private void updateToCent(List<UserCouponDTO> couponVOList) {
    if (CollectionUtils.isEmpty(couponVOList)) {
      return;
    }

    for (UserCouponDTO coupon : couponVOList) {
      double couponAmount = coupon.getAmount();
      double couponLimit = coupon.getAmountLimit();
      coupon.setAmount((double) -1);
      coupon.setAmountLimit((double) -1);
      // if (coupon.isFenPriceTypeUserCoupon()) {
      // int amount = new BigDecimal(couponAmount).intValue();
      // String amountYuan =
      // new BigDecimal(amount).divide(new BigDecimal(100), 2, BigDecimal.ROUND_DOWN).toString();
      // coupon.setAmountCent(amount);
      // coupon.setAmountYuanDescription(amountYuan);
      //
      // int limit = new BigDecimal(couponLimit).intValue();
      // String amountLimitYuan =
      // new BigDecimal(couponLimit).divide(new BigDecimal(100), 2, BigDecimal.ROUND_DOWN)
      // .toString();
      // coupon.setAmountLimitCent(limit);
      // coupon.setAmountLimitYuanDescription(amountLimitYuan);
      // } else {
      int amount = new BigDecimal(couponAmount).multiply(new BigDecimal(100)).intValue();
      String amountYuan = new BigDecimal(couponAmount).toString();
      coupon.setAmountCent(amount);
      coupon.setAmountYuanDescription(amountYuan);

      int limit = new BigDecimal(couponLimit).multiply(new BigDecimal(100)).intValue();
      String amountLimitYuan =
          new BigDecimal(couponLimit).setScale(2, BigDecimal.ROUND_DOWN).toString();
      coupon.setAmountLimitCent(limit);
      coupon.setAmountLimitYuanDescription(amountLimitYuan);
      // }
    }
  }

  private void updateCouponChannel(List<UserCouponDTO> couponVOList) {
    if (CollectionUtils.isEmpty(couponVOList)) {
      return;
    }

    for (UserCouponDTO coupon : couponVOList) {
      String amountLimitYuan = coupon.getAmountLimitYuanDescription();
      if (null != coupon.getChannelType() && null != coupon.getCouponType()) {
        if (coupon.getCouponType() == 12) {
          if (coupon.getChannelType() == 0) {
            amountLimitYuan = "全场指定商品" + amountLimitYuan;
          } else if (coupon.getChannelType() == 1) {
            amountLimitYuan = "线上指定商品" + amountLimitYuan;
          } else if (coupon.getChannelType() == 2) {
            amountLimitYuan = "店内指定商品" + amountLimitYuan;
          }
        } else {
          if (coupon.getChannelType() == 0) {
            amountLimitYuan = "全场消费" + amountLimitYuan;
          } else if (coupon.getChannelType() == 1) {
            amountLimitYuan = "线上消费" + amountLimitYuan;
          } else if (coupon.getChannelType() == 2) {
            if (coupon.getCouponId() != null && coupon.getCouponId() == 34) {
              amountLimitYuan = amountLimitYuan + "店内支付宝再减20";
            } else {
              amountLimitYuan = "店内消费" + amountLimitYuan;
            }
          }
        }
      }
      coupon.setAmountLimitYuanDescription(amountLimitYuan);
    }
  }


  /**
   * 针对优惠券id=OFFLINE_DISCOUNT_90_10_COUPON_ID 的优惠券，独立更新设置名称
   * 
   * @param couponVOList
   */
  private void updateOfflineCouponDispaly(List<UserCouponDTO> couponVOList) {
    if (CollectionUtils.isEmpty(couponVOList)) {
      return;
    }

    for (UserCouponDTO coupon : couponVOList) {
      if (null != coupon.getId() && OFFLINE_DISCOUNT_90_10_COUPON_ID == coupon.getCouponId()) {
        coupon.setCouponTitle("满99减10元，使用支付宝付款再减20");
      }
    }
  }

  public Result<Boolean> addUserCouponPackage(String token, int requestCouponType, int groupId) {
    BaseUserDTO user = userBaseComponent.queryUserByToken(token);
    if (null == user) {
      LOGGER.error("addUserCouponPackage query user not exit,input token:" + token);
      return Result.getErrDataResult(ServerResultCode.USER_UNEXIST,
          "发送失败");
    }

    long userId = user.getUserId();
    if (requestCouponType != 2) {
      LOGGER.error("addUserCouponPackage coupon type not support,input:" + requestCouponType);
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_UNSUPPORTEDQUERY,
          "发送失败");
    }

    if (groupId <= 0) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误发送失败");
    }

    SendParaDTO sendQuery = new SendParaDTO();
    sendQuery.setPackageId(groupId);
    sendQuery.setSendType(SendTypeConstants.BY_COUPON_PACKAGE);
    sendQuery.setUserId(userId);

    Result<Boolean> sendResult = userCouponService.sendCoupon(sendQuery);
    if (null != sendResult && sendResult.getSuccess() && sendResult.getModule()) {
      return Result.getResult(true, true, 1, "发送成功");
    } else {
      LOGGER.error("Insert failed;" + sendResult.getErrorMsg());
      return Result.getResult(true, true, 1, "已拥有优惠券，无需领取");
    }
  }
}
