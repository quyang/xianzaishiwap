package com.xianzaishi.wapcenter.component;

import java.util.List;
import java.util.Map;

import com.xianzaishi.usercenter.client.user.dto.BaseUserDTO.UserStatusConstants;
import com.xianzaishi.usercenter.client.user.dto.UserDTO.UserSexConstants;
import com.xianzaishi.usercenter.client.user.dto.UserDTO.UserTypeConstants;

public class UserSource {

  /**
   * 用户状态
   */
  public static final Short status = UserStatusConstants.USER_STATUS_REGISTERING;

  /**
   * 用户图片
   */
  public static final String[] pic = {"1.png", "2.png", "3.png", "4.png", "5.png", "6.png",
      "7.png", "8.png", "9.png", "10.png", "11.png", "12.png", "13.png", "14.png", "15.png",
      "16.png", "17.png", "18.png", "19.png", "20.png", "21.png", "22.png", "23.png", "24.png"};

  public static final String[] homeCatPic = {
      "1/1481383871743_1.png",
      "1/1481383871743_2.png",
      "1/1481383871743_10.png",
      "1/1481383871743_4.png",
      "1/1481383871743_5.png",
      "1/1481383871743_6.png",
      "1/1481383871743_7.png",
      "1/1481383871743_8.png",
      "1/1474598148440.png",
      "1/1474598161656.png",};
  public static final String[] listCatPic = {"1/1474609379726.jpg",
      "1/1474609335342.jpg",
      "1/1474609259665.jpg",
      "1/1474609317141.jpg",
      "1/1474609288264.jpg",
      "1/1474609274450.jpg",
      "1/1474609300850.jpg",
      "1/1474609226024.jpg",
      "1/1474609352027.jpg",
      "1/1474609368024.jpg"};

  public static final String[] catMemo = {"新鲜时令", "野生放养", "新鲜味美", "保质保鲜", "纯真奶源", "酒水饮料", "饮食必备",
      "生活必备", "三文鱼/料理", "糕点/蛋糕"};

  /**
   * 用户类型
   */
  public static final Short userType = UserTypeConstants.COMMON;

  public static final String angelMember = "天使会员";
  


  /**
   * 用户性别
   */
  public static final Short sex = UserSexConstants.FEMALE;

  /**
   * 用户各种标签
   */
  public static final Integer userTag = 0;

}
