package com.xianzaishi.wapcenter.component.itemdetail;

import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.xianzaishi.itemcenter.client.item.CommodityService;
import com.xianzaishi.itemcenter.client.item.dto.ItemBaseDTO;
import com.xianzaishi.itemcenter.client.item.dto.ItemDTO;
import com.xianzaishi.itemcenter.client.item.query.ItemQuery;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.sysproperty.SystemPropertyService;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.wapcenter.client.market.vo.ItemDetailVO;
import com.xianzaishi.wapcenter.client.market.vo.ItemSkuVO;
import com.xianzaishi.wapcenter.client.market.vo.ItemTagDetailVO;
import com.xianzaishi.wapcenter.component.itemlist.ItemListComponent;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.domain.client.itf.IInventory2CDomainClient;
import com.xianzaishi.wms.hive.vo.InventoryVO;

@Component("itemDetailComponent")
public class ItemDetailComponent {


  private static final Logger LOGGER = Logger.getLogger(ItemDetailComponent.class);

  @Autowired
  private CommodityService commodityService;

  @Autowired
  private SystemPropertyService systemPropertyService;

  @Autowired
  private IInventory2CDomainClient inventory2CDomainClient;

  @Autowired
  private ItemListComponent itemListComponent;

  private static final String HTTPHEAD = "http://";
  private static final String HTTPSHEAD = "https://";

  public String queryItemDetail(Long itemId) {
    LOGGER.info("商品id：" + itemId);
    ItemDetailVO itemDetailVO = new ItemDetailVO();
    ItemQuery itemQuery = new ItemQuery();
    itemQuery.setItemId(itemId);
    itemQuery.setHasItemSku(true);
    itemQuery.setReturnAll(true);
    Result<ItemBaseDTO> result = commodityService.queryCommodity(itemQuery);
    if (null == result || !result.getSuccess() || null == result.getModule()) {
      LOGGER.error("请求失败原因：" + JackSonUtil.getJson(result));
      return toJsonData(Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "没有找到与" + itemId
          + "相关的商品"));
    }
    ItemDTO itemDTO = (ItemDTO) result.getModule();
    setItemDetailVO(itemDTO, itemDetailVO);
    return toJsonData(Result.getSuccDataResult(itemDetailVO));
  }

  /**
   * 设置商品详细页数据
   * 
   * @param itemDto
   * @param itemDetailVO
   */
  private void setItemDetailVO(ItemDTO itemDto, ItemDetailVO itemDetailVO) {
    if (null == itemDto) {
      return;
    }
    List<ItemSkuVO> itemSkuVOs = new ArrayList<ItemSkuVO>();
    Inet4Address address = null;
    try {

      BeanCopierUtils.copyProperties(itemDto, itemDetailVO);// 设置商品详细页的title、subtitle、商品id、商品图片、商品产地
      address = (Inet4Address) Inet4Address.getLocalHost();
      String ipString = address.getHostAddress();
      String introductionUrl =
          HTTPSHEAD + ipString + "/requestcommodity/itemintroduce?itemId="
              + itemDetailVO.getItemId() + "&query=introduce";
      String propertyUrl =
          HTTPSHEAD + ipString + "/requestcommodity/itemintroduce?itemId="
              + itemDetailVO.getItemId() + "&query=property";
      itemDetailVO.setIntroductionUrl(introductionUrl);// 设置商品详细页的商品介绍URL
      itemDetailVO.setPropertyUrl(propertyUrl);// 设置商品详细页的商品属性URL
      // TODO:商品图片加前缀
//      itemDetailVO.setPicList(itemDto.getPicList());
      setVOHttpsPic(itemDetailVO,itemDto.getPicList());
      List<ItemCommoditySkuDTO> itemCommoditySkuDTO = itemDto.getItemSkuDtos();
      if (CollectionUtils.isNotEmpty(itemCommoditySkuDTO)) {
        setItemSkuVO(itemCommoditySkuDTO, itemSkuVOs);// 拷贝sku属性到itemSkuVO
      }
      itemDetailVO.setItemSkuVOs(itemSkuVOs);// 设置商品详细页的sku属性
    } catch (UnknownHostException e) {
      e.printStackTrace();
    }
  }
  
  private void setVOHttpsPic(ItemDetailVO itemDetailVO, List<String> picList){
    if(CollectionUtils.isEmpty(picList)){
      return;
    }
    List<String> picHttpsList = Lists.newArrayList();
    for(String tmp:picList){
      if(StringUtils.isNotEmpty(tmp)){
        tmp = tmp.trim();
        if(tmp.startsWith(HTTPHEAD)){
          tmp = tmp.replaceFirst(HTTPHEAD, HTTPSHEAD);
        }
        picHttpsList.add(tmp);
      }
    }
    itemDetailVO.setPicList(picHttpsList);
  }

  /**
   * 设置返回客户端商品sku属性
   * 
   * @param itemCommoditySkuDTOs
   * @param itemSkuVOs
   */
  private void setItemSkuVO(List<ItemCommoditySkuDTO> itemCommoditySkuDTOs,
      List<ItemSkuVO> itemSkuVOs) {
    if (itemCommoditySkuDTOs == null) {
      return;
    }
    for (ItemCommoditySkuDTO itemCommoditySkuDTO : itemCommoditySkuDTOs) {
      ItemSkuVO itemSkuVO = new ItemSkuVO();
      // 拷贝sku的后台skuID，商品id，库存，sku单位等
      BeanCopierUtils.copyProperties(itemCommoditySkuDTO, itemSkuVO);
      SimpleResultVO<InventoryVO> simpleResultVO =
          inventory2CDomainClient.getInventoryBySKUID(1L, itemCommoditySkuDTO.getSkuId());
      if (null != simpleResultVO && simpleResultVO.isSuccess()
          && null != simpleResultVO.getTarget()) {
        InventoryVO inventoryVO = simpleResultVO.getTarget();
        if (inventoryVO.getMaySale()) {
          itemSkuVO.setInventory(1);
        } else {
          itemSkuVO.setInventory(0);
        }
        LOGGER.info("库存数为：" + inventoryVO.getNumber() + "是否可销售：" + inventoryVO.getMaySale());
        Integer tags = itemCommoditySkuDTO.getTags();
        itemListComponent.resetItemInventory(tags, itemCommoditySkuDTO.getSkuId(), itemSkuVO);
      }
      itemSkuVO.setSaleDetailInfo(itemCommoditySkuDTO.querySpecification());
      itemSkuVO.setOriginplace(itemCommoditySkuDTO.queryOriginpace());
      if (null != itemCommoditySkuDTO.getTags()) {
        itemSkuVO.setTags(Integer.toBinaryString(itemCommoditySkuDTO.getTags()));
      }
      Long skuId = itemSkuVO.getSkuId();
      Result<List<ItemTagDetailVO>> itemTagDetailVOResult =
          itemListComponent.queryItemTagsDetail(Arrays.asList(skuId),(short) 1);
      if (null != itemTagDetailVOResult && itemTagDetailVOResult.getSuccess()
          && null != itemTagDetailVOResult.getModule()) {
        List<ItemTagDetailVO> itemDetailVOs = itemTagDetailVOResult.getModule();
        itemSkuVO.setTagDetail(itemDetailVOs.get(0));
      }
      itemSkuVOs.add(itemSkuVO);
    }
  }

  /**
   * 将传进来的数据转换为json数据
   * 
   * @param result
   * @return
   */
  public String toJsonData(Object result) {
    String jsonResult = JackSonUtil.getJson(result);
    return jsonResult;
  }

}
