package com.xianzaishi.wapcenter.component.initlistener;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.google.common.collect.Maps;

public class ContextInitListener implements ServletContextListener {
  
  public static Map<Integer, String> catPictureMap = Maps.newHashMap();

  @Override
  public void contextInitialized(ServletContextEvent sce) {
    Properties props = new Properties(); 
    InputStream inputStream = null;
    try { 
        inputStream = getClass().getResourceAsStream("/catpic.property"); 
        props.load(inputStream);
        for(Object key : props.keySet()){
          catPictureMap.put(Integer.valueOf((String) key), (String)props.get(key));
        }
    } catch (IOException ex) { 
        ex.printStackTrace(); 
    } 
  }

  @Override
  public void contextDestroyed(ServletContextEvent sce) {
    // TODO Auto-generated method stub
    
  }

}
