package com.xianzaishi.wapcenter.component.mine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.couponcenter.client.usercoupon.UserCouponService;
import com.xianzaishi.couponcenter.client.usercoupon.query.SendParaDTO;
import com.xianzaishi.couponcenter.client.usercoupon.query.SendParaDTO.SendTypeConstants;
import com.xianzaishi.itemcenter.client.sysproperty.SystemPropertyService;
import com.xianzaishi.itemcenter.client.sysproperty.dto.SystemConfigDTO;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.usercenter.client.user.UserService;
import com.xianzaishi.usercenter.client.user.dto.BaseUserDTO;
import com.xianzaishi.usercenter.client.userachievement.UserAchievementService;
import com.xianzaishi.usercenter.client.userachievement.dto.UserAchievementDTO;
import com.xianzaishi.usercenter.client.userachievement.query.UserAchievementQuery;
import com.xianzaishi.usercenter.client.userrelation.UserRelationService;
import com.xianzaishi.usercenter.client.userrelation.dto.UserRelationDTO;
import com.xianzaishi.usercenter.client.userrelation.query.UserRelationQuery;
import com.xianzaishi.wapcenter.client.mine.vo.UserAchievementDetailVO;
import com.xianzaishi.wapcenter.client.order.vo.OrderResponseVO;

@Component("oldWithNewComponent")
public class OldWithNewComponent {

  private static final Logger LOGGER = Logger.getLogger(OldWithNewComponent.class);

  @Autowired
  private UserService userService;// 用户中心服务

  @Autowired
  private UserRelationService userRelationService;// 用户关系服务

  @Autowired
  private UserAchievementService userAchievementService;// 用户成就服务

  @Autowired
  private UserCouponService userCouponService;// 优惠券服务

  @Autowired
  private SystemPropertyService systemPropertyService;

  private static final Integer COUPON_DENOMINATION = 50;

  private String queryOrderCountUrl;// 获取订单数量接口地址

  /**
   * 获取用户成就列表
   * 
   * @param token
   * @return
   */
  public Result<Map<String, Object>> getAchievementList(String token) {
    Map<String, Object> map = Maps.newHashMap();
    if (StringUtils.isEmpty(token)) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空!");
    }

    Result<BaseUserDTO> inviterResult = userService.queryBaseUserByToken(token);
    if (null == inviterResult || !inviterResult.getSuccess() || null == inviterResult.getModule()) {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_DATA_NOT_EXIST, "该用户不存在，请先登录！");
    }
    Long userId = inviterResult.getModule().getUserId();
    Result<List<UserRelationDTO>> userRelationResult =
        userRelationService.queryUserRelationByUserId(userId);
    List<UserAchievementDetailVO> userAchievementDetailVOs = null;
    if (null != userRelationResult && userRelationResult.getSuccess()
        && CollectionUtils.isNotEmpty(userRelationResult.getModule())) {
      List<UserRelationDTO> userRelationDTOs = userRelationResult.getModule();
      List<Long> inviteeIds = Lists.newArrayList();
      userAchievementDetailVOs = Lists.newArrayList();
      // 获取用户奖励明细
      for (UserRelationDTO userRelationDTO : userRelationDTOs) {
        UserAchievementDetailVO userAchievementDetailVO = new UserAchievementDetailVO();
        inviteeIds.add(userRelationDTO.getRelatedId());
        userAchievementDetailVO.setUserId(userRelationDTO.getUserId());
        userAchievementDetailVO.setStatuString(getUserAchievementStatus(userRelationDTO
            .getRelationTag()));
        userAchievementDetailVOs.add(userAchievementDetailVO);
      }
      Result<List<BaseUserDTO>> inviteeResult = userService.queryBaseUserByIds(inviteeIds);
      if (null != inviteeResult && inviteeResult.getSuccess()
          && CollectionUtils.isNotEmpty(inviteeResult.getModule())) {
        List<BaseUserDTO> baseUserDTOs = inviteeResult.getModule();
        for (BaseUserDTO baseUserDTO : baseUserDTOs) {
          int index = inviteeIds.indexOf(baseUserDTO.getUserId());
          userAchievementDetailVOs.get(index).setPhone(String.valueOf(baseUserDTO.getPhone()));
        }
      } else {
        LOGGER.error("被邀请人数据：" + JackSonUtil.getJson(inviteeResult));
      }
    } else {
      LOGGER.error("用户关系数据：" + JackSonUtil.getJson(userRelationResult));
    }
    map.put("achievementDetail", userAchievementDetailVOs);
    UserAchievementQuery query = new UserAchievementQuery();
    query.setUserId(userId);
    query.setType(1);
    query.setStatus(1);
    Result<List<UserAchievementDTO>> userAchievementResult =
        userAchievementService.queryUserAchievement(query);
    Integer inviterNumber = null;
    if (null != userAchievementResult && userAchievementResult.getSuccess()
        && null != userAchievementResult.getModule()) {
      UserAchievementDTO userAchievementDTO = userAchievementResult.getModule().get(0);
      inviterNumber = userAchievementDTO.getAchievementValue();
    }
    String copyWriter = getAchievementCopyWriter(inviterNumber);
    map.put("cw", copyWriter);// 文案
    List<String> redFonts = Lists.newArrayList();
    if (null != inviterNumber && inviterNumber > 0) {
      redFonts.add(new StringBuilder(inviterNumber).append("位").toString());
      redFonts.add(new StringBuilder(COUPON_DENOMINATION * inviterNumber).append("元").toString());
    } else {
      redFonts.add("0元");
    }

    map.put("redFont", redFonts);
    map.put("type", 1);
    return Result.getSuccDataResult(map);
  }

  /**
   * 获取成就文案
   * 
   * @param inviterNumber
   * @return
   */
  private String getAchievementCopyWriter(Integer inviterNumber) {
    Integer number = 0;
    Integer reward = 0;
    Integer denomination = COUPON_DENOMINATION;
    if (null != inviterNumber && inviterNumber > 0) {
      number = inviterNumber;
      Result<SystemConfigDTO> systemConfigResult = systemPropertyService.getSystemConfig(6);
      if (null != systemConfigResult && systemConfigResult.getSuccess()
          && null != systemConfigResult.getModule()) {
        SystemConfigDTO systemConfigDTO = systemConfigResult.getModule();
        if(StringUtils.isNotBlank(systemConfigDTO.getConfigInfo()) && StringUtils.isNumeric(systemConfigDTO.getConfigInfo())){
          denomination = Integer.valueOf(systemConfigDTO.getConfigInfo());
        }
      }
      reward = denomination * number;
      return new StringBuffer("您已邀请").append(number).append("位好友，获得").append(reward).append("元礼券")
          .toString();
    }
    return new StringBuffer("您尚未邀请好友，获得0元礼券").toString();
  }

  /**
   * 获取被邀请人状态
   * 
   * @param relationTag
   * @return
   */
  private String getUserAchievementStatus(Integer relationTag) {
    String status = null;
    if ((relationTag & 0x1) != 0) {
      status = "已交易";
    } else {
      status = "已注册";
    }
    return status;
  }

  public Result<Boolean> inviteCodeExchange(String token, String inviteCode) {
    if (null == token || null == inviteCode) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空");
    }
    Result<BaseUserDTO> inviteeResult = userService.queryBaseUserByToken(token);// 获取被邀请用户信息
    if (null == inviteeResult || !inviteeResult.getSuccess() || null == inviteeResult.getModule()) {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_DATA_NOT_EXIST, "用户不存在，请先登录");
    }
    Long relatedId = inviteeResult.getModule().getUserId();// 被邀请用户id
    // 判断该用户是否之前交易过
    if (tradeBefore(relatedId)) {
      return Result
          .getErrDataResult(ServerResultCode.SERVER_DB_DATA_AREADY_EXIST, "您已是老用户，不能参与此活动");
    }
    Result<BaseUserDTO> inviterResult = userService.queryBaseUserByInviteCode(inviteCode);// 获取邀请用户信息
    if (null == inviterResult || !inviterResult.getSuccess() || null == inviterResult.getModule()) {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_DATA_NOT_EXIST, "邀请码错误");
    }
    if (inviterResult.getModule().getUserId().longValue() == relatedId.longValue()) {
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "不能使用自己的邀请码兑换！");
    }
    Long userId = inviterResult.getModule().getUserId();// 邀请用户id
    UserRelationQuery query = new UserRelationQuery();
    query.setRelatedId(relatedId);
    query.setType(1);
    query.setStatus((short) 1);
    Result<List<UserRelationDTO>> userRelationResult =
        userRelationService.queryUserRelationByQuery(query);
    if (null != userRelationResult && userRelationResult.getSuccess()
        && CollectionUtils.isNotEmpty(userRelationResult.getModule())) {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_DATA_AREADY_EXIST,
          "您已经领取该优惠，请勿重复兑换！");
    }

    UserRelationDTO userRelationDTO = new UserCenterComponent().getUserRelation(userId, relatedId);
    Result<Boolean> insertRelation = userRelationService.insertUserRelation(userRelationDTO);
    if (null == insertRelation || !insertRelation.getSuccess()
        || null == insertRelation.getModule() || !insertRelation.getModule()) {
      LOGGER.error("用户关系数据插入失败：" + JackSonUtil.getJson(insertRelation));
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "网络连接失败，请稍后再试");
    }

    int sendPackageId = 7;// 后台针对老带新，固定配置7为key的优惠券组
    SendParaDTO send = new SendParaDTO();
    send.setPackageId(sendPackageId);
    send.setSendType(SendTypeConstants.BY_COUPON_PACKAGE);
    send.setUserId(userId);
    Result<Boolean> sendCoupons = null;
    //避免并发重复发放优惠券
    synchronized (userId) {
      sendCoupons = userCouponService.sendCoupon(send);
      if (null == sendCoupons || !sendCoupons.getSuccess() || null == sendCoupons.getModule()
          || !sendCoupons.getModule()) {
        // 如果发送优惠券失败，再次发送
        sendCoupons = userCouponService.sendCoupon(send);
        LOGGER.error("重新发送优惠券结果：" + JackSonUtil.getJson(sendCoupons));
      }
    }
    return sendCoupons;
  }

  private Boolean tradeBefore(Long relatedId) {
    if (null == relatedId) {
      return false;
    }
    Boolean isTradeBefore = false;
    // 获取交易数量
    Integer tradeCount = getTradeCount(relatedId);
    if (null != tradeCount && tradeCount > 0) {
      isTradeBefore = true;
    }
    return isTradeBefore;
  }

  private Integer getTradeCount(Long relatedId) {
    CloseableHttpClient httpclient = HttpClients.createDefault();
    try {

      HttpPost httpPost = new HttpPost(queryOrderCountUrl);
      List<NameValuePair> nvps = new ArrayList<NameValuePair>();
      nvps.add(new BasicNameValuePair("uid", String.valueOf(relatedId)));
      httpPost.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));
      RequestConfig requestConfig =
          RequestConfig.custom().setConnectTimeout(10000).setConnectionRequestTimeout(10000)
              .setSocketTimeout(10000).build();
      httpPost.setConfig(requestConfig);
      CloseableHttpResponse response = httpclient.execute(httpPost);

      try {
        HttpEntity entity = response.getEntity();
        InputStream is = entity.getContent();
        BufferedReader in = new BufferedReader(new InputStreamReader(is, HTTP.UTF_8));
        StringBuffer buffer = new StringBuffer();
        String line = "";
        while ((line = in.readLine()) != null) {
          buffer.append(line);
        }
        OrderResponseVO orderResult =
            (OrderResponseVO) JackSonUtil.jsonToObject(buffer.toString(), OrderResponseVO.class);;
        if (null != orderResult && orderResult.isSuccess()) {
          return (Integer) orderResult.getData();
        }
      } catch (IOException e) {
        e.printStackTrace();
      } finally {
        try {
          response.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    } catch (ConnectTimeoutException e) {
      // 捕获超时异常
      LOGGER.error(e);
    } catch (ClientProtocolException e1) {
      LOGGER.error("获取订单ClientProtocol错误" + e1);
      e1.printStackTrace();
    } catch (IOException e1) {
      LOGGER.error("获取订单数量IO错误：" + e1);
      e1.printStackTrace();
    } finally {
      try {
        httpclient.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    return 0;
  }

  public String getQueryOrderCountUrl() {
    return queryOrderCountUrl;
  }

  public void setQueryOrderCountUrl(String queryOrderCountUrl) {
    this.queryOrderCountUrl = queryOrderCountUrl;
  }

}
