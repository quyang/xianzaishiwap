package com.xianzaishi.wapcenter.component.mine;

import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.usercenter.client.user.UserService;
import com.xianzaishi.usercenter.client.user.dto.BaseUserDTO;
import com.xianzaishi.usercenter.client.user.dto.UserDTO;
import com.xianzaishi.wapcenter.client.mine.vo.UserProfileQueryVO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by quyang on 2017/2/23.
 */
@Component("userMsgConmponent")
public class UserMsgConmponent {

  @Autowired
  private UserService userService;

  private static final Logger LOGGER = Logger.getLogger(UserMsgConmponent.class);


  /**
   * 更新用户信息
   */
  public Result<Boolean> updateUserProfile(UserProfileQueryVO userProfileQueryVO) {

    LOGGER.error("参数1 UserMsgComponent.java, userDTO msgId=" + userProfileQueryVO.getRegitstId()
        + ", version=" + userProfileQueryVO.getVersion() + ", uuid=" + userProfileQueryVO
        .getUserId());

    if (null != userProfileQueryVO && null != userProfileQueryVO.getUserId()) {

      Result<? extends BaseUserDTO> result = null;

      //查询用户所有信息
      try {
        result = userService
            .queryUserByUserId(userProfileQueryVO.getUserId(),
                true);
      } catch (NumberFormatException e) {
        return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage());
      }

      //获取所有信息成功
      if (null != result && result.getSuccess() && null != result.getModule()) {
        UserDTO userDTO = (UserDTO) result.getModule();
        //更新消息
          userDTO.setMsgId(userProfileQueryVO.getRegitstId());
          userDTO.setEquipmentNumber(userProfileQueryVO.getEquipmentNumber());
          userDTO.setAppversion(userProfileQueryVO.getVersion());

          LOGGER.error(
              "参数2 UserMsgComponent.java, userDTO msgId=" + userDTO.getMsgId() + ", version="
                  + userDTO.getAppversion() + ", equipmentNum=" + userDTO.getEquipmentNumber()
                  + ", userId=" + userDTO.getUserId());

          userService.updateUserProfileInfo(userDTO);
      }
    }

    return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "更新用户信息失败");
  }

}
