package com.xianzaishi.wapcenter.component.itemlist;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.itemcenter.client.item.CommodityService;
import com.xianzaishi.itemcenter.client.item.dto.ItemDTO;
import com.xianzaishi.itemcenter.client.item.query.ItemListQuery;
import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuTagsDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.SkuTagDetail;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuTagsDTO.SkuTagDefinitionConstants;
import com.xianzaishi.itemcenter.client.sysproperty.SystemPropertyService;
import com.xianzaishi.itemcenter.client.sysproperty.dto.SystemConfigDTO;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.wapcenter.client.market.vo.BaseItemVO;
import com.xianzaishi.wapcenter.client.market.vo.ItemSkuVO;
import com.xianzaishi.wapcenter.client.market.vo.ItemTagDetailVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.domain.client.itf.IInventory2CDomainClient;
import com.xianzaishi.wms.hive.vo.InventoryVO;

@Component("itemListComponent")
public class ItemListComponent {


  private static Logger LOGGER = Logger.getLogger(ItemListComponent.class);

  @Autowired
  private CommodityService commodityService;

  @Autowired
  private SystemPropertyService systemPropertyService;

  @Autowired
  private IInventory2CDomainClient inventory2CDomainClient;

  @Autowired
  private SkuService skuService;

  public String queryCommodiesData(ItemListQuery itemListQuery) {
    if (null == itemListQuery) {
      return toJsonData(Result
          .getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    LOGGER.info(itemListQuery.toString());
    Result<Map<String, Object>> result = commodityService.queryCommodies(itemListQuery);// 请求后台数据
    List<ItemDTO> itemDTOs = (List<ItemDTO>) (result.getModule()).get("itemList");
    if (null == itemDTOs) {
      return toJsonData(Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "商品数据为空"));
    }
    List<BaseItemVO> baseItemVOs = itemDTOs2BaseItemVOs(itemDTOs);

    result.getModule().remove("itemList");
    result.getModule().put("baseItemVOs", baseItemVOs);
    return toJsonData(result);
  }

  /**
   * DTO列表转换为VO列表
   * 
   * @param itemDTOs
   * @return
   */
  public List<BaseItemVO> itemDTOs2BaseItemVOs(List<ItemDTO> itemDTOs) {
    if (null == itemDTOs || itemDTOs.size() == 0) {
      return null;
    }
    List<Long> skuIds = Lists.newArrayList();
    for (ItemDTO itemDTO : itemDTOs) {
      List<ItemCommoditySkuDTO> itemCommoditySkuDTOs = itemDTO.getItemSkuDtos();
      if (CollectionUtils.isNotEmpty(itemCommoditySkuDTOs)) {
        skuIds.add(itemCommoditySkuDTOs.get(0).getSkuId());
      }
    }
    if (CollectionUtils.isEmpty(skuIds)) {
      return null;
    }
    SimpleResultVO<List<InventoryVO>> simpleResultVO = null;
    try {
      // 批量获取商品库存
      simpleResultVO = inventory2CDomainClient.batchGetInventoryBySKUID(1L, skuIds);
    } catch (Exception e) {
      LOGGER.error("批量获取库存出错：" + e.getMessage());
    }
    List<InventoryVO> inventoryVOs = Lists.newArrayList();
    // 获取库存sku id列表，这里这么做是避免skuIds集合里面有重复的sku id,inventorySkuIds集合中没有重复的sku id
    List<Long> inventorySkuIds = Lists.newArrayList();
    if (null != simpleResultVO && simpleResultVO.isSuccess()
        && CollectionUtils.isNotEmpty(simpleResultVO.getTarget())) {
      inventoryVOs = simpleResultVO.getTarget();
      for (InventoryVO inventoryVO : inventoryVOs) {
        inventorySkuIds.add(inventoryVO.getSkuId());
      }
    }
    // 设置商品标详细情况
    Result<List<ItemTagDetailVO>> itemTagDetailVOResult = queryItemTagsDetail(skuIds, (short) 1);
    List<BaseItemVO> baseItemVOs = Lists.newArrayList();
    for (ItemDTO itemDTO : itemDTOs) {
      BaseItemVO baseItemVO = new BaseItemVO();
      BeanCopierUtils.copyProperties(itemDTO, baseItemVO);
      baseItemVO.setPriceYuanString(itemDTO.queryPriceYuanString());
      baseItemVO.setDiscountPriceYuanString(itemDTO.queryDiscountPriceYuanString());
      List<ItemCommoditySkuDTO> itemCommoditySkuDTOs = itemDTO.getItemSkuDtos();
      List<ItemSkuVO> itemSkuVOs = Lists.newArrayList();
      if (CollectionUtils.isEmpty(itemCommoditySkuDTOs)) {
        baseItemVOs.add(baseItemVO);
        continue;
      }
      // 目前一个商品对应一个sku
      ItemCommoditySkuDTO itemCommoditySkuDTO = itemCommoditySkuDTOs.get(0);
      ItemSkuVO itemSkuVO = new ItemSkuVO();
      // 拷贝sku的后台skuID，商品id，库存，sku单位等
      BeanCopierUtils.copyProperties(itemCommoditySkuDTO, itemSkuVO);
      // 获取库存数据
      if (CollectionUtils.isNotEmpty(inventoryVOs)) {
        // 获取当前sku id在inventorySkuIds中的位置，进而获取准确的库存信息
        int index = inventorySkuIds.indexOf(itemCommoditySkuDTO.getSkuId());
        if (index >= 0) {
          InventoryVO inventoryVO = inventoryVOs.get(index);
          Integer inventory = inventoryVO.getMaySale() ? 1 : 0;
          itemSkuVO.setInventory(inventory);
          LOGGER.info("商品sku id：" + itemCommoditySkuDTO.getSkuId() + ",库存数为："
              + inventoryVO.getNumber() + "，是否可销售：" + inventoryVO.getMaySale());
        } else {
          // 库存数据库中没有就设为0
          itemSkuVO.setInventory(0);
        }
        Integer tags = itemCommoditySkuDTO.getTags();
        resetItemInventory(tags, itemCommoditySkuDTO.getSkuId(), itemSkuVO);
      } else {
        itemSkuVO.setInventory(0);
      }
      itemSkuVO.setOriginplace(itemCommoditySkuDTO.queryOriginpace());
      itemSkuVO.setSaleDetailInfo(itemCommoditySkuDTO.querySpecification());
      if (null != itemCommoditySkuDTO.getTags()) {
        itemSkuVO.setTags(Integer.toBinaryString(itemCommoditySkuDTO.getTags()));
      }
      itemSkuVOs.add(itemSkuVO);
      // 设置商品标详细情况
      if (null != itemTagDetailVOResult && itemTagDetailVOResult.getSuccess()
          && null != itemTagDetailVOResult.getModule()) {
        List<ItemTagDetailVO> itemTagDetailVOs = itemTagDetailVOResult.getModule();
        int index = skuIds.indexOf(itemSkuVOs.get(0).getSkuId());
        ItemTagDetailVO itemTagDetailVO = new ItemTagDetailVO();
        if (index >= 0 && index < itemTagDetailVOs.size()) {
          itemTagDetailVO = itemTagDetailVOs.get(index);
        }
        itemSkuVOs.get(0).setTagDetail(itemTagDetailVO);
      }
      baseItemVO.setItemSkuVOs(itemSkuVOs);
      baseItemVOs.add(baseItemVO);
    }
    return baseItemVOs;
  }

  // /**
  // * 设置商品标
  // *
  // * @param itemCommoditySkuDTO
  // * @return
  // */
  // public Integer setItemTags(ItemCommoditySkuDTO itemCommoditySkuDTO) {
  // if (null != itemCommoditySkuDTO.getTags()
  // && itemCommoditySkuDTO.getTags() > 0
  // && ((itemCommoditySkuDTO.getTags() & 0x8) != 0 || (itemCommoditySkuDTO.getTags() & 0x80) != 0))
  // {
  // Result<SystemConfigDTO> systemConfigResult = systemPropertyService.getSystemConfig(1);
  // if (null != systemConfigResult && systemConfigResult.getSuccess()
  // && null != systemConfigResult.getModule()
  // && StringUtils.isNotBlank(systemConfigResult.getModule().getConfigInfo())) {
  // Map<String, String> itemMap =
  // (Map<String, String>) JackSonUtil.jsonToObject(systemConfigResult.getModule()
  // .getConfigInfo(), Map.class);
  // for (String key : itemMap.keySet()) {
  // String[] timeRegion = key.split("#");
  // SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
  // if (timeRegion.length < 2 || !isValidDate(timeRegion[0]) || !isValidDate(timeRegion[1])) {
  // continue;
  // }
  // try {
  // long start = sdf.parse(timeRegion[0]).getTime();
  // long now = sdf.parse(sdf.format(new Date())).getTime();
  // long end = sdf.parse(timeRegion[1]).getTime();
  // List<String> itemSkuIds = Arrays.asList(itemMap.get(key).split(","));
  // if (start < end) {
  // if ((now >= start && now < end)
  // && itemSkuIds.contains(String.valueOf(itemCommoditySkuDTO.getSkuId()))) {
  // return 0;
  // }
  // } else if (start > end) {
  // if ((now >= start || now < end)
  // && itemSkuIds.contains(String.valueOf(itemCommoditySkuDTO.getSkuId()))) {
  // return 0;
  // }
  // }
  // } catch (ParseException e) {
  // e.printStackTrace();
  // }
  // }
  // } else {
  // LOGGER.info("getSystemConfig查询失败，结果：" + JackSonUtil.getJson(systemConfigResult));
  // }
  // }
  // return itemCommoditySkuDTO.getTags();
  // }

  /**
   * 重新设置商品库存
   * 
   * @param tags
   * @param skuId
   * @param itemSkuVO
   */
  public void resetItemInventory(Integer tags, Long skuId, ItemSkuVO itemSkuVO) {
    if (null == tags || tags <= 0 || ((tags & 0x8) == 0 && (tags & 0x80) == 0)) {
      return;
    }
    Result<SystemConfigDTO> systemConfigResult = systemPropertyService.getSystemConfig(1);
    if (null != systemConfigResult && systemConfigResult.getSuccess()
        && null != systemConfigResult.getModule()
        && StringUtils.isNotBlank(systemConfigResult.getModule().getConfigInfo())) {
      Map<String, String> itemMap =
          (Map<String, String>) JackSonUtil.jsonToObject(systemConfigResult.getModule()
              .getConfigInfo(), Map.class);
      for (String key : itemMap.keySet()) {
        String[] timeRegion = key.trim().split("#");
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        if (timeRegion.length < 1 || !isValidDate(timeRegion[0]) || !isValidDate(timeRegion[1])) {
          continue;
        }
        try {
          long start = sdf.parse(timeRegion[0]).getTime();
          long now = sdf.parse(sdf.format(new Date())).getTime();
          long end = sdf.parse(timeRegion[1]).getTime();
          List<String> itemSkuIds = Arrays.asList(itemMap.get(key).split(","));
          if (start < end) {
            if ((now < start || now >= end) && itemSkuIds.contains(String.valueOf(skuId))) {
              itemSkuVO.setInventory(0);
            }
          } else if (start > end) {
            if ((now < start && now >= end) && itemSkuIds.contains(String.valueOf(skuId))) {
              itemSkuVO.setInventory(0);
            }
          }
        } catch (ParseException e) {
          e.printStackTrace();
        }
      }
    } else {
      LOGGER.info("getSystemConfig查询失败，结果：" + JackSonUtil.getJson(systemConfigResult));
    }
  }

  /**
   * 将传进来的数据转换为json数据
   * 
   * @param result
   * @return
   */
  public String toJsonData(Object result) {
    String jsonResult = JackSonUtil.getJson(result);
    return jsonResult;
  }

  /**
   * 查询商品标详细信息
   * 
   * @param channel
   * 
   * @param skus
   * @return
   */
  public Result<List<ItemTagDetailVO>> queryItemTagsDetail(List<Long> skuIds, Short channel) {
    if (CollectionUtils.isEmpty(skuIds)) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空");
    }
    if (null == channel) {
      channel = 1;
    }
    List<ItemTagDetailVO> itemTagDetailVOs = Lists.newArrayList();
    Result<List<ItemSkuTagsDTO>> itemSkuTagsResult = null;
    try {
      itemSkuTagsResult = skuService.querySkuTagsDetail(skuIds);
    } catch (Exception e) {
      LOGGER.error(e.getMessage());
    }
    if (null == itemSkuTagsResult || !itemSkuTagsResult.getSuccess()
        || CollectionUtils.isEmpty(itemSkuTagsResult.getModule())) {
      LOGGER.error("商品sku id：" + JackSonUtil.getJson(skuIds) + "下的商品标数据为空");
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_DATA_NOT_EXIST, "数据为空");
    }
    List<ItemSkuTagsDTO> itemSkuTagsDTOs = itemSkuTagsResult.getModule();
    List<Long> itemSkuIds = Lists.newArrayList();
    for (ItemSkuTagsDTO itemSkuTagsDTO : itemSkuTagsDTOs) {
      itemSkuIds.add(itemSkuTagsDTO.getSkuId());
    }
    for (Long skuId : skuIds) {
      int index = itemSkuIds.indexOf(skuId);
      ItemTagDetailVO itemTagDetailVO = new ItemTagDetailVO();
      itemTagDetailVO.setSkuId(skuId);
      itemTagDetailVO.setChannel(channel);
      // 解析tags位
      if (index < itemSkuTagsDTOs.size()) {
        parseItemTags(itemSkuTagsDTOs.get(index), itemTagDetailVO);
      }
      itemTagDetailVOs.add(itemTagDetailVO);
    }

    return Result.getSuccDataResult(itemTagDetailVOs);
  }

  /**
   * 解析商品标
   * 
   * @param itemSkuTagsDTO
   * @param itemTagDetailVO
   * @param channel
   */
  private void parseItemTags(ItemSkuTagsDTO itemSkuTagsDTO, ItemTagDetailVO itemTagDetailVO) {
    Integer tags = itemSkuTagsDTO.getTags();
    if (null == tags) {
      tags = 0;
    }

    itemTagDetailVO.setMaySale(itemSkuTagsDTO.getMayInventoryEnough());
    itemTagDetailVO.setMayPlus(itemSkuTagsDTO.getMayPlus());
    itemTagDetailVO.setMayOrder(itemSkuTagsDTO.getMayOrder());
    itemTagDetailVO.setSpecialTag(true);
    joinSkuTagInfo(itemSkuTagsDTO, itemTagDetailVO);
  }

  /**
   * 判断日期是否为合法日期
   * 
   * @param inDate
   * @return
   */
  private boolean isValidDate(String inDate) {
    if (inDate == null) {
      return false;
    }
    // set the format to use as a constructor argument
    SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
    timeFormat.setLenient(false);
    try {
      // parse the inDate parameter
      timeFormat.parse(inDate.trim());
    } catch (ParseException pe) {
      return false;
    }
    return true;
  }

  /**
   * 合并细分后的skutag信息，统一出整体对外的tag信息
   * 
   * @param result
   * @param itemTagDetailVO
   */
  private void joinSkuTagInfo(ItemSkuTagsDTO result, ItemTagDetailVO itemTagDetailVO) {
    // 遍历 SkuTagDetailList 列表，整合整体对外的结果
    String tagInfo = null;
    List<SkuTagDetail> skuTagDetails = result.getSkuTagDetailList();
    if (CollectionUtils.isEmpty(skuTagDetails)) {
      return;
    }
    LOGGER.info(JackSonUtil.getJson(skuTagDetails));
    Map<Integer, SkuTagDetail> tagsMap = Maps.newHashMap();
    for (SkuTagDetail skuTagDetail : skuTagDetails) {
      int key = 0;
      // TODO:待优化
      String tagId = skuTagDetail.getTagId();
      if (StringUtils.isBlank(tagId)
          || !channelConsistence(itemTagDetailVO.getChannel(), skuTagDetail.getChannel())) {
        continue;
      }
      switch (tagId) {
        case SkuTagDefinitionConstants.DES_TAG:
          key = 1;
          break;
        case SkuTagDefinitionConstants.NOT_ALLOW_CHARGE:
          key = 2;
          break;
        case SkuTagDefinitionConstants.NOT_ALLOW_SHOPPINGCART:
          key = 3;
          break;
        case SkuTagDefinitionConstants.NOT_ENOUGH_INVENTORY:
          key = 4;
          break;
        case SkuTagDefinitionConstants.BUY_LIMIT:
          skuTagDetail.setTagName(new StringBuilder().append("限购")
              .append(skuTagDetail.getTagRule()).append("件").toString());
          key = 5;
          break;
        case SkuTagDefinitionConstants.BUY_SKU_DISCOUNT_RATE:
          key = 6;
          break;
        case SkuTagDefinitionConstants.BUY_SKU_SEND_COUPON:
          key = 7;
          break;
        case SkuTagDefinitionConstants.SEND_ONE_BUY_MORE_THAN_TWO:
          key = 8;
          break;
        case SkuTagDefinitionConstants.BUY_DISCOUNT_99_40:
          key = 9;
          break;
        case SkuTagDefinitionConstants.BUY_DISCOUNT_40_20:
          key = 10;
          break;
      }
      if (null != tagsMap.get(key) && null != tagsMap.get(key).getTagId()) {
        skuTagDetail.setChannel((short) 0);
      }
      tagsMap.put(key, skuTagDetail);
    }
    Short channel = null;
    for (int i = 1; i < 11; i++) {
      SkuTagDetail skuTagDetail = tagsMap.get(i);
      if (null != skuTagDetail && null != skuTagDetail.getTagId()) {
        tagInfo = skuTagDetail.getTagName();
        channel = skuTagDetail.getChannel();
        break;
      }
    }
    if (null == tagInfo && null != tagsMap.get(0)) {
      tagInfo = tagsMap.get(0).getTagName();
      channel = tagsMap.get(0).getChannel();
    }
    if ((null != channel && channel == 0) || (itemTagDetailVO.getChannel() == 0)) {
      channel = itemTagDetailVO.getChannel();
    }
    if (null != channel && channel.shortValue() == itemTagDetailVO.getChannel().shortValue()
        && StringUtils.isEmpty(itemTagDetailVO.getTagContent()) && StringUtils.isNotBlank(tagInfo)) {
      itemTagDetailVO.setTagContent(tagInfo);
    }
  }

  /**
   * 判断两个渠道是否一致
   * 
   * @param channel
   * @param anotherChannel
   * @return
   */
  private boolean channelConsistence(Short channel, Short anotherChannel) {
    if (null == channel || null == anotherChannel || channel.shortValue() == 0
        || anotherChannel.shortValue() == 0 || channel.shortValue() == anotherChannel.shortValue()) {
      return true;
    }
    return false;
  }
}
