package com.xianzaishi.wapcenter.client.promotion;

/**
 * Created by Administrator on 2016/12/29 0029.
 */

/**
 * 展示计算后的结果
 *
 * @author jianmo
 * @create 2016-12-29 下午 1:20
 **/
public class DiscountInfoVO {


    /**
     * 计算后得到的页面展示结果
     */
    private String discountDesc;

    /**
     * 原始价
     */
    private double originPrice;

    /**
     * 优惠价
     */
    private double discountPrice;

    /**
     * 当前价
     */
    private double currPrice;

    public String getDiscountDesc() {
        return discountDesc;
    }

    public void setDiscountDesc(String discountDesc) {
        this.discountDesc = discountDesc;
    }

    public double getOriginPrice() {
        return originPrice;
    }

    public void setOriginPrice(double originPrice) {
        this.originPrice = originPrice;
    }

    public double getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(double discountPrice) {
        this.discountPrice = discountPrice;
    }

    public double getCurrPrice() {
        return currPrice;
    }

    public void setCurrPrice(double currPrice) {
        this.currPrice = currPrice;
    }
}
