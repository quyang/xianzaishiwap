package com.xianzaishi.wapcenter.client.mine.vo;

import java.io.Serializable;

public class UserContactVO implements Serializable {
  
  /**
   * serial version UID
   */
  private static final long serialVersionUID = 6301568760562876870L;

  /**
   * 联系人id
   */
  private Long contactUserId;
  
  /**
   * 联系人名字
   */
  private String name;
  
  /**
   * 手机号
   */
  private Long phone;
  
  /**
   * 1:默认收货，0:普通收货
   */
  private Short type;
  
  /**
   * 收货地址区域
   */
  private String area;
  
  /**
   * 收货楼号门牌
   */
  private String address;

  public Long getContactUserId() {
    return contactUserId;
  }

  public void setContactUserId(Long contactUserId) {
    this.contactUserId = contactUserId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getPhone() {
    return phone;
  }

  public void setPhone(Long phone) {
    this.phone = phone;
  }

  public Short getType() {
    return type;
  }

  public void setType(Short type) {
    this.type = type;
  }

  public String getArea() {
    return area;
  }

  public void setArea(String area) {
    this.area = area;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

}
