package com.xianzaishi.wapcenter.client.order.vo;

public class OrderResponseVO {

  /**
   * 错误编号 >=1表示成功 <=-1表示失败
   */
  protected int code;
  /**
   * 错误信息
   */
  protected String message = "成功";
  /**
   * 返回的具体实体对象
   */
  protected Object data = null;
  
  /**
   * 成功码
   */
  private boolean success;
  
  /**
   * 错误码
   */
  private boolean error;

  public int getCode() {
    return code;
  }

  public void setCode(int code) {
    this.code = code;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Object getData() {
    return data;
  }

  public void setData(Object data) {
    this.data = data;
  }

  public boolean isSuccess() {
    return success;
  }

  public void setSuccess(boolean success) {
    this.success = success;
  }

  public boolean isError() {
    return error;
  }

  public void setError(boolean error) {
    this.error = error;
  }
  
}
