package com.xianzaishi.wapcenter.client.activity.vo;

import java.io.Serializable;
import java.util.List;

import com.google.common.base.Preconditions;
import com.xianzaishi.wapcenter.client.market.vo.BaseItemVO;

/**
 * 首页接口数据字段
 * @author dongpo
 *
 */
public class ActivityStepVO implements Serializable {
  /**
   * serial version UID
   */
  private static final long serialVersionUID = 9165531558531800253L;
  
  private String pageName;
  
  private Integer stepId;
  
  /**
   * 楼层中的图片列表
   */
  private List<ActivityStepPicVO> picList;

  /**
   * 楼层的标题
   */
  private String title;

  /**
   * 楼层中的商品列表
   */
  private List<BaseItemVO> items;

  /**
   * 楼层类型
   */
  private Integer stepType;

  /**
   * 类目数据信息
   */
  private List<ActivityStepCatVO> catList;
  
  /**
   * 对外提供的活动信息
   */
  private List<ActivityStepContentVO> contentList;
  
  /**
   * 内部套嵌结构数据
   */
  private List<ActivityStepVO> innerStepInfo;
  
  /**
   * 开始时间
   */
  private Long begin;
  
  /**
   * 结束时间
   */
  private Long end;
  
  public String getPageName() {
    return pageName;
  }

  public void setPageName(String pageName) {
    this.pageName = pageName;
  }
  
  public Integer getStepId() {
    return stepId;
  }

  public void setStepId(Integer stepId) {
    this.stepId = stepId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }


  public List<BaseItemVO> getItems() {
    return items;
  }

  public void setItems(List<BaseItemVO> items) {
    Preconditions.checkNotNull(items, "items is null");
    Preconditions.checkArgument(items.size() > 0, "items'size must be greater than 0");
    this.items = items;
  }

  public List<ActivityStepPicVO> getPicList() {
    return picList;
  }

  public void setPicList(List<ActivityStepPicVO> picList) {
    this.picList = picList;
  }

  public Integer getStepType() {
    return stepType;
  }

  public void setStepType(Integer stepType) {
    this.stepType = stepType;
  }

  public List<ActivityStepVO> getInnerStepInfo() {
    return innerStepInfo;
  }

  public void setInnerStepInfo(List<ActivityStepVO> innerStepInfo) {
    this.innerStepInfo = innerStepInfo;
  }

  public List<ActivityStepCatVO> getCatList() {
    return catList;
  }

  public void setCatList(List<ActivityStepCatVO> catList) {
    this.catList = catList;
  }

  public Long getBegin() {
    return begin;
  }

  public void setBegin(Long begin) {
    this.begin = begin;
  }

  public Long getEnd() {
    return end;
  }

  public void setEnd(Long end) {
    this.end = end;
  }

  public List<ActivityStepContentVO> getContentList() {
    return contentList;
  }

  public void setContentList(List<ActivityStepContentVO> contentList) {
    this.contentList = contentList;
  }
  
}
