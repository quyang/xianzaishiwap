package com.xianzaishi.wapcenter.client.market.vo.query;

import java.io.Serializable;
import java.util.List;

/**
 * 商品list查询条件
 * @author dongpo
 *
 */
public class ItemListQueryVO implements Serializable {
  
  /**
   * serial version UID
   */
  private static final long serialVersionUID = 8731294682920376486L;

  /**
   * 商品id组
   */
  private List<Long> itemIds;

  /**
   * 商品list当前页
   */
  private Integer pageNum;

  /**
   * 商品list页面大小
   */
  private Integer pageSize;

  /**
   * 商品标题
   */
  private String itemTitle;

  /**
   * 商品价格区间:左值
   */
  private Integer startPrice;

  /**
   * 商品价格区间:右值
   */
  private Integer endPrice;

  /**
   * 通过多个后台类目id来查询商品
   */
  private Integer categoryId;

  /**
   * 商店id
   */
  private Integer shopId;

  /**
   * 是否包含sku属性
   */
  private Boolean hasItemSku = false;

  /**
   * 是否返回商品全部属性
   */
  private Boolean returnAll = false;

  /**
   * 排序方式
   */
  private String orderBy;

  /**
   * 是否按升序来排序
   */
  private Boolean isAscendingOrder = false;


  public List<Long> getItemIds() {
    return itemIds;
  }

  public void setItemIds(List<Long> itemIds) {
    for (Long itemId : itemIds) {
      if (itemId <= 0) {
        throw new IllegalArgumentException("itemId must be greater than 0");
      }
    }
    this.itemIds = itemIds;
  }

  public Boolean getHasItemSku() {
    return hasItemSku == null ? false : hasItemSku;
  }

  public void setHasItemSku(Boolean hasItemSku) {
    this.hasItemSku = hasItemSku;
  }

  public Boolean getReturnAll() {
    return returnAll == null ? false : returnAll;

  }

  public void setReturnAll(Boolean returnAll) {
    this.returnAll = returnAll;
  }

  public Boolean getIsAscendingOrder() {
    return isAscendingOrder;
  }

  public void setIsAscendingOrder(Boolean isAscendingOrder) {
    this.isAscendingOrder = isAscendingOrder;
  }

  public String getOrderBy() {
    return orderBy;
  }

  public void setOrderBy(String orderBy) {
    this.orderBy = orderBy;
  }

  public Integer getPageNum() {
    return pageNum == null ? 0 : pageNum;
  }

  public void setPageNum(Integer pageNum) {
    if (pageNum != null && pageNum < 0) {
      throw new IllegalArgumentException("pageNum can not be smaller than 0");
    }
    this.pageNum = pageNum;
  }

  public Integer getPageSize() {
    return pageSize;
  }

  public void setPageSize(Integer pageSize) {
    if (pageSize != null && pageSize < 0) {
      throw new IllegalArgumentException("pageSize can not be smaller than 0");
    }
    this.pageSize = pageSize;
  }

  public String getItemTitle() {
    return itemTitle;
  }

  public void setItemTitle(String itemTitle) {
    this.itemTitle = itemTitle;
  }


  public Integer getStartPrice() {
    return startPrice == null ? 0 : startPrice;
  }

  public void setStartPrice(Integer startPrice) {
    if (startPrice != null && startPrice < 0) {
      throw new IllegalArgumentException("startPrice can not be smaller than 0");
    }
    this.startPrice = startPrice;
  }

  public Integer getEndPrice() {
    return endPrice == null ? 0 : endPrice;
  }

  public void setEndPrice(Integer endPrice) {
    if (startPrice != null && startPrice < 0) {
      throw new IllegalArgumentException("startPrice can not be smaller than 0");
    }
    this.endPrice = endPrice;
  }

  public Integer getCategoryId() {
    return categoryId;
  }

  public void setCategoryIds(Integer categoryId) {
    
    this.categoryId = categoryId;
  }

  public Integer getShopId() {
    return shopId == null ? 0 : shopId;
  }

  public void setShopId(Integer shopId) {
    if (shopId != null && shopId <= 0) {
      throw new IllegalArgumentException("shopId must be greater than 0");
    }
    this.shopId = shopId;
  }


}
