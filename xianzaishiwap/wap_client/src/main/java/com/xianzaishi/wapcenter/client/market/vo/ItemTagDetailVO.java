package com.xianzaishi.wapcenter.client.market.vo;

public class ItemTagDetailVO {
  
  /**
   * 商品sku id
   */
  private Long skuId;
  
  /**
   * 是否可加购
   */
  private Boolean mayPlus = true;
  
  /**
   * 是否可下单
   */
  private Boolean mayOrder = true;
  
  /**
   * 是否有库存
   */
  private Boolean maySale = true;
  
  /**
   * 是否有特殊标记
   */
  private Boolean specialTag = false;
  
  /**
   * 渠道
   */
  private Short channel;
  
  /**
   * 商品标
   */
  private String tagContent;

  public Long getSkuId() {
    return skuId;
  }

  public void setSkuId(Long skuId) {
    this.skuId = skuId;
  }

  public Boolean getMayPlus() {
    return mayPlus;
  }

  public void setMayPlus(Boolean mayPlus) {
    this.mayPlus = mayPlus;
  }

  public Boolean getMayOrder() {
    return mayOrder;
  }

  public void setMayOrder(Boolean mayOrder) {
    this.mayOrder = mayOrder;
  }

  public Boolean getMaySale() {
    return maySale;
  }

  public void setMaySale(Boolean maySale) {
    this.maySale = maySale;
  }

  public Boolean getSpecialTag() {
    return specialTag;
  }

  public void setSpecialTag(Boolean specialTag) {
    this.specialTag = specialTag;
  }

  public String getTagContent() {
    return tagContent;
  }

  public void setTagContent(String tagContent) {
    this.tagContent = tagContent;
  }

  public Short getChannel() {
    return channel;
  }

  public void setChannel(Short channel) {
    this.channel = channel;
  }
  
}
