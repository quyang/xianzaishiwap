package com.xianzaishi.wapcenter.client.mine.vo;

/**
 * Created by quyang on 2017/2/23.
 */
public class UserProfileQueryVO {

  /**
   * 用户id
   */
  private Long userId;


  /**
   * 消息id
   */
  private String regitstId;
  /**
   * token
   */
  private String token;
  /**
   * 设备号
   */
  private String equipmentNumber;
  /**
   *
   */
  private String ecode;
  /**
   * app版本
   */
  private String version;

  public String getEquipmentNumber() {
    return equipmentNumber;
  }

  public void setEquipmentNumber(String equipmentNumber) {
    this.equipmentNumber = equipmentNumber;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public String getRegitstId() {
    return regitstId;
  }

  public void setRegitstId(String regitstId) {
    this.regitstId = regitstId;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public String getEcode() {
    return ecode;
  }

  public void setEcode(String ecode) {
    this.ecode = ecode;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }
}
