package com.xianzaishi.wapcenter.client.market.vo;

import java.io.Serializable;
import java.util.List;

import com.google.common.base.Preconditions;

/**
 * 商品接口list字段
 * @author dongpo
 *
 */
public class BaseItemVO implements Serializable {
  
  /**
   * serial version UID
   */
  private static final long serialVersionUID = 4867054690468213766L;

  /**
   * 商品id
   */
  private Long itemId;

  /**
   * 商品图片地址
   */
  private String picUrl;

  /**
   * 商品标题
   */
  private String title;

  /**
   * 商品副标题
   */
  private String subtitle;

  /**
   * 原价，以分为单位
   */
  private Integer price;

  /**
   * 商品折扣价，以分为单位
   */
  private Integer discountPrice;
  
  /**
   * 获取元为单位的价格，字符串表示
   */
  private String priceYuanString;
  
  /**
   * 获取元为单位的价格，字符串表示
   */
  private String discountPriceYuanString;

  /**
   * sku id列表
   */
  private String sku;

  /**
   * 商品特征，包含类目标准属性，扩展结构和商品特征值，sku id列表
   */
  private String feature;
  
  /**
   * 商品库存
   */
  private Integer inventory;

  /**
   * sku列表
   */
  private List<ItemSkuVO> itemSkuVOs;
  
  /**
   * 前台类目id
   */
  private Integer cmCat2;

  public Long getItemId() {
    return itemId == null ? 0 : itemId;
  }

  public void setItemId(Long itemId) {
    Preconditions.checkNotNull(itemId, "itemId is null");
    Preconditions.checkArgument(itemId > 0, "itemId must be greater than 0");
    this.itemId = itemId;
  }

  public String getPicUrl() {
    return picUrl;
  }

  public void setPicUrl(String picUrl) {
    this.picUrl = picUrl;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getSubtitle() {
    return subtitle;
  }

  public void setSubtitle(String subtitle) {
    this.subtitle = subtitle;
  }

  public Integer getPrice() {
    return price == null ? 0 : price;
  }

  public void setPrice(Integer price) {
    Preconditions.checkNotNull(price, "price is null");
    Preconditions.checkArgument(price > 0, "price must be greater than 0");
    this.price = price;
  }

  public Integer getDiscountPrice() {
    return discountPrice == null ? 0 : discountPrice;
  }

  public void setDiscountPrice(Integer discountPrice) {
    Preconditions.checkNotNull(discountPrice, "discountPrice is null");
    Preconditions.checkArgument(discountPrice >= 0, "discountPrice must be greater than 0");
    this.discountPrice = discountPrice;
  }
  
  public String getPriceYuanString() {
    return priceYuanString;
  }

  public void setPriceYuanString(String priceYuanString) {
    this.priceYuanString = priceYuanString;
  }

  public String getDiscountPriceYuanString() {
    return discountPriceYuanString;
  }

  public void setDiscountPriceYuanString(String discountPriceYuanString) {
    this.discountPriceYuanString = discountPriceYuanString;
  }

  public String getSku() {
    return sku;
  }

  public void setSku(String sku) {
    this.sku = sku;
  }

  public String getFeature() {
    return feature;
  }

  public void setFeature(String feature) {
    this.feature = feature;
  }

  public Integer getInventory() {
    return inventory == null ? 0 : inventory;
  }

  public void setInventory(Integer inventory) {
    Preconditions.checkNotNull(inventory, "inventory is null");
    Preconditions.checkArgument(inventory >= 0, "inventory must be greater than 0");
    this.inventory = inventory;
  }

  public List<ItemSkuVO> getItemSkuVOs() {
    return itemSkuVOs;
  }

  public void setItemSkuVOs(List<ItemSkuVO> itemSkuVOs) {
    this.itemSkuVOs = itemSkuVOs;
  }

  public Integer getCmCat2() {
    return cmCat2;
  }

  public void setCmCat2(Integer cmCat2) {
    this.cmCat2 = cmCat2;
  }

}
