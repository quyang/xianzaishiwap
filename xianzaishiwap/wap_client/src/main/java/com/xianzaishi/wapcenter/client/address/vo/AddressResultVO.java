package com.xianzaishi.wapcenter.client.address.vo;

import java.util.List;

/**
 * 高德地图地理编码返回结果
 * @author dongpo
 *
 */
public class AddressResultVO {
  private String status;
  
  private String count;
  
  private String info;
  
  private String infocode;
  
  private List<AddressGeocodeVO> geocodes;

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getCount() {
    return count;
  }

  public void setCount(String count) {
    this.count = count;
  }

  public String getInfo() {
    return info;
  }

  public void setInfo(String info) {
    this.info = info;
  }

  public String getInfocode() {
    return infocode;
  }

  public void setInfocode(String infocode) {
    this.infocode = infocode;
  }

  public List<AddressGeocodeVO> getGeocodes() {
    return geocodes;
  }

  public void setGeocodes(List<AddressGeocodeVO> geocodes) {
    this.geocodes = geocodes;
  }
  
}
