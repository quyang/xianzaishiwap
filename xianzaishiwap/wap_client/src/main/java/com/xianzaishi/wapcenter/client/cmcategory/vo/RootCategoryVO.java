package com.xianzaishi.wapcenter.client.cmcategory.vo;

import java.util.List;

/**
 * 类目
 * @author dongpo
 *
 */
public class RootCategoryVO extends BaseCategoryVO {
  
  /**
   * 一级类目图片
   */
  private String picture;
  
  /**
   * 子类目
   */
  private List<RootCategoryVO> subCategoryVOs;
  
  /**
   * 类目描述
   */
  private String memo;
  
  /**
   * 一级类目id
   */
  private Integer rootCatId;
  
  /**
   * h5跳转链接
   */
  private String h5Url;

  
  public String getPicture() {
    return picture;
  }

  public void setPicture(String picture) {
    this.picture = picture;
  }

  public List<RootCategoryVO> getSubCategoryVOs() {
    return subCategoryVOs;
  }

  public void setSubCategoryVOs(List<RootCategoryVO> subCategoryVOs) {
    this.subCategoryVOs = subCategoryVOs;
  }

  public String getMemo() {
    return memo;
  }

  public void setMemo(String memo) {
    this.memo = memo;
  }

  public Integer getRootCatId() {
    return rootCatId;
  }

  public void setRootCatId(Integer rootCatId) {
    this.rootCatId = rootCatId;
  }

  public String getH5Url() {
    return h5Url;
  }

  public void setH5Url(String h5Url) {
    this.h5Url = h5Url;
  }
}
