package com.xianzaishi.wapcenter.client.market.vo.query;

import java.io.Serializable;

import com.xianzaishi.itemcenter.common.query.BaseQuery;

/**
 * 商品list查询条件
 * @author dongpo
 *
 */
public class ItemQueryVO extends BaseQuery implements Serializable {
  
  /**
   * serial version UID
   */
  private static final long serialVersionUID = 8731294682920376486L;


  /**
   * 通过多个后台二级类目id来查询商品
   */
  private Integer cmCat2Ids;
  
  /**
   * 后台叶子类目id
   */
  private Integer catId;
  

  /**
   * 排序方式
   */
  private String orderBy;

  /**
   * 是否按升序来排序
   */
  private Boolean isAscendingOrder = false;


  public Boolean getIsAscendingOrder() {
    return isAscendingOrder;
  }

  public void setIsAscendingOrder(Boolean isAscendingOrder) {
    this.isAscendingOrder = isAscendingOrder;
  }

  public String getOrderBy() {
    return orderBy;
  }

  public void setOrderBy(String orderBy) {
    this.orderBy = orderBy;
  }

  public Integer getCmCat2Ids() {
    return cmCat2Ids;
  }

  public void setCmCat2Ids(Integer cmCat2Ids) {
    this.cmCat2Ids = cmCat2Ids;
  }

  public Integer getCatId() {
    return catId;
  }

  public void setCatId(Integer catId) {
    this.catId = catId;
  }

}
