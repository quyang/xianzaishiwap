package com.xianzaishi.wapcenter.client.promotion;

import java.util.List;

public class BuyDetailVO {

  private String userToken;
  
  private int userId;
  
  /**
   * 支付渠道。1:线上调用传，2：属于线下收银机调用
   */
  private int payType;
  
  /**
   * 查询目的  0:查询用户所有可用优惠券，1:查询用户当前购买情况下可用优惠
   */
  private int queryCouponType;
  
  /**
   * 购买情况参数
   */
  private List<BuySkuInfoVO> skuInfoList;
  
  /**
   * 优惠券id
   */
  private long couponId;
  
  public String getUserToken() {
    return userToken;
  }

  public void setUserToken(String userToken) {
    this.userToken = userToken;
  }

  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  public int getPayType() {
    return payType;
  }

  public void setPayType(int payType) {
    this.payType = payType;
  }

  public int getQueryCouponType() {
    return queryCouponType;
  }

  public void setQueryCouponType(int queryCouponType) {
    this.queryCouponType = queryCouponType;
  }

  public List<BuySkuInfoVO> getSkuInfoList() {
    return skuInfoList;
  }

  public void setSkuInfoList(List<BuySkuInfoVO> skuInfoList) {
    this.skuInfoList = skuInfoList;
  }
  
  public long getCouponId() {
    return couponId;
  }

  public void setCouponId(long couponId) {
    this.couponId = couponId;
  }
}
