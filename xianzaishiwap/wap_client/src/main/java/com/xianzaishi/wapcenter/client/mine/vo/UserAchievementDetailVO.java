package com.xianzaishi.wapcenter.client.mine.vo;

public class UserAchievementDetailVO {
  
  /**
   * 用户id
   */
  private Long userId;
  
  /**
   * 用户手机号
   */
  private String phone;
  
  /**
   * 当前用户状态
   */
  private String statuString;

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getStatuString() {
    return statuString;
  }

  public void setStatuString(String statuString) {
    this.statuString = statuString;
  }
  
}
