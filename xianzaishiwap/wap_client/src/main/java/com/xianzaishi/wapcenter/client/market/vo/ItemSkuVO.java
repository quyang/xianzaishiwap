package com.xianzaishi.wapcenter.client.market.vo;

import java.io.Serializable;

import com.google.common.base.Preconditions;

public class ItemSkuVO implements Serializable {
  /**
   * serial version UID
   */
  private static final long serialVersionUID = 8170754954255549035L;

  /**
   * 后台skuID，系统使用，唯一标识一个sku
   */
  private Long skuId;

  /**
   * 对应后台采购\销售商品id
   */
  private Long itemId;

  /**
   * 对应库存数量
   */
  private Integer inventory;

  /**
   * sku单位，取标准属性库，比如kg、合、包、斤等<br/>
   * 针对采购sku，属于采购进货单位<br/>
   * 针对销售sku，属于销售sku单位<br/>
   * 采购进货单位与销售sku可能不同（拆包装出售时）
   */
  private Integer skuUnit;

  /**
   * sku标题
   */
  private String title;

  /**
   * 销量
   */
  private Integer quantity;
  
  /**
   * 促销信息
   */
  private String promotionInfo;
  
  /**
   * 原价，以分为单位
   */
  private Integer price;

  /**
   * 商品折扣价，以分为单位
   */
  private Integer discountPrice;
  
  /**
   * 获取元为单位的价格，字符串表示
   */
  private String priceYuanString;
  
  /**
   * 获取元为单位的价格，字符串表示
   */
  private String discountPriceYuanString;
  
  /**
   * 规格
   */
  private String saleDetailInfo;
  
  /**
   * 商品产地
   */
  private String originplace;
  
  /**
   * 商品打的各种标
   */
  private String tags;
  
  /**
   * 标详细信息
   */
  private ItemTagDetailVO tagDetail;
  
  
  public Long getSkuId() {
    return skuId == null ? 0 : skuId;
  }

  public void setSkuId(Long skuId) {
    Preconditions.checkNotNull(skuId, "skuId == null");
    Preconditions.checkArgument(skuId >= 0, "skuId <= 0");
    this.skuId = skuId;
  }

  public Long getItemId() {
    return itemId == null ? 0 : itemId;
  }

  public void setItemId(Long itemId) {
    Preconditions.checkNotNull(itemId, "itemId == null");
    Preconditions.checkArgument(itemId >= 0, "itemId <= 0");
    this.itemId = itemId;
  }


  public Integer getInventory() {
    return inventory == null ? 0 : inventory;
  }

  public void setInventory(Integer inventory) {
    Preconditions.checkNotNull(inventory, "inventory == null");
    this.inventory = inventory;
  }

  public Integer getSkuUnit() {
    return skuUnit == null ? 0 : skuUnit;
  }

  public void setSkuUnit(Integer skuUnit) {
    Preconditions.checkNotNull(skuUnit, "skuUnit == null");
    Preconditions.checkArgument(skuUnit >= 0, "skuUnit < 0");
    this.skuUnit = skuUnit;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title == null ? null : title.trim();
  }

  public Integer getQuantity() {
    return quantity;
  }

  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }

  public String getPromotionInfo() {
    return promotionInfo;
  }

  public void setPromotionInfo(String promotionInfo) {
    this.promotionInfo = promotionInfo;
  }
  
  public Integer getPrice() {
    return price;
  }

  public void setPrice(Integer price) {
    this.price = price;
  }

  public Integer getDiscountPrice() {
    return discountPrice;
  }

  public void setDiscountPrice(Integer discountPrice) {
    this.discountPrice = discountPrice;
  }

  public String getPriceYuanString() {
    return priceYuanString;
  }

  public void setPriceYuanString(String priceYuanString) {
    this.priceYuanString = priceYuanString;
  }

  public String getDiscountPriceYuanString() {
    return discountPriceYuanString;
  }

  public void setDiscountPriceYuanString(String discountPriceYuanString) {
    this.discountPriceYuanString = discountPriceYuanString;
  }

  public String getSaleDetailInfo() {
    return saleDetailInfo;
  }

  public void setSaleDetailInfo(String saleDetailInfo) {
    this.saleDetailInfo = saleDetailInfo;
  }
  

  public String getOriginplace() {
    return originplace;
  }

  public void setOriginplace(String originplace) {
    this.originplace = originplace;
  }

  public String getTags() {
    return tags;
  }

  public void setTags(String tags) {
    this.tags = tags;
  }

  public ItemTagDetailVO getTagDetail() {
    return tagDetail;
  }

  public void setTagDetail(ItemTagDetailVO tagDetail) {
    this.tagDetail = tagDetail;
  }
  
}
