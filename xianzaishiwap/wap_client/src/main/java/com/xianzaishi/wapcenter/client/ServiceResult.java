package com.xianzaishi.wapcenter.client;

import java.io.Serializable;

/**
 * 调用接口的响应结果存放类
 *
 */
public class ServiceResult<T> implements Serializable {
  /** */
  private static final long serialVersionUID = -7815723967101753647L;
  private String message;
  private T data;

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public T getData() {
    return data;
  }

  public void setData(T data) {
    this.data = data;
  }
}
