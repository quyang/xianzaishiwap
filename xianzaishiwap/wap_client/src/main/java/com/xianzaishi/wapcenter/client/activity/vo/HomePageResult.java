package com.xianzaishi.wapcenter.client.activity.vo;

import com.xianzaishi.itemcenter.common.result.Result;

public class HomePageResult<T> extends Result<T>{
  
  private static final long serialVersionUID = 6709441113120457245L;
  
  private String color;
  
  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  /**
   * object返回客户端需要的数据类型,result通用模板类型
   * 
   * @param object
   * @return result
   */
  public static <K> Result<K> getResult(K object, boolean isSuccess, int bizCode, String resultInfo) {
    HomePageResult<K> result = new HomePageResult<K>();
    result.setModule(object);
    result.setSuccess(isSuccess);
    result.setResultCode(bizCode);
    result.setCurrentTime(System.currentTimeMillis());
    result.setErrorMsg(resultInfo);
    result.setColor("FFFFFF");
    return result;
  }
}
