package com.xianzaishi.wapcenter.client.cmcategory.vo;

import java.io.Serializable;

public class LeafCategoryVO implements Serializable {
  
  /**
   * serial version UID
   */
  private static final long serialVersionUID = -6927518277128495695L;

  /**
   * 类目id
   */
  private Integer catId;
  
  /**
   * 类目名称
   */
  private String catName;
  
  /**
   * 父类目id
   */
  private Integer parentId;
  
  public Integer getCatId() {
    return catId;
  }

  public void setCatId(Integer catId) {
    this.catId = catId;
  }

  public String getCatName() {
    return catName;
  }

  public void setCatName(String catName) {
    this.catName = catName;
  }

  public Integer getParentId() {
    return parentId;
  }

  public void setParentId(Integer parentId) {
    this.parentId = parentId;
  }

}
