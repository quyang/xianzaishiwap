package com.xianzaishi.wapcenter.client.mine.vo;

import java.io.Serializable;
import java.util.Date;

import com.google.common.base.Preconditions;

/**
 * 用户中心数据字段
 * @author dongpo
 *
 */
public class MineVO implements Serializable {
  
  /**
   * serial version UID
   */
  private static final long serialVersionUID = 2768614154030937983L;

  /**
   * 用户id
   */
  private Long userId;
  
  /**
   * 用户名
   */
  private String name;
  
  /**
   * 用户头像
   */
  private String pic;
  
  /**
   * 用户类型
   */
  private Short userType;
  
  /**
   * 用户性别
   */
  private Short sex;
  
  /**
   * 用户生日
   */
  private Date birthday;
  
  /**
   * 优惠劵数量
   */
  private Integer couponCount;
  
  /**
   * 消息数量
   */
  private Integer messageCount;

  public Long getUserId() {
    return userId == null ? 0 : userId;
  }

  public void setUserId(Long userId) {
    Preconditions.checkNotNull(userId, "userId is null");
    Preconditions.checkArgument(userId > 0, "useId must be greater than 0");
    this.userId = userId;
  }
  
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPic() {
    return pic;
  }

  public void setPic(String pic) {
    this.pic = pic;
  }

  public Short getUserType() {
    return userType;
  }

  public void setUserType(Short useType) {
    this.userType = useType;
  }

  public Short getSex() {
    return sex;
  }

  public void setSex(Short sex) {
    this.sex = sex;
  }

  public Date getBirthday() {
    return birthday;
  }

  public void setBirthday(Date birthday) {
    this.birthday = birthday;
  }

  public Integer getCouponCount() {
    return couponCount == null ? 0 : couponCount;
  }

  public void setCouponCount(Integer couponCount) {
    Preconditions.checkNotNull(couponCount, "couponCount is null");
    Preconditions.checkArgument(couponCount>=0, "couponCount can not be smaller than 0");
    this.couponCount = couponCount;
  }

  public Integer getMessageCount() {
    return messageCount == null ? 0 : messageCount;
  }

  public void setMessageCount(Integer messageCount) {
    Preconditions.checkNotNull(messageCount, "messageCount is null");
    Preconditions.checkArgument(messageCount>=0, "messageCount can not be smaller than 0");
    this.messageCount = messageCount;
  }
  

}
