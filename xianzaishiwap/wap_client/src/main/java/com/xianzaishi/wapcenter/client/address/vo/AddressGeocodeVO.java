package com.xianzaishi.wapcenter.client.address.vo;

import java.util.List;

/**
 * POI信息
 * @author dongpo
 *
 */
public class AddressGeocodeVO {
  /**
   * 结构化地址信息
   */
  private String formatted_address;
  
  /**
   * 地址所在的省份名
   */
  private String province;
  
  /**
   * 城市编码
   */
  private String citycode;
  
  /**
   * 城市名
   */
  private String city;
  
  /**
   * 地址所在的区
   */
  private String district;
  
  /**
   * 地址所在的乡镇
   */
  private List<String> township;
  
  /**
   * 区域编码
   */
  private String adcode;
  
  /**
   * 街道
   */
  private List<String> street;
  
  /**
   * 门牌
   */
  private List<String> number;
  
  /**
   * 坐标点
   */
  private String location;
  
  /**
   * 匹配级别
   */
  private String level;

  public String getFormatted_address() {
    return formatted_address;
  }

  public void setFormatted_address(String formatted_address) {
    this.formatted_address = formatted_address;
  }

  public String getProvince() {
    return province;
  }

  public void setProvince(String province) {
    this.province = province;
  }

  public String getCitycode() {
    return citycode;
  }

  public void setCitycode(String citycode) {
    this.citycode = citycode;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getDistrict() {
    return district;
  }

  public void setDistrict(String district) {
    this.district = district;
  }

  public List<String> getTownship() {
    return township;
  }

  public void setTownship(List<String> township) {
    this.township = township;
  }

  public String getAdcode() {
    return adcode;
  }

  public void setAdcode(String adcode) {
    this.adcode = adcode;
  }

  public List<String> getStreet() {
    return street;
  }

  public void setStreet(List<String> street) {
    this.street = street;
  }

  public List<String> getNumber() {
    return number;
  }

  public void setNumber(List<String> number) {
    this.number = number;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public String getLevel() {
    return level;
  }

  public void setLevel(String level) {
    this.level = level;
  }
  
}
