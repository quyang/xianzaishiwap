package com.xianzaishi.wapcenter.client.cmcategory.vo;

import java.io.Serializable;

import com.google.common.base.Preconditions;

/**
 * 前台二级类目字段
 * @author dongpo
 *
 */
public class CategoryVO extends BaseCategoryVO implements Serializable {
  /**
   * serial version UID
   */
  private static final long serialVersionUID = -836323566348475771L;
  
  /**
   * 类目描述
   */
  private String memo;
  
  /**
   * 类目图片
   */
  private String pic;

  public String getMemo() {
    return memo;
  }

  public void setMemo(String memo) {
    Preconditions.checkNotNull(memo, "memo is null");
    this.memo = memo;
  }

  public String getPic() {
    return pic;
  }

  public void setPic(String pic) {
    this.pic = pic;
  }
  
  

}
