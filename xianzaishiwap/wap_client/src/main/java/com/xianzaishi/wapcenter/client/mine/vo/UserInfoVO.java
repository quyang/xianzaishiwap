package com.xianzaishi.wapcenter.client.mine.vo;

import java.io.Serializable;

import com.google.common.base.Preconditions;

public class UserInfoVO implements Serializable {

  /**
   * serial version UID
   */
  private static final long serialVersionUID = -4384048208566578342L;

  /**
   * 用户id
   */
  private Long userId;
  
  /**
   * 用户头像图片地址
   */
  private String pic;
  
  /**
   * 用户昵称
   */
  private String name;
  
  /**
   * 用户会员类型
   */
  private Short userType;
  
  /**
   * 性别
   */
  private Short sex;
  
  /**
   * 用户生日
   */
  private String birthday;

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    Preconditions.checkNotNull(userId, "userId nust not be null");
    Preconditions.checkArgument(userId > 0, "userId must be greater than 0");
    this.userId = userId;
  }

  public String getPic() {
    return pic;
  }

  public void setPic(String pic) {
    this.pic = pic;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Short getUserType() {
    return userType;
  }

  public void setUserType(Short userType) {
    this.userType = userType;
  }

  public Short getSex() {
    return sex;
  }

  public void setSex(Short sex) {
    this.sex = sex;
  }

  public String getBirthday() {
    return birthday;
  }

  public void setBirthday(String birthday) {
    this.birthday = birthday;
  }
  
}
  
  
