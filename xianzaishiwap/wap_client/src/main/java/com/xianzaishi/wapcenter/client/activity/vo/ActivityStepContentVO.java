package com.xianzaishi.wapcenter.client.activity.vo;

import java.io.Serializable;

/**
 * 文案格式信息
 * 
 * @author zhancang
 */
public class ActivityStepContentVO implements Serializable {
  /**
   * serial version UID
   */
  private static final long serialVersionUID = -3471363791694106763L;

  /**
   * 首页每个模块的图片地址
   */
  private String content = "";

  /**
   * 楼层图片的跳转目标地址，包含跳转h5活动页面或者商品详情页面或者原生活动页面
   */
  private String targetId = "";

  /**
   * 楼层图片的跳转目标类型
   */
  private Short targetType;
  
  /**
   * 标题相关信息
   */
  private String titleInfo = "";

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getTargetId() {
    return targetId;
  }

  public void setTargetId(String targetId) {
    this.targetId = targetId;
  }

  public Short getTargetType() {
    return targetType;
  }

  public void setTargetType(Short targetType) {
    this.targetType = targetType;
  }

  public String getTitleInfo() {
    if(null == titleInfo){
      return "";
    }
    return titleInfo;
  }

  public void setTitleInfo(String titleInfo) {
    this.titleInfo = titleInfo;
  }
}
