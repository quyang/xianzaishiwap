package com.xianzaishi.wapcenter.client.mine.vo;

import java.io.Serializable;

import com.google.common.base.Preconditions;

/**
 * 用户注册信息字段
 * 
 * @author dongpo
 * 
 */
public class UserRegistVO implements Serializable {

  /**
   * serial version UID
   */
  private static final long serialVersionUID = -4804097567850560232L;

  /**
   * 用户id
   */
  private Long userId;

  /**
   * 注册手机号
   */
  private Long phone;

  /**
   * 验证码
   */
  private String verificationCode;

  /**
   * 密码
   */
  private String pwd;

  /**
   * 邀请码
   */
  private String inviteCode;

  /**
   * 注册类型，0表示线上注册，1表示线下注册（收银机），2标识用户邀请注册
   */
  private Short registType = RegistTypeContent.ONLINE_REGIST;

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Long getPhone() {
    return phone;
  }

  public void setPhone(Long phone) {
    Preconditions.checkNotNull(phone, "phone number is null");
    this.phone = phone;
  }

  public String getVerificationCode() {
    return verificationCode;
  }

  public void setVerificationCode(String verificationCode) {
    this.verificationCode = verificationCode;
  }

  public String getPwd() {
    return pwd;
  }

  public void setPwd(String pwd) {
    this.pwd = pwd;
  }

  public Short getRegistType() {
    return registType;
  }

  public void setRegistType(Short registType) {
    this.registType = registType;
  }

  public String getInviteCode() {
    return inviteCode;
  }

  public void setInviteCode(String inviteCode) {
    this.inviteCode = inviteCode;
  }



  public static class RegistTypeContent implements Serializable {

    /**
     * serial version uid
     */
    private static final long serialVersionUID = 2728321010984894473L;

    /**
     * 线上注册（app端注册）
     */
    public static final Short ONLINE_REGIST = 0;

    /**
     * 线下注册（收银机端注册）
     */
    public static final Short OFFLINE_REGIST = 1;

    /**
     * 用户邀请注册
     */
    public static final Short INVITE_REGIST = 2;

    public static Boolean isCorrectParams(Short param) {
      if (ONLINE_REGIST.shortValue() == param.shortValue()
          || OFFLINE_REGIST.shortValue() == param.shortValue()
          || INVITE_REGIST.shortValue() == param.shortValue()) {
        return true;
      }

      return false;
    }
  }

}
