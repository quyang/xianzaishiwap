package com.xianzaishi.wapcenter.client.mine.vo;

/**
 * 用户基本信息+用户邀请码
 * @author dongpo
 *
 */
public class BaseUserInfoVO {
  
  /**
   * 用户id
   */
  private Long userId;
  
  /**
   * 用户名
   */
  private String name;
  
  /**
   * 用户手机号
   */
  private Long phone;
  
  /**
   * 用户状态
   */
  private Short status;
  
  /**
   * 用户邀请码
   */
  private String inviteCode;

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getPhone() {
    return phone;
  }

  public void setPhone(Long phone) {
    this.phone = phone;
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

  public String getInviteCode() {
    return inviteCode;
  }

  public void setInviteCode(String inviteCode) {
    this.inviteCode = inviteCode;
  }
  
}
