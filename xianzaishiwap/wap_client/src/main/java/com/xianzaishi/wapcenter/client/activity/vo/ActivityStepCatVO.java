package com.xianzaishi.wapcenter.client.activity.vo;


/**
 * 楼层对象中的某个商品对象
 * 
 * @author zhancang
 */
public class ActivityStepCatVO{

  /**
   * 商品的图片
   */
  private String picUrl;

  /**
   * 类目id
   */
  private Long catId;

  /**
   * 类目id
   */
  private String catName;
  
  /**
   * 跳向活动页面
   */
  private String jumpLink;

  public String getPicUrl() {
    return picUrl;
  }

  public void setPicUrl(String picUrl) {
    this.picUrl = picUrl;
  }

  public Long getCatId() {
    return catId;
  }

  public void setCatId(Long catId) {
    this.catId = catId;
  }

  public String getCatName() {
    return catName;
  }

  public void setCatName(String catName) {
    this.catName = catName;
  }

  public String getJumpLink() {
    return jumpLink;
  }

  public void setJumpLink(String jumpLink) {
    this.jumpLink = jumpLink;
  }
  
}
