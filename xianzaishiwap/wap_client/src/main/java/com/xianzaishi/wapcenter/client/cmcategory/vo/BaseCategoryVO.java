package com.xianzaishi.wapcenter.client.cmcategory.vo;

public class BaseCategoryVO {
  
  /**
   * 类目id
   */
  private Integer catId;
  
  /**
   * 类目名
   */
  private String name;

  public Integer getCatId() {
    return catId;
  }

  public void setCatId(Integer catId) {
    this.catId = catId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
  
  

}
