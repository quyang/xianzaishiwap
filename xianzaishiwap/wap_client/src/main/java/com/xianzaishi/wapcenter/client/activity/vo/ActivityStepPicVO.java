package com.xianzaishi.wapcenter.client.activity.vo;

import java.io.Serializable;

public class ActivityStepPicVO implements Serializable {
  /**
   * serial version UID
   */
  private static final long serialVersionUID = -3471363791694106763L;

  /**
   * 首页每个模块的图片地址
   */
  private String picUrl = "";
  
  /**
   * 楼层图片的跳转目标地址
   */
  private String targetId = "";
  
  /**
   * 楼层图片的跳转目标类型
   */
  private Short targetType;

  /**
   * 标题相关信息
   */
  private String titleInfo = "";
  
  public String getPicUrl() {
    return picUrl;
  }

  public void setPicUrl(String picUrl) {
    this.picUrl = picUrl;
  }

  public String getTargetId() {
    return targetId;
  }

  public void setTargetId(String targetId) {
    this.targetId = targetId;
  }

  public Short getTargetType() {
    return targetType;
  }

  public void setTargetType(Short targetType) {
    this.targetType = targetType;
  }

  public String getTitleInfo() {
    if(null == titleInfo){
      return "";
    }
    return titleInfo;
  }

  public void setTitleInfo(String titleInfo) {
    this.titleInfo = titleInfo;
  }
  
}
