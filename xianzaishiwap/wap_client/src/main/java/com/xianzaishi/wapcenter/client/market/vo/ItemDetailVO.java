package com.xianzaishi.wapcenter.client.market.vo;

import java.io.Serializable;
import java.util.List;

import com.google.common.base.Preconditions;

/**
 * 商品详情页接口数据字段
 * @author dongpo
 *
 */
public class ItemDetailVO implements Serializable{
  
  /**
   * serial version UID
   */
  private static final long serialVersionUID = -9095080844672815815L;

  /**
   * 商品id
   */
  private Long itemId;

  /**
   * 图片地址
   */
  private List<String> picList;

  /**
   * 商品标题
   */
  private String title;

  /**
   * 商品副标题
   */
  private String subtitle;
  
  /**
   * sku id 列表
   */
  private List<ItemSkuVO> itemSkuVOs;

  /**
   * 商品介绍URL
   */
  private String introductionUrl;
  
  /**
   * 商品属性Url
   */
  private String propertyUrl;

  public Long getItemId() {
    return itemId == null ? 0 : itemId;
  }

  public void setItemId(Long itemId) {
    Preconditions.checkNotNull(itemId, "itemId is null");
    Preconditions.checkArgument(itemId > 0, "itemId must be greater than 0");
    this.itemId = itemId;
  }

  public List<String> getPicList() {
    return picList;
  }

  public void setPicList(List<String> picList) {
    this.picList = picList;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getSubtitle() {
    return subtitle;
  }

  public void setSubtitle(String subtitle) {
    this.subtitle = subtitle;
  }
  
  public List<ItemSkuVO> getItemSkuVOs() {
    return itemSkuVOs;
  }

  public void setItemSkuVOs(List<ItemSkuVO> itemSkuVOs) {
    this.itemSkuVOs = itemSkuVOs;
  }

  public String getIntroductionUrl() {
    return introductionUrl;
  }

  public void setIntroductionUrl(String introductionUrl) {
    this.introductionUrl = introductionUrl;
  }

  public String getPropertyUrl() {
    return propertyUrl;
  }

  public void setPropertyUrl(String propertyUrl) {
    this.propertyUrl = propertyUrl;
  }

  
  

}
