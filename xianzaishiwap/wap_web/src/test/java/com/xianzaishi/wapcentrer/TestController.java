package com.xianzaishi.wapcentrer;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xianzaishi.itemcenter.client.item.CommodityService;
import com.xianzaishi.itemcenter.client.item.dto.ItemDTO;
import com.xianzaishi.itemcenter.client.item.query.ItemListQuery;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.usercenter.client.user.UserService;
import com.xianzaishi.usercenter.client.user.dto.BaseUserDTO;

/**
 * 商品list接口
 * @author dongpo
 *
 */
@Controller
@RequestMapping("/testzhancang")
public class TestController {
  
  @Autowired
  private CommodityService commodityService;
  
  @Autowired
  private UserService userService;
  
  private static final Logger LOGGER = Logger.getLogger(TestController.class);
  
  /**
   * 查询商品list数据
   * @param itemListQuery 
   * @return
   */
  @RequestMapping(value = "/testlist",produces="text/html;charset=UTF-8")
  @ResponseBody
  public String queryItemList() {
    ItemListQuery it  =new ItemListQuery();
    it.setPageNum(0);
    it.setPageSize(100);
    Result<Map<String, Object>> result = commodityService.queryCommodies(it);
    List<ItemDTO> tmp =  (List<ItemDTO>) result.getModule().get("itemList");
    for(ItemDTO item:tmp){
      LOGGER.error(item.getItemSkuDtos().get(0).queryOriginpace());
    }
    LOGGER.error(result.getSuccess());
    return JackSonUtil.getJson(Result.getSuccDataResult(JackSonUtil.getJson(tmp)));
  }
  
  /**
   * 查询商品list数据
   * @param itemListQuery 
   * @return
   */
  @RequestMapping(value = "/testUser",produces="text/html;charset=UTF-8")
  @ResponseBody
  public String testUser() {
    Result<? extends BaseUserDTO> result = userService.queryUserByPhone(13738860434L, false);
    LOGGER.error(result.getSuccess());
    LOGGER.error(result.getModule().getName());
    return JackSonUtil.getJson(result.getModule());
  }
}
