package com.xianzaishi.wapcentrer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.util.Properties;

public class Test {

  private static final long itemid = 10201602L;
  private static final long skuid = 2L;
  
  private static final String MYSQL_ADDRESS = "C:\\Program Files (x86)\\MySQL\\MySQL Workbench CE 6.0.8\\";

  public static void main(String[] args) throws IOException {
    // testCommodityItemQuery();
    // System.out.println("test end");
    //
    // UserDTO userDTO = new UserDTO();
    // Long long1 = new Long(10000L);
    // userDTO.setPhone(long1);
    // long1 = 0000L;
    // System.out.println(userDTO.getPhone());
    String root = "root";
    String rootPass = "123456";
    String dbName = "erp";
    String backupsPath = "E:\\123\\";
    String backupsSqlFileName = "erpDB.sql";
    // dbBackUp(root, rootPass, backupsPath, backupsSqlFileName);
//     backup(backupsPath+backupsSqlFileName);
    load(backupsPath + backupsSqlFileName,"erp");
    

  }

  public static String dbBackUp(String root,String rootPass,String dbName,String backupsPath,String backupsSqlFileName)  
  {  
      //生成临时备份文件  
//    SimpleDateFormat sd=new SimpleDateFordckupsSqlFileName;  
      String pathSql = backupsPath+backupsSqlFileName;  
      try {  
          File fileSql = new File(pathSql);  
          if(!fileSql.exists()){  
              fileSql.createNewFile();  
          }  
          StringBuffer sbs = new StringBuffer();  
          sbs.append("mysqldump ");  
          sbs.append(" -h 127.0.0.1 ");  
          sbs.append(" -u ");  
          sbs.append(root+" ");  
          sbs.append("-p"+rootPass+" ");  
          sbs.append(dbName);  
          sbs.append(" --default-character-set=utf8 ");  
//        sbs.append(">"+pathSql);  
          sbs.append(" --result-file="+pathSql);  
          System.out.println("cmd命令为：——>>>"+sbs.toString());  
          Runtime runtime = Runtime.getRuntime();  
          Process child = runtime.exec(sbs.toString());  
            
          //读取备份数据并生成临时文件  
          InputStream in = child.getInputStream();  
          OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(pathSql), "utf8");  
          BufferedReader reader = new BufferedReader(new InputStreamReader(in, "utf8"));  
          String line=reader.readLine();  
          while (line != null) {  
              writer.write(line+"\n");  
              line=reader.readLine();  
           }  
           writer.flush();  
           System.out.println("数据库已备份到——>>"+pathSql);  
      } catch (Exception e) {  
            
      }  
      return pathSql;  
  }  
  
  public static boolean load(String path, String databaseName) throws IOException {// 还原
    Runtime runtime = Runtime.getRuntime();
    // 因为在命令窗口进行mysql数据库的导入一般分三步走，所以所执行的命令将以字符串数组的形式出现
    String cmdarray[] = getImportCommand(path, databaseName);// 根据属性文件的配置获取数据库导入所需的命令，组成一个数组
    // runtime.exec(cmdarray);//这里也是简单的直接抛出异常
    Process process = runtime.exec(cmdarray[0]);
    // 执行了第一条命令以后已经登录到mysql了，所以之后就是利用mysql的命令窗口
    // 进程执行后面的代码

    OutputStream os = process.getOutputStream();
    OutputStreamWriter writer = new OutputStreamWriter(os,"utf8");
    // 命令1和命令2要放在一起执行
    writer.write(cmdarray[1] + "\r\n" + cmdarray[2]);
    writer.flush();
    writer.close();
    os.close();
    System.out.println("load数据成功");
    return true;
  }

  private static String[] getImportCommand(String path, String databaseName) {
    String username = "root";// 用户名
    String password = "123456";// 密码
    String host = "localhost";// 导入的目标数据库所在的主机
    String importDatabaseName = databaseName;// 导入的目标数据库的名称
    String importPath = path;// 导入的目标文件所在的位置
    // 第一步，获取登录命令语句
    String loginCommand =
        new StringBuffer().append(MYSQL_ADDRESS).append("mysql ").append(" -h").append(host)
            .append(" -u").append(username).append(" -p").append(password)
            .append(" --default-character-set=utf8 ").toString();
    // 第二步，获取切换数据库到目标数据库的命令语句
    String switchCommand = new StringBuffer("use ").append(importDatabaseName).toString();
    // 第三步，获取导入的命令语句
    String importCommand = new StringBuffer("source ").append(importPath).toString();
    // 需要返回的命令语句数组
    String[] commands = new String[] {loginCommand, switchCommand, importCommand};
    return commands;
  }

}
