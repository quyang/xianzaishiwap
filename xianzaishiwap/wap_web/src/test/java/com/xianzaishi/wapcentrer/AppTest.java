package com.xianzaishi.wapcentrer;

import java.net.MalformedURLException;
import java.util.UUID;

import com.xianzaishi.itemcenter.common.JackSonUtil;

public class AppTest {
  
  private static final String MESSAGEURL = "http://139.196.94.125/hessian/messageService";
  
  public static String[] inviteChars = new String[] { "a", "b", "c", "d", "e", "f",
    "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s",
    "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5",
    "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I",
    "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
    "W", "X", "Y", "Z" };
  

  public static void main(String[] args) throws MalformedURLException {
    
    /*
     * <servlet> <!-- 配置 HessianServlet，Servlet的名字随便配置，例如这里配置成ServiceServlet-->
     * <servlet-name>ServiceServlet</servlet-name>
     * <servlet-class>com.caucho.hessian.server.HessianServlet</servlet-class>
     * 
     * <!-- 配置接口的具体实现类 --> <init-param> <param-name>service-class</param-name>
     * <param-value>gacl.hessian.service.impl.ServiceImpl</param-value> </init-param> </servlet>
     * <!-- 映射 HessianServlet的访问URL地址--> <servlet-mapping>
     * <servlet-name>ServiceServlet</servlet-name> <url-pattern>/ServiceServlet</url-pattern>
     * </servlet-mapping>
     */
    // 在服务器端的web.xml文件中配置的HessianServlet映射的访问URL地址
//    String url = "http://localhost/wapcenter/hessian/sayHello";
//    HessianProxyFactory factory = new HessianProxyFactory();
//    SayHello service = (SayHello) factory.create(SayHello.class, url);// 创建IService接口的实例对象
//    String i = service.sayHello("111111");
//    String activeCodeString = "xu;2016-8-17 14:09:22";
//    UserCenter userCenter = new UserCenter();
//    userCenter.a();
//    UserCenter userCenter2 = new UserCenter();
//    userCenter2.b();
    
//    UserCenter userCenter = new UserCenter();
//    UserRegistVO registVO = new UserRegistVO();
//    registVO.setPwd("1234567");
//    registVO.setPhone( 18680305605L);
//    registVO.setVerificationCode("123456");
//    String result = userCenter.regist(registVO);
//    userCenter.sendCheckCode(18680305605L, 1);
//    userCenter.logout("fd9ca179828c10a24606f0669fc89cc7");
//    System.out.println(userCenter.logout("3cb95cfbe1035bce8c448fcaf80fe7d8"));
    
//    ItemList itemList = new ItemList();
//    itemList.textItemInteface();
    
//    List<ItemCommoditySkuDTO> itemCommoditySkuDTOs = Lists.newArrayList();
//    ItemCommoditySkuDTO itemCommoditySkuDTO = new ItemCommoditySkuDTO();
//    itemCommoditySkuDTO.setSkuId(99999L);
//    itemCommoditySkuDTO.setCheckedFailedReason("太帅了");
//    itemCommoditySkuDTO.setCost(2222);
//    itemCommoditySkuDTO.setDiscountPrice(333);
//    itemCommoditySkuDTO.setDiscountPriceYuanString("9.99");
//    itemCommoditySkuDTO.setInventory(200);
//    itemCommoditySkuDTO.setItemId(10006L);
//    itemCommoditySkuDTO.setPrice(999);
//    itemCommoditySkuDTO.setPriceYuanString("11.1");
//    itemCommoditySkuDTO.setPromotionInfo("第二件半价");
//    itemCommoditySkuDTO.setQuantity(30);
//    itemCommoditySkuDTO.setSaleStatus((short) 1);
//    itemCommoditySkuDTO.setSkuCode(10001L);
//    itemCommoditySkuDTO.setSkuUnit(3);
//    itemCommoditySkuDTO.setTitle("11222");
//    itemCommoditySkuDTO.setStatus((short) 2);
//    itemCommoditySkuDTOs.add(itemCommoditySkuDTO);
//    List<ItemSkuVO> itemSkuVOs = setItemSkuVO(itemCommoditySkuDTOs);
//    if(null != itemSkuVOs){
//        System.out.println(toJsonData(itemSkuVOs));
//    }
//    MessageService messageService = creatMessageService();
//    messageService.send("18680305603", "【鲜在时】验证码888888，有效期10分钟，感谢您的注册。请不要把验证码告诉其他人，谨防诈骗");
    for(int i = 1;i < 100000; i ++){
      System.out.println(createInviteCode((long) i));
      System.out.println("\n");
    }
    
  }
  
  public static String createInviteCode(Long userId){
    StringBuffer shortBuffer = new StringBuffer();
    String uuid = UUID.randomUUID().toString().replace("-", "");
    String userIdCode = Long.toString(userId, 16);
    if(userIdCode.length() < 8){
      for (int i = 0; i < 8 - userIdCode.length(); i++) {
        String str = uuid.substring(i * 4, i * 4 + 4);
        int x = Integer.parseInt(str, 16);
        shortBuffer.append(inviteChars[x % 0x3E]);
      }
      shortBuffer.append(userIdCode);
    }else{
      for (int i = 0; i < 8; i++) {
        String str = uuid.substring(i * 4, i * 4 + 4);
        int x = Integer.parseInt(str, 16);
        shortBuffer.append(inviteChars[x % 0x3E]);
      }
    }
    
    return shortBuffer.toString();
  }
  
//  /**
//   * 创建服务接口
//   * 
//   * @return
//   */
//  private static MessageService creatMessageService() {
//    HessianProxyFactory factory = new HessianProxyFactory();// hessian服务
//    factory.setOverloadEnabled(true);
//    MessageService service = null;
//    try {
//      service = (MessageService) factory.create(MessageService.class, MESSAGEURL);// 创建IService接口的实例对象
//    } catch (MalformedURLException e) {
//      // TODO Auto-generated catch block
//      e.printStackTrace();
//    }
//    return service;
//  }

//  
//  private static List<ItemSkuVO> setItemSkuVO(List<ItemCommoditySkuDTO> itemCommoditySkuDTOs) {
//    List<ItemSkuVO> itemSkuVOs = new ArrayList<ItemSkuVO>();
//      if (itemCommoditySkuDTOs == null) {
//        return null;
//      }
//      for(int i = 0;i < itemCommoditySkuDTOs.size();i++){
//          ItemSkuVO itemSkuVO = new ItemSkuVO();
//          BeanCopierUtils.copyProperties(itemCommoditySkuDTOs.get(i), itemSkuVO);// 拷贝sku的后台skuID 商品id 库存 sku单位
//          itemSkuVOs.add(itemSkuVO);
//      }
                                                                         // sku标题 销量 促销信息到前台VO中
//      for (int i = 0; i < itemCommoditySkuDTOs.size(); i++) {
//        itemSkuVOs.get(i).setDiscountPriceYuanString(
//            itemCommoditySkuDTOs.get(i).queryDiscountPriceYuanString());// 设置sku折扣价
//        itemSkuVOs.get(i).setPriceYuanString(itemCommoditySkuDTOs.get(i).queryPriceYuanString());// 设置sku原价
//        itemSkuVOs.get(i).setSaleDetailInfo(itemCommoditySkuDTOs.get(i).querySaleDetailInfo());// 设置sku销售信息
//      }
        
//      return itemSkuVOs;
//    }
  
  /**
   * 将传进来的数据转换为json数据
   * 
   * @param result
   * @return
   */
  public static String toJsonData(Object result) {
    String jsonResult = JackSonUtil.getJson(result);
    return jsonResult;
  }
}