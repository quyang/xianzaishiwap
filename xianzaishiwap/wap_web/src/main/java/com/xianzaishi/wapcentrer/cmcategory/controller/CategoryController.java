package com.xianzaishi.wapcentrer.cmcategory.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.xianzaishi.itemcenter.client.item.dto.ItemBaseDTO.ItemStatusConstants;
import com.xianzaishi.itemcenter.client.item.dto.ItemBaseDTO.SaleStatusConstants;
import com.xianzaishi.itemcenter.client.item.query.ItemListQuery;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.query.BaseQuery;
import com.xianzaishi.itemcenter.common.result.PagedResult;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.wapcenter.client.market.vo.query.ItemQueryVO;
import com.xianzaishi.wapcenter.component.cmcategory.CmCategoryComponent;

@Controller
@RequestMapping(value = "/requestcategory")
public class CategoryController {
  private static final Logger LOGGER = Logger.getLogger(CategoryController.class);

  @Autowired
  private CmCategoryComponent cmCategoryComponent;

  /**
   * 请求一级类目
   * 
   * @return
   */
  @RequestMapping(value = "/category", produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String queryCategory(Boolean homePageCategory) {
    if (null == homePageCategory) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }

    String result = cmCategoryComponent.queryDefaultCategoryList(homePageCategory);

    return result;
  }

  /**
   * 请求叶子类目
   * 
   * @param parentId 父类目id
   * @return
   */
  @RequestMapping(value = "/leafcategory", produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String queryLeafCategory(Integer parentId, Integer pageSize, Integer pageNum) {
    if (null == parentId) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }

    return JackSonUtil.getJson(cmCategoryComponent.queryLeafCategory(parentId, pageSize, pageNum));
  }

  /**
   * 请求一级类目新接口
   * 
   * @return
   */
  @RequestMapping(value = "/queryrootcategory", produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String queryRootCategory() {
    Result<Map<String, Object>> result = cmCategoryComponent.queryRootCategory();
    
    return JackSonUtil.getJson(result);
  }
  
  /**
   * 一级类目请求子类目新接口
   * 
   * @return
   */
  @RequestMapping(value = "/querysubcategory", produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String querySubCategory(Integer catId) {
    if (null == catId) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }

    Result<Map<String, Object>> result = cmCategoryComponent.querySubCategory(catId);

    return JackSonUtil.getJson(result);
  }
  
  /**
   * 请求叶子类目商品数据
   * 
   * @param data
   * @param response
   * @return
   */
  @RequestMapping(value = "/commodies", produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String queryCommodiesData(@RequestBody String data, HttpServletResponse response) {
    ItemQueryVO itemQueryVO = (ItemQueryVO) JackSonUtil.jsonToObject(data, ItemQueryVO.class);
    if (null == itemQueryVO) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    LOGGER.info(data);
    response.setContentType("text/html;charset=UTF-8");
    ItemListQuery itemListQuery = new ItemListQuery();
    if (null != itemQueryVO.getPageSize()) {
      itemListQuery.setPageSize(itemQueryVO.getPageSize());
    }
    if (null != itemQueryVO.getPageNum()) {
      itemListQuery.setPageNum(itemQueryVO.getPageNum());
    }
    if (null != itemQueryVO.getCmCat2Ids()) {
      itemListQuery.setCmCat2Ids(Arrays.asList(itemQueryVO.getCmCat2Ids()));
    }
    if(null != itemQueryVO.getCatId()){
      itemListQuery.setCategoryIds(Arrays.asList(itemQueryVO.getCatId()));
    }
    List<Short> saleStatus = Lists.newArrayList();
    saleStatus.add(SaleStatusConstants.SALE_ONLY_ONLINE);
    saleStatus.add(SaleStatusConstants.SALE_ONLINE_AND_MARKET);
    itemListQuery.setSaleStatusList(saleStatus);
    itemListQuery.setStatus(ItemStatusConstants.ITEM_STATUS_PASS_AND_UP_SHELVES);// 上架商品

    return JackSonUtil.getJson(cmCategoryComponent.queryCommodiesData(itemListQuery));
  }

  /**
   * 请求外卖叶子类目商品数据
   * 
   * @param data
   * @param response
   * @return
   */
  @RequestMapping(value = "/takeoutleafcategory", produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String queryTakeOutLeafCategory(Integer pageSize, Integer pageNum) {
    Integer parentId = 10002;
    if (null == pageSize) {
      pageSize = 100;
    }
    if (null == pageNum) {
      pageNum = BaseQuery.DEFAULT_PAGE_NUM;
    }
    return JackSonUtil.getJson(cmCategoryComponent.queryTakeOutLeafCategory(parentId, pageSize,
        pageNum));
  }

  /**
   * 请求叶子类目商品数据
   * 
   * @param data
   * @param response
   * @return
   */
  @RequestMapping(value = "/takeoutcommodies", produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String queryTakeOutCommodiesData(String cmCat2Ids, Integer pageSize, Integer pageNum) {
    if (StringUtils.isEmpty(cmCat2Ids)) {
      return JackSonUtil.getJson(PagedResult.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    if (null == pageSize) {
      pageSize = BaseQuery.DEFAULT_PAGE_SIZE;
    }
    if (null == pageNum) {
      pageNum = BaseQuery.DEFAULT_PAGE_NUM;
    }
    ItemListQuery itemListQuery = new ItemListQuery();
    itemListQuery.setPageSize(pageSize);
    itemListQuery.setPageNum(pageNum);
    List<Integer> catIds = Lists.newArrayList();
    for(String cmCat2Id : cmCat2Ids.split(",")){
      catIds.add(Integer.valueOf(cmCat2Id));
    }
    itemListQuery.setCmCat2Ids(catIds);
    List<Short> saleStatus = Lists.newArrayList();
    saleStatus.add(SaleStatusConstants.SALE_ONLY_ONLINE);
    saleStatus.add(SaleStatusConstants.SALE_ONLINE_AND_MARKET);
    itemListQuery.setSaleStatusList(saleStatus);
    itemListQuery.setStatus(ItemStatusConstants.ITEM_STATUS_PASS_AND_UP_SHELVES);// 上架商品
    
    return JackSonUtil.getJson(cmCategoryComponent.queryTakeOutCommodiesData(itemListQuery));
  }

}
