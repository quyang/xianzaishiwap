package com.xianzaishi.wapcentrer.itemdetail.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.wapcenter.component.itemdetail.ItemDetailComponent;

/**
 * 详细页接口
 * 
 * @author dongpo
 * 
 */
@Controller
@RequestMapping("/requestcommodity")
public class ItemDetailController {
  private static final Logger LOGGER = Logger.getLogger(ItemDetailController.class);

  @Autowired
  private ItemDetailComponent itemDetailComponent;

  /**
   * 详细页基本数据（第一屏数据）
   * 
   * @param itemId
   * @return
   */
  @RequestMapping(value = "/itemdetail", produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String queryItemDetail(Long itemId) {
    if (null == itemId) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    LOGGER.info("请求详情页面的商品号：" + itemId);
    String result = itemDetailComponent.queryItemDetail(itemId);
    return result;
  }

}
