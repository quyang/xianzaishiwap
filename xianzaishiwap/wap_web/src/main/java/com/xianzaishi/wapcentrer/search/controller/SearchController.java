package com.xianzaishi.wapcentrer.search.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.filter.StringUtils;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.wapcenter.component.search.SearchComponent;

@Controller
@RequestMapping(value = "/search")
public class SearchController {
  private static final Logger LOGGER = Logger.getLogger(SearchController.class);
  @Autowired
  private SearchComponent searchComponent;
  
  @RequestMapping(value = "/queryitems",produces="text/html;charset=UTF-8")
  @ResponseBody
  public String queryItem(String itemKeyWord,Integer pageSize,Integer pageNum, HttpServletRequest request){
    if(null == itemKeyWord){
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,"参数为空"));
    }
    if(StringUtils.sqlValidate(itemKeyWord)){
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "输入的参数不合法"));
    }
    itemKeyWord = StringUtils.doFilter(itemKeyWord);
    if(itemKeyWord == ""){
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,"除去特殊字符后参数为空"));
    }
    LOGGER.info("查询商品关键字:" + itemKeyWord + ",pageSize:" + pageSize + ",pageNum:" + pageNum);
    String result = searchComponent.queryItemByKeyWord(itemKeyWord,pageSize,pageNum);
    
    return result;
  }
  
  @RequestMapping(value = "/hotsearchword",produces="text/html;charset=UTF-8")
  @ResponseBody
  public String hotSearchWord(){
    Integer pageId = 2;
    Integer stepId = 22;
    String result = searchComponent.queryHotWord(pageId,stepId);
    return result;
  }

}
