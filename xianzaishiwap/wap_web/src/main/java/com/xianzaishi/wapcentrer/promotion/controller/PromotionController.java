package com.xianzaishi.wapcentrer.promotion.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xianzaishi.couponcenter.client.discount.DiscountInfoService;
import com.xianzaishi.couponcenter.client.usercoupon.dto.UserCouponDiscontResultDTO;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.wapcenter.client.mine.vo.UserAllPromotionVO;
import com.xianzaishi.wapcenter.client.promotion.DiscountInfoVO;
import com.xianzaishi.wapcenter.component.promotion.PromotionComponet;

@Controller
@RequestMapping(value = "/promotion")
public class PromotionController {

  private static final Logger LOGGER = Logger.getLogger(PromotionController.class);

  @Autowired
  private PromotionComponet promotionComponet;
  
  @Autowired
  private DiscountInfoService discountInfoService;

  /**
   * 添加优惠券
   * 
   * @param token
   * @param type
   * @return
   */
  @RequestMapping(value = "/addDiscountCoupon", produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String addCoupon90Discount40(@RequestParam String token, @RequestParam int type,
      @RequestParam(value = "couponId", defaultValue = "0", required = false) int couponId, @RequestParam(
          value = "groupId", defaultValue = "0" , required = false) int groupId) {
    if (type == 1) {
      return JackSonUtil.getJson(promotionComponet.addUserCoupon(token, type, couponId));
    } else if (type == 2) {
      return JackSonUtil.getJson(promotionComponet.addUserCouponPackage(token, type, groupId));
    }
    return JackSonUtil.getJson(Result
        .getErrDataResult(ServerResultCode.ERROR_CODE_UNSUPPORTEDQUERY,
            "Query coupon type not support,input:" + type));
  }

  /**
   * 提示用户当前可享受到的优惠券的信息，只提示一个最大享受的优惠券,用在购物车顶部提示
   * 
   * @param token
   * @param payType
   * @param buySkuDetailJson
   * @return
   */
  @RequestMapping(value = "/selectMathCoupon", produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String selectMathCoupon(@RequestParam String token, @RequestParam int payType,
      @RequestParam String buySkuDetailJson) {
    Result<UserCouponDiscontResultDTO> result = promotionComponet.selectMathCoupon(token, payType, buySkuDetailJson);
    if(null == result || !result.getSuccess()){
      UserCouponDiscontResultDTO result1 = new UserCouponDiscontResultDTO();
      return JackSonUtil.getJson(Result.getResult(result1, true, 0, ""));
    }
    return JackSonUtil
        .getJson(result);
  }

  /**
   * 取用户可用优惠券，包含用户所有可用优惠券，和当前购买情况下可用优惠券，app端调用，个人中心和结算页面中心调用
   * 
   * @param token
   * @param queryType=0取用户所有优惠券，queryType=1取当前购物情况下可用优惠券
   * @param payType
   * @param buySkuDetailJson
   * @return
   */
  @RequestMapping(value = "/selectUserCouponList", produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String selectCurrentMathCouponList(@RequestParam String token,
      @RequestParam int queryType, @RequestParam(value = "payType", defaultValue = "1",
          required = false) int payType, @RequestParam(value = "buySkuDetailJson",
          defaultValue = "", required = false) String buySkuDetailJson) {
    return JackSonUtil.getJson(promotionComponet.selectUserCouponByPhone(token, queryType, payType,
        buySkuDetailJson));
  }

  /**
   * 计算根据当前优惠券，是否会抵扣并且真实折扣信息是多少，现在只有线下在调用，并且只使用了返回的金额字段
   * 
   * @param  json化后的整个参数对象
   * @return
   */
  @RequestMapping(value = "/calculateDiscountPriceWithCoupon", produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String calculateDiscountPriceWithCoupon(@RequestParam String buyDetailJson) {
    return JackSonUtil.getJson(promotionComponet.calculateDiscountPriceWithCoupon(buyDetailJson));
  }

  /**
   * 根据用户id查取该用户所有的优惠券信息
   * 
   * @param  用户id
   * @return 
   */
  @RequestMapping(value = "/queryAllPromotionInfo", produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String queryUserPromotionInfo(@RequestParam String userId) {
	  UserAllPromotionVO user = new UserAllPromotionVO();
	  user.setUserId(Long.parseLong(userId.trim()));
    return JackSonUtil.getJson(promotionComponet.queryPromotionInfo(user));
  }

  
  
  /**
   * 取用户可用优惠券，包含用户所有可用优惠券，和当前购买情况下可用优惠券.收银台调用。线下不用查询用户所有优惠券，线下没有个人中心的需求
   * 
   * @param
   * @param
   * @param
   * @param
   * @return
   */
  @RequestMapping(value = "/selectUserCouponListByShop", produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String selectUserCouponListByShop(@RequestParam String buyDetailJson) {
    return JackSonUtil.getJson(promotionComponet.selectUserCouponByShop(buyDetailJson));
  }

  /**
   * 根据购买情况取优惠信息和优惠券
   * 
   * @param
   * @param
   * @param
   * @param
   * @return
   */
  @RequestMapping(value = "/getUserDiscount", produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String getUserDiscount(@RequestParam String buyDetailJson) {
    return JackSonUtil.getJson(promotionComponet.getUserDiscount(buyDetailJson));
  }

//  /**
//   *
//   * @param buyDetailJson
//   * @return
//     */
//  @RequestMapping(value = "/selectDiscountInfoOnOffline", produces = "text/html;charset=UTF-8")
//  @ResponseBody
//  public  String selectDiscountInfoOnOffline(@RequestParam String buyDetailJson){
//      Map<String,Object> reslut = new HashMap<>();
//      Result<List<UserCouponDTO>>  userCouponListResult =  promotionComponet.selectUserCouponByShop(buyDetailJson);
////    if(!userCouponListResult.getSuccess()){
////
////    }
//      List<UserCouponDTO> userCouponDTOs = userCouponListResult.getModule();
//      reslut.put("coupons",userCouponDTOs);
//    List<Map<String,Object>> discountInfoVOs = new ArrayList<>();
//    Map<String,Object> discMap=new HashMap<>();
//    discMap.put("skuId",10004);
//    discMap.put("itemId",10201604);
//    discMap.put("number",5);
//    DiscountInfoVO discountInfoVO = new DiscountInfoVO();
//    discountInfoVO.setDiscountDesc("第二件5折,已为你立减15元");
//    discMap.put("discountInfo",discountInfoVO);
//    discountInfoVOs.add(discMap);
//
//      reslut.put("objects",discountInfoVOs);
//
//    DiscountInfoVO discountInfoVOSum = new DiscountInfoVO();
//    discountInfoVOSum.setDiscountDesc("满99元立减30元专区，已为你立减30元");
//    discountInfoVOSum.setOriginPrice(100.00);
//    discountInfoVOSum.setDiscountPrice(10.00);
//    discountInfoVOSum.setCurrPrice(99.00);
//      reslut.put("discountInfo",discountInfoVOSum);
//    return JackSonUtil.getJson( Result.getSuccDataResult(reslut));
//  }
}
