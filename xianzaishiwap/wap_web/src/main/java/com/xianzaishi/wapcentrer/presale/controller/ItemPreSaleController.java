package com.xianzaishi.wapcentrer.presale.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xianzaishi.itemcenter.client.presale.query.ItemPreSaleQuery;
import com.xianzaishi.wapcenter.component.presale.ItemPreSaleComponent;

/**
 * app访问入口，一般为json格式的数据
 *
 * @author jianmo
 * @create 2016-12-23 上午 10:52
 **/
@Controller
@RequestMapping("/itemPreSales")
public class ItemPreSaleController {

    private static final Logger LOGGER = Logger.getLogger(ItemPreSaleController.class);

    @Autowired
    private ItemPreSaleComponent itemPreSaleComponent;


    /**
     * 获取预售商品的列表信息
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/queryItemPreSales")
    public String queryItemPreSales(ItemPreSaleQuery query){
        LOGGER.info("查询参数 ==》》 【"+query.toString()+"】");
        //解析数据
        return itemPreSaleComponent.queryItemPreSalesData(query);
    }


}
