package com.xianzaishi.wapcentrer.market.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HTTP;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 详细页接口
 * 
 * @author dongpo
 * 
 */
@Controller
@RequestMapping("/requestweixin")
public class WeixinController {

  private static final Logger LOGGER = Logger.getLogger(WeixinController.class);
  
  /**
   * 详细页基本数据（第一屏数据）
   * 
   * @param itemId
   * @return
   * @throws IOException 
   * @throws ClientProtocolException 
   */
  @RequestMapping(value = "/weixinkey", produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String queryItemDetail(String code,String appid,String secret) throws Exception {
    
    String queryUrl = "https://api.weixin.qq.com/sns/oauth2/access_token?appid="+appid+"&secret="+secret+"&code="+code+"&grant_type=authorization_code";
    LOGGER.error("Query url:"+queryUrl);
    HttpGet get = new HttpGet(queryUrl);
    CloseableHttpClient httpclient = HttpClients.createDefault();
    CloseableHttpResponse response1 = httpclient.execute(get);
    System.out.println(response1.getStatusLine());
    HttpEntity entity1 = response1.getEntity();
    InputStream is = entity1.getContent();
    BufferedReader in = new BufferedReader(new InputStreamReader(is, HTTP.UTF_8));
    StringBuffer buffer = new StringBuffer();
    String line = "";
    while ((line = in.readLine()) != null) {
      buffer.append(line);
    }
    // end 读取整个页面内容
    LOGGER.error("Result entity:"+entity1);
    System.out.println(buffer.toString());
    String result = buffer.toString();

    response1.close();
    httpclient.close();

    return result;
  }

}
