package com.xianzaishi.wapcentrer.itemdetail.controller;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Maps;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;

@Controller
@RequestMapping("/itemtag")
public class ItemTagController {

  private static final Logger LOGGER = Logger.getLogger(ItemTagController.class);
  
  @RequestMapping(value = "/gettaginfo", produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String getTagInfo(@RequestParam String tagInfo) {
    if(StringUtils.isEmpty(tagInfo)){
      return JackSonUtil.getJson(Result.getErrDataResult(-1, "请求参数错误"));
    }
    Map<String, String> tagResult = Maps.newHashMap();
    int tagNum = 0;
    try{
      tagNum = Integer.parseInt(tagInfo, 2);
    }catch(Exception e){
      e.printStackTrace();
      LOGGER.error("Input item tag is:"+tagInfo);
      return JackSonUtil.getJson(Result.getErrDataResult(-1, "请求参数错误"));
    }
    if (tagNum == 1) {
      tagResult.put("online", "true");
      tagResult.put("info", "满99减40");
    } else if (tagNum == 32) {
      tagResult.put("online", "true");
      tagResult.put("info", "满40减20");
    } else {
      tagResult.put("online", "false");
      tagResult.put("info", "线下标");
    }
    return JackSonUtil.getJson(Result.getSuccDataResult(tagResult));
  }
}
