package com.xianzaishi.wapcentrer.itemlist.controller;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.xianzaishi.itemcenter.client.item.dto.ItemBaseDTO.ItemStatusConstants;
import com.xianzaishi.itemcenter.client.item.dto.ItemBaseDTO.SaleStatusConstants;
import com.xianzaishi.itemcenter.client.item.query.ItemListQuery;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.wapcenter.client.market.vo.ItemTagDetailVO;
import com.xianzaishi.wapcenter.client.market.vo.query.ItemQueryVO;
import com.xianzaishi.wapcenter.component.itemlist.ItemListComponent;

/**
 * 商品list接口
 * 
 * @author dongpo
 * 
 */
@Controller
@RequestMapping("/requestcommodies")
public class ItemListController {

  private static final Logger LOGGER = Logger.getLogger(ItemListController.class);

  @Autowired
  private ItemListComponent itemListComponent;

  /**
   * 查询商品list数据
   * 
   * @param itemListQuery
   * @return
   */
  @RequestMapping(value = "/itemlist", produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String queryItemList(ItemQueryVO itemQueryVO) {
    if (null == itemQueryVO) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "itemQueryVO is null"));
    }
    LOGGER.info(JackSonUtil.getJson(itemQueryVO));
    ItemListQuery itemListQuery = new ItemListQuery();
    itemListQuery.setPageNum(0);
    itemListQuery.setPageSize(200);
    List<Short> saleStatus = Lists.newArrayList();
    saleStatus.add(SaleStatusConstants.SALE_ONLY_ONLINE);
    saleStatus.add(SaleStatusConstants.SALE_ONLINE_AND_MARKET);
    itemListQuery.setSaleStatusList(saleStatus);
    itemListQuery.setStatus(ItemStatusConstants.ITEM_STATUS_PASS_AND_UP_SHELVES);
    String result = itemListComponent.queryCommodiesData(itemListQuery);
    return result;

  }

  @RequestMapping(value = "/queryitemtagsdetail", produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String queryItemTagsDetail(String[] skuIds,Short channel) {
    if (null == skuIds || skuIds.length == 0) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    if(null == channel){
      channel = 1;
    }
    List<Long> skus = Lists.newArrayList();
    for(String skuId : skuIds){
      if(StringUtils.isNumeric(skuId)){
        skus.add(Long.valueOf(skuId));
      }
    }
    Result<List<ItemTagDetailVO>> result = itemListComponent.queryItemTagsDetail(skus,channel);

    return JackSonUtil.getJson(result);
  }
}
