package com.xianzaishi.wapcentrer.mine.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Maps;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.wapcenter.component.mine.OldWithNewComponent;

@Controller
@RequestMapping(value = "/oldwithnew")
public class OldWithNewController {

  @Autowired
  private OldWithNewComponent oldWithNewComponent;

  @RequestMapping(value = "/getinviterinfo", produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String getInviterInfo(String token) {
    Map<String, Object> map = Maps.newHashMap();
    map.put("status", 1);
    map.put("inviteCode", "XYV18ZWT");
    return JackSonUtil.getJson(Result.getSuccDataResult(map));
  }

  @RequestMapping(value = "/getachievementlist", produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String getAchievementList(String token) {
    // Map<String,Object> map11 = Maps.newHashMap();
    // map11.put("type", 1);//1表示邀请，2表示其他
    // map11.put("value", "您已邀请8人，获得100元礼券");
    // map11.put("redFont", "9-4");
    if(null == token){
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    Result<Map<String, Object>> result = oldWithNewComponent.getAchievementList(token);
    return JackSonUtil.getJson(result);
  }

  @RequestMapping(value = "/invitecodeexchange", produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String inviteCodeExchange(String token, String inviteCode) {
    if (null == token || null == inviteCode) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    Result<Boolean> result = oldWithNewComponent.inviteCodeExchange(token, inviteCode);
    return JackSonUtil.getJson(result);
  }

}
