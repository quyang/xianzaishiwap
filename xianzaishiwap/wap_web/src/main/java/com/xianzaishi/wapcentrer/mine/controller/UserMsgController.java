package com.xianzaishi.wapcentrer.mine.controller;

import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.wapcenter.client.mine.vo.UserProfileQueryVO;
import com.xianzaishi.wapcenter.component.mine.UserMsgConmponent;
import java.io.IOException;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 用户消息推送相关
 * @author zhancang
 */
@Controller
@RequestMapping("/usermsg")
public class UserMsgController {

  private static final Logger LOGGER = Logger.getLogger(UserMsgController.class);

  @Autowired
  private UserMsgConmponent userMsgConmponent;
  
  /**
   * 上传用户当前接受消息id和用户身份数据
   * @param data
   * @param response
   * @return
   * @throws IOException
   */
  @RequestMapping(value = "/updatemsgid",produces="text/html;charset=UTF-8")
  @ResponseBody
  public String update(@RequestBody String data,HttpServletResponse response) throws IOException {
    if (null == data) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    Map<String, Object> map = (Map<String, Object>) JackSonUtil.jsonToObject(data, Map.class);
    if (null == map) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    String regitstId = (String) map.get("regitstId");//消息id
    String userId = (String) map.get("uuid");//用户id
    String equipmentNumber = (String) map.get("equipmentNumber");//设备号
    String version = (String) map.get("version");//app版本
    String token = (String) map.get("token");
    String ecode = (String) map.get("ecode");

    UserProfileQueryVO userProfileQueryVO = new UserProfileQueryVO();
    userProfileQueryVO.setRegitstId(regitstId);
    userProfileQueryVO.setEquipmentNumber(equipmentNumber);
    userProfileQueryVO.setVersion(version);
    if (!StringUtils.isEmpty(userId)) {
      userProfileQueryVO.setUserId(Long.parseLong(userId.trim()));
    }
    userProfileQueryVO.setToken(token);

    LOGGER.error(
        "Receive regitstId:" + userProfileQueryVO.getRegitstId() + ",token:" + userProfileQueryVO.getToken() + ",uid:" + userProfileQueryVO.getUserId() + ",equipmentNumber:" + userProfileQueryVO.getEquipmentNumber()+", messgeId="+userProfileQueryVO.getRegitstId());

    userMsgConmponent.updateUserProfile(userProfileQueryVO);

    return JackSonUtil.getJson(Result.getSuccDataResult(true));

  }
}
