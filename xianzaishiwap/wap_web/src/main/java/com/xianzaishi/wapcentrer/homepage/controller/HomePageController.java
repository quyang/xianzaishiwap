package com.xianzaishi.wapcentrer.homepage.controller;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.xianzaishi.itemcenter.common.GsonUtil;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.action.BaseActionAdapter;
import com.xianzaishi.itemcenter.common.query.HttpQuery;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.wapcenter.client.market.vo.BaseItemVO;
import com.xianzaishi.wapcenter.component.homepage.HomePageComponent;

/**
 * 首页接口
 * @author dongpo
 *
 */
@Controller
@RequestMapping("/requesthome")
public class HomePageController {
  
  @Autowired
  private HomePageComponent homePageComponent;
  
  private static final String STEP_PWD = "xzsUYFhkaf";
  private static final Logger LOGGER = Logger.getLogger(HomePageComponent.class);
  

  /**
   * 请求首页数据，分两次传输给客户端
   * @param hasAllFloor:是否包含首页所有楼层的数据
   * @return
   * @throws IOException
   */
  @RequestMapping(value = "/homepage",produces="text/html;charset=UTF-8")
  @ResponseBody
  public String queryHomePage(@RequestBody String data,HttpServletResponse response) throws IOException {
    if (null == data) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    response.setContentType("text/html;charset=UTF-8");
    Map<String, Object> map = (Map<String, Object>) JackSonUtil.jsonToObject(data, Map.class);
    if(null == map){
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    Integer pageId = Integer.valueOf((String) map.get("pageId"));
    Boolean hasAllFloor = null;
    if(null != map.get("hasAllFloor") && Boolean.valueOf(map.get("hasAllFloor").toString())){
      hasAllFloor = true;
    }else{
      hasAllFloor = false;
    }
    int version = 0;
    if(null != map.get("version")){
      version = 1;
    }
    String result = homePageComponent.queryHomePage(pageId,hasAllFloor, version);
    return result;
  }
  
  /**
   * 请求首页数据，分两次传输给客户端
   * @return
   * @throws IOException 
   */
  @RequestMapping(value = "/homepageoptimization",produces="text/html;charset=UTF-8")
  @ResponseBody
  public String getHomePage(@RequestBody String data,HttpServletResponse response) throws IOException {
    if (null == data) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    response.setContentType("text/html;charset=UTF-8");
    Map<String, Object> map = (Map<String, Object>) JackSonUtil.jsonToObject(data, Map.class);
    if(null == map){
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    Integer pageId = Integer.valueOf((String) map.get("pageId"));
    String[] stepList = ((String)map.get("stepIds")).split(",");
    List<Integer> stepIds = Lists.newArrayList();
    if(null != stepList){
      for(String step : stepList){
        stepIds.add(Integer.valueOf(step));
      }
    }
    LOGGER.info("请求首页参数为：" + pageId + "," + stepIds);
    String result = homePageComponent.getHomePage(pageId,stepIds);
    return result;
    
  }
  
  
  /**
   * 运营在搭建活动页面时，抓取商品数据接口
   * @param hasAllFloor:是否包含首页所有楼层的数据
   * @return
   * @throws IOException
   */
  @RequestMapping(value = "/activitypageinfo",produces="text/html;charset=UTF-8")
  @ResponseBody
  public String queryActivityPageItemInfo(@RequestBody String para) throws IOException {
    HttpQuery<List<Long>> idList = BaseActionAdapter.processListObjectParameter(para, Long.class);
    if(!BaseActionAdapter.isRightParameter(idList)){
      LOGGER.error("请求首页参数为：" + para);
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数错误"));
    }
    List<BaseItemVO> voList = homePageComponent.getItemBySkuIds(idList.getData());
    return GsonUtil.getJson(Result.getSuccDataResult(voList));
  }
  
  /**
   * 改变楼层
   * @param hasAllFloor:是否包含首页所有楼层的数据
   * @return
   * @throws IOException 
   */
  @RequestMapping(value = "/stepchange",produces="text/html;charset=UTF-8")
  @ResponseBody
  public String chageStep(String data, String pwd,HttpServletResponse response) throws IOException {
    if(StringUtils.isEmpty(data) || StringUtils.isEmpty(pwd) || pwd.equals(STEP_PWD)){
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空或参数不正确"));
    }
    FileWriter fw = new FileWriter(HomePageComponent.stepfile);
    fw.write(data);
    fw.flush();
    fw.close();
    return JackSonUtil.getJson(Result.getSuccDataResult(true));
    
  }
  
}
