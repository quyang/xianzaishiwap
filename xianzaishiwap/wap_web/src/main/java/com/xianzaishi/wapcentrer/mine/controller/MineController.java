package com.xianzaishi.wapcentrer.mine.controller;

import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.filter.StringUtils;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.wapcenter.client.mine.vo.UserInfoVO;
import com.xianzaishi.wapcenter.client.mine.vo.UserRegistVO;
import com.xianzaishi.wapcenter.client.mine.vo.UserRegistVO.RegistTypeContent;
import com.xianzaishi.wapcenter.component.mine.UserCenterComponent;
import java.io.IOException;
import java.text.ParseException;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.HEAD;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 用户中心接口
 * 
 * @author dongpo
 * 
 */
@Controller
@RequestMapping(value = "/requestuser")
public class MineController {

  @Autowired
  private UserCenterComponent userCenterComponent;

  private static final Logger logger = Logger.getLogger(MineController.class);

  /**
   * 用户登录接口
   * 
   * @param phone 手机号
   * @param verificationCode 验证码
   * @param hardwareCode 硬件设备号
   * @return
   */
  @RequestMapping(value = "/login", produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String userLogin(@RequestBody String data, HttpServletResponse response) {
    if (null == data) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    response.setContentType("text/html;charset=UTF-8");
    Map<String, String> map = (Map<String, String>) JackSonUtil.jsonToObject(data, Map.class);
    Long phone = Long.valueOf(map.get("phone"));
    String verificationCode = map.get("verificationCode");
    String hardwareCode = map.get("hardwareCode");
    String result = userCenterComponent.login(phone, verificationCode, hardwareCode);

    return result;
  }
  
  /**
   * 用户登录接口
   * 
   * @param phone 手机号
   * @param verificationCode 验证码
   * @param hardwareCode 硬件设备号
   * @return
   */
  @RequestMapping(value = "/activitylogin", produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String userActivityLogin(String data, HttpServletResponse response) {
    if (null == data) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    response.setContentType("text/html;charset=UTF-8");
    Map<String, String> map = (Map<String, String>) JackSonUtil.jsonToObject(data, Map.class);
    Long phone = Long.valueOf(map.get("phone"));
    String verificationCode = map.get("verificationCode");
    String hardwareCode = map.get("hardwareCode");
    String result = userCenterComponent.login(phone, verificationCode, hardwareCode);
    return result;
  }

  /**
   * 用户中心数据
   * 
   * @param token 用户登录token
   * @return
   */
  @RequestMapping(value = "/mine", produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String mineData(@RequestBody String data, HttpServletResponse response) {
    if (null == data) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    response.setContentType("text/html;charset=UTF-8");
    Map<String, String> map = (Map<String, String>) JackSonUtil.jsonToObject(data, Map.class);
    if (null == map) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    String token = map.get("token");
    String result = userCenterComponent.queryUserByToken(token);
    return result;
  }

  /**
   * 用户注册接口
   * 
   * @param userRegistVO 用户注册信息，包括phone、pwd、verificationCode
   * @return
   */
  @RequestMapping(value = "/regist", method = RequestMethod.POST,
      produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String userRegist(@RequestBody UserRegistVO userRegistVO) {
    if (null == userRegistVO || null == userRegistVO.getPhone()) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    Result<Long> result = new Result<>();
    if(userRegistVO.getRegistType().shortValue() == RegistTypeContent.INVITE_REGIST.shortValue()){
      result = userCenterComponent.inviteeRegist(userRegistVO);
    }else{
      result = userCenterComponent.regist(userRegistVO);
    }
    
    return JackSonUtil.getJson(result);
  }
  
  /**
   * 发送验证码
   * 
   * @param phone 手机号码
   */
  @RequestMapping(value = "/sendcheckcode", produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String sendCheckCode(@RequestBody String data, HttpServletResponse response) {
    if (null == data) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    response.setContentType("text/html;charset=UTF-8");
    Map<String, String> map = (Map<String, String>) JackSonUtil.jsonToObject(data, Map.class);
    if(null == map){
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    Long phone = Long.valueOf(map.get("phone"));
    if (null == phone) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "手机号码为空"));
    }
    
    String result = userCenterComponent.sendCheckCode(phone);
    return result;

  }
  
  /**
   * 发送验证码
   * 
   * @param phone 手机号码
   */
  @RequestMapping(value = "/sendActivityCheckcode", produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String sendActivityCheckCode(String data,HttpServletResponse response){
    if (null == data) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    response.setContentType("text/html;charset=UTF-8");
    Map<String, String> map = (Map<String, String>) JackSonUtil.jsonToObject(data, Map.class);
    if(null == map){
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    Long phone = Long.valueOf(map.get("phone"));
    String userTag = map.get("userTag");
    if (null == phone) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "手机号码为空"));
    }
    
    String result = userCenterComponent.sendActivityCheckCode(phone,userTag);
    return result;
  }

  /**
   * 忘记密码修改接口
   * 
   * @param userRegistVO 用户注册信息，包括phone、pwd、verificationCode
   * @return
   */
  @RequestMapping(value = "/modifiedpwd", method = RequestMethod.POST,
      produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String modifiedPwd(@RequestBody UserRegistVO userRegistVO) {
    if (null == userRegistVO) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    String result = userCenterComponent.updateUserPwd(userRegistVO);
    return result;
  }

  /**
   * 用户退出接口
   * 
   * @param token 登陆token
   * @return
   */
  @RequestMapping(value = "/logout", produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String userLogout(@RequestBody String data, HttpServletResponse response) {
    if (null == data) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    response.setContentType("text/html;charset=UTF-8");
    Map<String, String> map = (Map<String, String>) JackSonUtil.jsonToObject(data, Map.class);
    String token = map.get("token");
    if (null == token) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "token为空"));
    }
    String result = userCenterComponent.logout(token);
    return result;
  }

  /**
   * 根据token请求userId
   * 
   * @param token
   * @return
   */
  @RequestMapping(value = "/requestuserid", produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String requestUserId(@RequestBody String data, HttpServletResponse response) {
    if (null == data) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    response.setContentType("text/html;charset=UTF-8");
    Map<String, String> map = (Map<String, String>) JackSonUtil.jsonToObject(data, Map.class);
    String token = map.get("token");
    if (null == token) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "token为空"));
    }
    String result = userCenterComponent.queryUserIdByToken(token);
    return result;
  }

  /**
   * 修改个人信息
   * 
   * @param userInfoVO
   * @return
   */
  @RequestMapping(value = "/updateuserinfo", produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String updateUserInfo(@RequestBody UserInfoVO userInfoVO) {
    if (null == userInfoVO || null == userInfoVO.getUserId()) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    if(null != userInfoVO.getName()){
      String name = userInfoVO.getName();
      if(StringUtils.sqlValidate(name)){
        return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "名字中不允许有非法字符"));
      }
      name = StringUtils.doFilter(name);
      userInfoVO.setName(name);
    }
    String result = userCenterComponent.updateUserInfo(userInfoVO);

    return result;
  }

  /**
   * 验证是否是会员
   * 
   * @param phone
   * @return
   */
  @RequestMapping(value = "/verifymember", produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String verifyMember(@RequestBody String data, HttpServletResponse response) {
    if (null == data) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    response.setContentType("text/html;charset=UTF-8");
    Map<String, String> map = (Map<String, String>) JackSonUtil.jsonToObject(data, Map.class);
    Long phone = Long.valueOf(map.get("phone"));
    if (null == phone) {
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    String result = userCenterComponent.checkMember(phone);

    return result;
  }
  
  @RequestMapping(value = "/updatetag", produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String updateUserTag() throws IOException{
    String result = userCenterComponent.updateUserTag();
    return result;
  }
  
  @RequestMapping(value = "/queryuserinfobyid", produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String queryUserInfo(Long userId){
    if(null == userId){
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    String result = userCenterComponent.queryUserById(userId);
    
    return result;
  }
  
  @RequestMapping(value = "/querycouponinfo", produces = "text/html;charset=UTF-8")
  @ResponseBody
  public String queryCouponInfo(Long phone) throws ParseException{
    if(null == phone){
      return JackSonUtil.getJson(Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    String result = userCenterComponent.queryCouponInfo(phone);
    return result;
  }
  
}
